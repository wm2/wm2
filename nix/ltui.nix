{ sources ? import ./sources.nix, pkgs ? import sources.nixpkgs {}, lua ? pkgs.lua5_4}:

with pkgs;
with pkgs.luajitPackages;

let
  ncurses = import ./ncurses.nix {sources=sources; pkgs=pkgs;};
in
  callPackage({ buildLuarocksPackage, fetchzip, lua, luaOlder, luaAtLeast }:
    buildLuarocksPackage {
      pname = "ltui";
      version = "2.7-1";
      knownRockspec = (fetchurl {
        url    = "mirror://luarocks/ltui-2.7-1.rockspec";
        sha256 = "sha256-iQKyhPdl2q20TDJvaiQv+eHBHctq7goBb54WNUnuECU=";
      }).outPath;
      src = fetchzip {
        url    = "http://github.com/tboox/ltui/archive/v2.7.zip";
        sha256 = "sha256-J5aJJDxurrrFgABmSJFM7uJCuz1fcCIKEQ0bkxC25qY=";
      };
      disabled = (luaOlder "5.1") || (luaAtLeast "5.5");
      propagatedBuildInputs = [
          lua
          ncurses
      ];
      installPhase = ''
        runHook preInstall

        # work around failing luarocks test for Write access
        mkdir -p $out

        # luarocks make assumes sources are available in cwd
        # After the build is complete, it also installs the rock.
        # If no argument is given, it looks for a rockspec in the current directory
        # but some packages have several rockspecs in their source directory so
        # we force the use of the upper level since it is
        # the sole rockspec in that folder
        # maybe we could reestablish dependency checking via passing --rock-trees

        nix_debug "ROCKSPEC $rockspecFilename"
        luarocks $LUAROCKS_EXTRA_ARGS make --deps-mode=all --tree=$out ''${rockspecFilename} CURSES_DIR=${ncurses}

        runHook postInstall
      '';

    }) {}
