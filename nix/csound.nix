# Csound which also build Lua API

{ sources ? import ./nix/sources.nix, pkgs ? import sources.nixpkgs {}, lua ? pkgs.lua5_4}:

let
  csound = pkgs.csound;
in
  csound.overrideAttrs (
    finalAttrs: previousAttrs: {
      # See https://github.com/csound/csound/blob/a1580f9cdf331c35dceb486f4231871ce0b00266/interfaces/CMakeLists.txt#L5C8-L7
      cmakeFlags = [ "-DBUILD_CSOUND_AC=0" "-DBUILD_LUA_INTERFACE=1" "-DBUILD_CXX_INTERFACE=1" "-DLUA_LIBRARY=${lua}/lib/liblua.a" ];
      propagatedBuildInputs = [ lua ];
      # Swig is automatically found by cmake, but somehow cmake can't find lua, neither lua5_2 nor lua5_1...
      buildInputs = [ lua pkgs.swig ] ++ previousAttrs.buildInputs;
      nativeBuildInputs = [ lua pkgs.swig ] ++ previousAttrs.nativeBuildInputs;
    }
  )
