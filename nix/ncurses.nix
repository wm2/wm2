# ncurses with small fix that it can be found by ltui

{ sources ? import ./sources.nix, pkgs ? import sources.nixpkgs {}}:

let
  ncurses = pkgs.ncurses;
in
  ncurses.overrideAttrs (
    finalAttrs: previousAttrs: {
      # Help ltui to find curses by renaming it from 'ncurses' to 'curses'.
      postFixup = previousAttrs.postFixup + ''cp "$out"/lib/libncurses.so "$out"/lib/libcurses.so'';
    }
  )
 
