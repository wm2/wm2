package = "wm2"
version = "dev-1"
source = {
   url = "git+https://codeberg.org/wm2/wm2"
}
description = {
   summary = "vi for audio",
   detailed = [[
vi for audio
]],
   homepage = "https://codeberg.org/wm2/wm2",
   license = "*** please specify a license ***"
}
dependencies = {
   "lua ~> 5.4",
   "luaCsnd6"
}
build = {
   type = "builtin",
   modules = {
      ["wm2.init"] = "wm2/init.lua",
      ["wm2.utils"] = "wm2/utils.lua",
   },
   copy_directories = {
      "tests"
   }
}
