#!/usr/bin/env lua

-- Run all WM2 unit tests & print test results.

local wm2 = require("wm2")

RED = "\27[31m"
RESET = "\27[0m"
TEST_LOG_PATH = ".t"

wm2.utils.rm(TEST_LOG_PATH)

-- Log test info to terminal & to test log file.
function log(str, colour)
    local fp = io.open(TEST_LOG_PATH, "a")
    for s in str:gmatch("[^\r\n]+") do
        -- Tidy up result - this is necessary as we run the TUI
        -- that returns all type of data that pollutes & confuses the
        -- logs.
        s = s:gsub("[^%w^ ^\t^.^/^:^-^_^'^\"]", "")
        fp:write(s .. "\n")
        -- Colour only for terminal output
        s = (colour or "") .. s
        print(s)
    end
    print(RESET)
    fp:close()
end

-- Run test at test_path
function run(test_path)
    local exit, stdout, stderr = wm2.utils.execute("lua tests/" .. test_path)
    local colour = nil
    log("Run " .. test_path)
    if exit == 0 then
        -- We only need the last two lines of stdout -> they show the
        -- test results. Usually stdout shouldn't be much bigger in
        -- case no error was created, but when running TUI tests the
        -- TUI is piped to the stdout, which means we'd print the TUIs
        -- binary output if we don't split stdout.
        local stdoutlines = wm2.utils.splitlines(stdout)
        c = #stdoutlines
        stdout = wm2.utils.join("\n", { stdoutlines[c - 2], stdoutlines[c - 1] })
    else
        colour = RED
        log(stderr, RED)
    end
    log(stdout, colour)
    log("")
end

log("Write test output to '" .. TEST_LOG_PATH .. "'\n")
for f in io.popen([[ls -pa ./tests | grep -v /]]):lines() do
    if string.sub(f, 1, 4):lower() == "test" then
        run(f)
    end
end
