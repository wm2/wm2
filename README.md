# wm2

wm2 is a software to orchestra programs.

wm2 has a particular focus on audio-based programs & is intended to be used in performances or installations.

Imagine a mixture between a low-key QLab without video support & a sound programming environment like [Pure data](https://puredata.info) or [Supercollider](https://github.com/supercollider/supercollider).

As wm2 is build on-top of [Csound](https://csound.com/), you can do everything that's possible to do with Csound.

It provides a default TUI (based on [ltui](https://github.com/tboox/ltui)), a project declaration language & a program orchestrator.

It is driven by [permacomputing ideas](https://permacomputing.net/).

It is written in Lua.

## Goals

- highly stable, resilient & self-healing software: avoid disasters during performances
- small, simple & minimalistic: easy to maintain, to understand, to fix or debug & to extend
- low system requirements: tiny RAM & CPU usage
- constant backwards-compatibility: a wm2 based project should continue to work forever after any wm2 version upgrade
- reducing redundancy: providing very basic commonly needed live-electronic tools

## Non-goals

- trendy expensive new sound synthesis capacities (fancy AI DSP etc.)
- fully featured (e.g. bloated) software
- fancy GUI
- live-coding

## Installation

With [nix](https://nixos.wiki/wiki/Nix_package_manager) `wm2` can easily be installed.
Just clone this repository and start the nix shell:

```bash
git clone https://codeberg.org/wm2/wm2
cd wm2/
nix-shell shell.nix
```

Without nix, you'll need to setup the dependencies yourself:

- Csound with Lua API support (see [here](https://github.com/csound/csound/blob/a1580f9cd/interfaces/CMakeLists.txt#L5C8-L7): most Linux distributions seems to deliver csound without the Lua API which means you may need to compile csound yourself)
- [ncurses](https://en.wikipedia.org/wiki/Ncurses): perhaps already installed on your system
- [ltui](https://github.com/tboox/ltui): install with luarocks
- [stylua](https://github.com/JohnnyMorganz/StyLua): install with luarocks (optional, only needed for developing to autoformat the source code)

Then you can just clone this repository and import `wm2` in your Lua script.

## Usage

TODO

## Running tests

Just use the `t` script:

```bash
./t
```
