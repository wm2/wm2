-- The conductor is responsible to start & stop P while following ACID¹ properties.
--
-- The basic logic is that all programs defined in the users patch can be described by
-- a global 'Conductor.State'. This 'Conductor.State' contains the 'P.State' of each
-- program (e.g. a sequence of requesters & its runtime arguments). Whenever we request
-- the start, stop or switch of a P, we essentially commit a change in the global
-- 'Conductor.State'. However not all changes can be committed - some changes would lead
-- to inconsistent states and are therefore rejected by the conductor. To detect and avoid
-- inconsistent states, the conductor follows the following procedure whenever it receives
-- a start/stop/switch request:
--
--  1. It detects the current 'Conductor.State'.
--  2. It makes a copy of this state and performs all requested changes on this copy
--     of the state.
--  3. If any change would lead to an inconsistent state, it rejects the request.
--  4. If all changes are ok, it then finally compares the current state and the state
--     with the committed changes. Whenever there is a difference, it applies this
--     difference to the individual 'P' and its 'P.State'. This means it starts, stops
--     or switches a 'P' and it overrides its requesters and its runtime arguments.

local class = require("wm2.lib.middleclass")
local WM2Object = require("wm2.wm2object")
local Process = require("wm2.process")
local utils = require("wm2.utils")
local inspect = require("wm2.lib.inspect")

-- 'Conductor' handles starting & stopping of P.
local Conductor = class("Conductor", WM2Object)

function Conductor:initialize()
    WM2Object.initialize(self)
    self.requests_process = Conductor.RequestsProcess:new()
end

-- 'start' starts the conductor with patch.
function Conductor:start(patch)
    if self.patch then
        error("conductor is already started")
    end
    self.patch = patch
    self.requests_process:register()
end

-- 'stop' stops the conductor and unbind it from the
-- patch.
function Conductor:stop()
    if not self.patch then
        error("conductor is already stopped")
    end
    self.patch = nil
    self.requests_process:unregister()
end

-- 'current_state' returns a mapping of 'P.Id => P.State'.
-- The mapping contains entries for each P that has been defined
-- in the used patch.
function Conductor:current_state()
    local states = {}
    for _, p in ipairs(self.patch.sorted_programs) do
        states[tostring(p.id)] = p.state
    end
    return Conductor.State:new(self.patch.programs, states)
end

-- request_start, request_stop & request_switch must fulfill ACID¹ capacities
-- (but without durability as we don't persist program states):
--
-- A: They must be atomic e.g. all of their indented changes should be applied or none of them.
--    Otherwise we have inconsistent states. To avoid inconsistent states is the main purpose
--    of the 'request_X' functions and the main reason why other bits should never directly
--    call P.start, P.stop & P.switch.
--
-- C: They must be consistent: e.g. a P which depends on another P with state X
--    can't be started if this other P doesn't fulfill state x.
--
-- I: They must be isolated: when multiple parts call them, they are nevertheless
--    sequentially executed.
--
-- ¹https://en.wikipedia.org/wiki/ACID

-- TODO: Add restart_force argument to 'request_start' and 'request_switch':
--
--   Currently wm2 doesn't restart a P if it is already running in the
--   requested state at request time. But sometimes such a start may be
--   desired, for instance in case when we have an instrument with a decaying
--   plucked sound, and we want to sequentially play exactly the same note
--   for multiple times.
--
--   This is currently not possible, but we would need a new argument
--   for this:
--
--   restart_force = nil:   don't restart if already running
--   restart_force = 1:     try to restart, don't restart if someone
--                          else also requested current P
--   restart_force = 2:     restart and if it's not possible, just fail

-- Request start of 'P' with 'Id'. 'requester' is also a 'Id'.
function Conductor:request_start(id, requester, runtime_arg, callback)
    self:_defer_request(callback, function()
        return self._start_p(self, { id = id, requester = requester, runtime_arg = runtime_arg })
    end)
end

-- Request stop of 'P' with 'Id'. 'requester' is also a 'Id'.
function Conductor:request_stop(id, requester, callback)
    self:_defer_request(callback, function()
        return self._stop_p(self, { id = id, requester = requester })
    end)
end

-- Requests stop of 'P' with 'previous_id' and start of 'P'
-- with 'id'.
function Conductor:request_switch(previous_id, id, requester, runtime_arg, callback)
    self:_defer_request(callback, function()
        return self._switch_p(
            self,
            { previous_id = previous_id, id = id, requester = requester, runtime_arg = runtime_arg }
        )
    end)
end

-- defer request to next loop iteration
function Conductor:_defer_request(callback, func)
    callback = callback
        -- default callback raises warning in case of failed request
        or function(status, errormsg)
            if not status then
                self:warn("request failed: " .. errormsg)
            end
        end
    self.requests_process:add_request(function()
        local status, errormsg = pcall(func)
        local status2, errormsg2 = pcall(callback, status, errormsg)
        if not status2 then
            self:warn("raised error when calling callback: " .. errormsg2)
        end
    end)
end

-- proceed start request
function Conductor:_start_p(opt)
    self:debug(tostring(opt.requester) .. " requests start of " .. tostring(opt.id))
    self:_setp("start", opt, function(err, call, state0, state1)
        call(state1.start_p, state1, opt)
    end)
end

-- proceed stop request
function Conductor:_stop_p(opt)
    self:debug(tostring(opt.requester) .. " requests stop of " .. tostring(opt.id))
    self:_setp("stop", opt, function(err, call, state0, state1)
        call(state1.stop_p, state1, opt)
    end)
end

-- proceed switch request
function Conductor:_switch_p(opt)
    self:debug(
        tostring(opt.requester) .. " requests switch from " .. tostring(opt.previous_id) .. " to " .. tostring(opt.id)
    )
    self:_setp("switch", opt, function(err, call, state0, state1)
        call(state1.stop_p, state1, { requester = opt.requester, id = opt.previous_id })
        call(state1.start_p, state1, { requester = opt.requester, id = opt.id, runtime_arg = opt.runtime_arg })
    end)
end

-- Common code for executing a start/stop/switch request.
function Conductor:_setp(request_name, opt, func)
    function err(message)
        error(request_name .. " request rejected: " .. message)
    end

    function call(func, ...)
        local arg = { ... }
        local status, errmsg = pcall(func, table.unpack(arg))
        if not status then
            err(errmsg)
        end
    end

    if not self.patch then
        return err("patch undefined (perhaps not-yet started conductor)")
    end

    for k, v in pairs(opt) do
        utils.isarg(v, k, err)
    end

    local state0 = self:current_state()
    local state1 = state0:copy()

    func(err, call, state0, state1)

    local delta = state0:delta(state1)
    call(self._apply_delta, self, delta)
end

-- Apply global state delta (difference between for wanted and current
-- P states)
function Conductor:_apply_delta(delta)
    for id, states in pairs(delta) do
        local s0 = states[1]
        local s1 = states[2]
        local p = self.patch.programs[id]
        if not p then
            error("couldn't find P with id " .. tostring(id) .. " in patch")
        end
        local status, e = pcall(self._apply_delta0, self, p, s0, s1)
        if not status then
            -- Perhaps bug in p definition: this means we
            -- are in an inconsistent state now. This is really
            -- bad. Some P may already have started.
            --
            -- Is it better if we still continue and ignore only
            -- the buggy P(s)? Or should we really reject
            -- the complete requested P? The basic design
            -- of atomicity certainly asks for a complete rejection.
            -- On the other hand, in real-world examples, it may be better
            -- for the performance if the cue still runs, even if some parts
            -- of the program aren't working...
            self:fatal("bug in p detected. exit with inconsistent state. TODO: add 'revert' function to starters")
            error(e)
        end
    end
end

-- Apply delta for one P
function Conductor:_apply_delta0(p, s0, s1)
    local is_started = s0:any_requester()
    local runtime_arg_different = not table.eq(s0.runtime_arg, s1.runtime_arg)
    local should_start = s1:any_requester()
    p.state.requesters = s1.requesters
    if should_start then -- start | switch
        p.state.runtime_arg = s1.runtime_arg
        if is_started then
            -- NOTE We don't restart P as currently running state is the state
            -- that is expected. This is enforced here, but it's also enforced through
            -- the delta logic (e.g. that we only proceed programs which have a delta in
            -- their state). The only way, how the program can end up here with the same
            -- runtime arguments, is in case that the requesters changed while the runtime
            -- state persists. That's the reason why we need this extra check here.
            --
            -- TODO A better way to do this (because now we need to compute runtime_arg
            -- equality twice, once when creating the delta & once again here, which is
            -- wasting time) would be to differentiate delta type: requesters delta + runtime
            -- arg deltas.
            --
            -- TODO Sometimes it could be desired to restart P again (e.g. pluck).
            -- For this case, add 'restart_reforce' option.
            if runtime_arg_different then
                p:switch()
            end
        else
            p:start()
        end
    elseif is_started then -- Stop
        p:stop()
        p.state.runtime_arg = s1.runtime_arg
    else
        -- This should never be reached.
        error("stopped P should have the same state! s0: " .. tostring(s0) .. "; s1: " .. tostring(s1))
    end
end

-- Conductor.RequestsProcess handles start/stop/switch requests safely and concurrently.
Conductor.RequestsProcess = class("RequestsProcess", Process)

function Conductor.RequestsProcess:initialize()
    self._requests = {}
    Process.initialize(self, "requests process")
end

function Conductor.RequestsProcess:add_request(request)
    table.insert(self._requests, request)
end

function Conductor.RequestsProcess:unregister()
    Process.unregister(self)
    -- Cleanup remaining requests that aren't executed before shutdown.
    -- We also don't want to execute them anymore: the wm2 server already
    -- takes care of unconditionally stopping all 'P' during shutdown.
    self._requests = {}
end

function Conductor.RequestsProcess:iteration1()
    local request = self._requests[1]
    if request then
        table.remove(self._requests, 1)
        request()
        return 1
    end
    return 2 -- no work done
end

-- 'Conductor.State' is a collection of 'P.State' that are active at a
-- specific moment of time.
Conductor.State = class("State", WM2Object)

function Conductor.State:initialize(programs, states)
    self.programs = programs
    self.states = states -- maps P.Id to P.State
    WM2Object.initialize(self)
end

-- 'start_p' commits start of P to state
function Conductor.State:start_p(opt)
    local must_be_started = self:_start_p0(opt)
    if must_be_started then
        for _, d in ipairs(self.programs[tostring(opt.id)]:dependencies()) do
            self:start_p(d)
        end
    end
end

-- 'start_p' commits stop of P to state
function Conductor.State:stop_p(opt)
    local must_be_stopped = self:_stop_p0(opt)
    if must_be_stopped then
        for _, d in ipairs(self.programs[tostring(opt.id)]:dependencies()) do
            self:stop_p(d)
        end
    end
end

-- '_start_p0' changes the 'Conductor.State' by the provided start opt.
function Conductor.State:_start_p0(opt)
    local d = opt
    local s = self.states[tostring(d.id)]

    if not s then
        error("couldn't find P with id " .. tostring(d.id) .. " in 'Conductor.State'")
    end

    if s.requesters[d.requester] then
        error("requester " .. tostring(d.requester) .. " already requested start of " .. tostring(d.id))
    end

    local is_started = s:any_requester()

    -- Prevent inconsistent state here
    if is_started and not table.eq(s.runtime_arg, d.runtime_arg) then
        error(
            tostring(d.id)
                .. " is already running with a different state.\n\n\tRequested state:\n"
                .. inspect(d.runtime_arg)
                .. "\n\n\tRunning state:\n"
                .. inspect(s.runtime_arg)
        )
    end

    s.requesters[d.requester] = 1
    local must_be_started = not is_started
    if must_be_started then
        s.runtime_arg = d.runtime_arg
    end
    return must_be_started
end

-- 'stop_p0' changes the 'Conductor.State' by the provided stop opt.
function Conductor.State:_stop_p0(opt)
    local d = opt
    local s = self.states[tostring(d.id)]
    if not s then
        error("couldn't find P with id " .. tostring(d.id) .. " in 'Conductor.State'")
    end
    local is_started = s:any_requester()
    s.requesters[opt.requester] = nil
    local must_be_stopped = not s:any_requester()
    if must_be_stopped then
        s.runtime_arg = nil
    end
    return must_be_stopped
end

-- 'copy' deep-copies 'Conductor.State' into a new object.
function Conductor.State:copy()
    local states = {}
    for id, state in pairs(self.states) do
        states[tostring(id)] = state:copy()
    end
    return Conductor.State(self.programs, states)
end

-- 'delta' returns the difference between itself and another 'Conductor.State'.
-- The difference is encoded in the mapping 'id => { before, after }'.
function Conductor.State:delta(state1)
    local delta = {}
    for id, s0 in pairs(self.states) do
        local s1 = state1.states[id]
        if s0 ~= s1 then
            delta[id] = { s0, s1 }
        end
    end
    return delta
end

return Conductor
