-- WM2 utility functions

local middleclass = require("wm2.lib.middleclass")

local utils = {}

-- 'rm' removes file with 'path' from disk
function utils.rm(path)
    return utils.execute("rm " .. path)
end

-- 'mv' moves a file to a different path
function utils.mv(src, dst)
    return utils.execute("mv " .. src .. " " .. dst)
end

-- 'file_exists' returns true if file exists on system and otherwise false
function utils.file_exists(name)
    local f = io.open(name, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end

-- 'read_file' returns the string content of a file
function utils.read_file(path)
    local f = io.open(path, "r")
    if not f then
        error("couldn't find file '" .. path .. "'")
    end
    local content = ""
    while true do
        local l = f:read()
        if not l then
            break
        end
        content = content .. "\n" .. l
    end
    io.close(f)
    return content
end

-- 'execute' executes command and returns exitcode, stdout & stderr.
-- As 'io.popen' doesn't return the exitcode and 'os.execute' doesn't
-- return the output, this custom function is implemented.
-- See https://stackoverflow.com/questions/7607384/getting-return-status-and-program-output
function utils.execute(command)
    local tmpfile = os.tmpname()
    local exit = os.execute(command .. " > " .. tmpfile .. " 2> " .. tmpfile .. ".err")

    local stdout_file = io.open(tmpfile)
    local stdout = stdout_file:read("*all")

    local stderr_file = io.open(tmpfile .. ".err")
    local stderr = stderr_file:read("*all")

    stdout_file:close()
    stderr_file:close()

    return exit, stdout, stderr
end

-- 'isarg' tests that 'arg' is not 'nil' nor 'false'.
function utils.isarg(arg, name, err)
    if not err then
        err = error
    end

    if not arg then
        err(name .. " missing")
    end
end

-- 'unused_options' raises a warning for each not used
-- option
function utils.unused_options(options, warn)
    if not options then
        return
    end
    for key, value in pairs(options) do
        warn("Unused option key = " .. tostring(key) .. "; value = " .. tostring(value))
    end
end

-- 'sleep' sleeps for 'n' seconds
function utils.sleep(n)
    -- Is this only linux/unix compatible?
    -- Could it be more precise?
    os.execute("sleep " .. tonumber(n))
end

-- 'write_to_file' writes string content into path.
function utils.write_to_file(path, content)
    local f = io.open(path, "w")
    if f then
        f:write(content)
        io.close(f)
    else
        error("couldn't open " .. path)
    end
end

-- 'join' joins strings together by delimiter
function utils.join(delimiter, sequence)
    local r = ""
    local is_first = true
    for _, s in ipairs(sequence) do
        if is_first then
            is_first = false
            r = s
        else
            r = r .. delimiter .. tostring(s)
        end
    end
    return r
end

-- 'splitlines' splits string into lines
function utils.splitlines(str)
    lines = {}
    for l in str:gmatch("[^\r\n]+") do
        table.insert(lines, l)
    end
    return lines
end

function utils.remove_empty_lines(str)
    local lines = utils.splitlines(str)
    local flines = {}
    for _, l in ipairs(lines) do
        if l:gmatch("[^ ]")() ~= nil then
            table.insert(flines, l)
        end
    end
    return utils.join("\n", flines)
end

-- 'average' calculate the average number of
-- a table with numbers
function utils.average(data)
    local v = 0
    for i, k in ipairs(data) do
        v = v + k
    end
    return v / #data
end

-- 'scale' scales a value from an old range to a new range
function utils.scale(v, oldmin, oldmax, newmin, newmax)
    local oldspan = oldmax - oldmin
    local newspan = newmax - newmin

    assert(oldspan ~= 0)
    assert(newspan ~= 0)

    local percentage = (v - oldmin) / oldspan

    return (newspan * percentage) + newmin
end

-- 'errbank' calls functions and collects all errors that they return.
-- With 'errbank' you can raise all these errors later together with the
-- 'raise' method. This is helpful if you want to execute multiple functions
-- at different parts in your code and you want to ensure that as many functions
-- as possible are executed before the errors are raised.
utils.errbank = middleclass("errbank")

function utils.errbank:initialize()
    self.errors = {}
end

function utils.errbank:call(name, f, options)
    status, d = pcall(f, unpack(options or {}))
    if not status then
        table.insert(self.errors, "err when calling " .. name .. ": " .. d)
        return nil
    end
    return d
end

function utils.errbank:raise()
    local msg = utils.join("\n", self.errors)
    if #msg > 0 then
        error(msg)
    end
end

return utils
