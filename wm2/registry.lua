-- General purpose registry.
-- Mostly used to allow users to hook into wm2 code.

local class = require("wm2.lib.middleclass")
local WM2Object = require("wm2.wm2object")

local Registry = class("Registry", WM2Object)

function Registry:initialize(options)
    options = options or {}
    type_name = options.type_name or "object"

    WM2Object.initialize(self)
    self:info("initialize " .. type_name)

    self._content = {}
    self._type_name = type_name
    self._warn_override = options.warn_override or true
end

function Registry:register(key, value)
    self:debug("register '" .. tostring(key) .. "' with value '" .. tostring(value) .. "'")
    if self._warn_override then
        if self._content[key] then
            self:warn("override already defined '" .. self._type_name .. "' with name '" .. key .. "'")
        end
    end
    self._content[key] = value
end

function Registry:unregister(key)
    self:debug("unregister '" .. tostring(key) .. "'")
    self._content[key] = nil
end

function Registry:get(key)
    return self._content[key]
end

function Registry:content()
    return self._content
end

function Registry:reset()
    self:debug("reset '" .. tostring(key) .. "'")
    self._content = {}
end

return Registry
