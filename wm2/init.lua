local wm2 = { name = "wm2", version = "0.1.0" }

-- side effects
require("wm2.xlua")
require("wm2.xtable")
require("wm2.xstring")

wm2.utils = require("wm2.utils")
wm2.WM2Object = require("wm2.wm2object")
wm2.Registry = require("wm2.registry")
wm2.Process = require("wm2.process")
wm2.Patch = require("wm2.patch")
wm2.P = require("wm2.p")
wm2.Q = require("wm2.q")
wm2.cs = require("wm2.cs")
wm2.TUI = require("wm2.tui")
wm2.Seq = require("wm2.seq")
wm2.Conductor = require("wm2.conductor")
wm2.Server = require("wm2.server")
wm2.parser = require("wm2.parser")

local log = require("wm2.lib.log")
log.outfile = ".wm2.log"
wm2.utils.rm(log.outfile)
log.info("Write to logfile " .. log.outfile)

return wm2
