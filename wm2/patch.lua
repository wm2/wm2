-- A Patch is a collection of programms 'P'.
-- It essentially encapsulates a user declared wm2 setup.
-- A patch can be started by becoming bound to a server.

local class = require("wm2.lib.middleclass")
local cs = require("wm2.cs")
local log = require("wm2.lib.log")
local utils = require("wm2.utils")
local WM2Object = require("wm2.wm2object")

local Patch = class("Patch", WM2Object)

function Patch:initialize(options, ...)
    function p(k)
        return table.pop(options, k)
    end

    self.path = p("path")

    local filename = ""
    if self.path then
        local split_path = self.path:split("/")
        filename = split_path[#split_path]
    end
    self._repr = "Patch(" .. filename .. ")"

    WM2Object.initialize(self)

    -- General options
    self.loglevel = p("loglevel") or "info"

    self:info("set loglevel to '" .. self.loglevel .. "'")
    log.level = self.loglevel

    -- Audio options
    self.channel_count = (p("channel_count") or 1)
    self.sampling_rate = (p("sampling_rate") or 44100)
    self.audio_type = p("audio") or "jack"
    self.midi_type = p("midi") or "jack"
    self.software_buffer = p("software_buffer") or 256
    self.hardware_buffer = p("hardware_buffer") or 2048

    -- Warn unused options
    utils.unused_options(options, function(msg)
        self:warn(msg)
    end)

    local arg = { ... }
    self.p_count = 0
    -- 1. Use 'programs' if you want to get P by id.
    -- 2. Use 'sorted_programs' if you want to keep the same order as
    --    the patch was defined.
    self.programs = {}
    self.sorted_programs = arg

    for _, p in ipairs(arg) do
        -- We don't use the object itself, but its string representation
        -- to map P: in this way we can also use simple strings to
        -- fetch them; also we don't need to care whether two tables are equal.
        local key = tostring(p.id)
        if self.programs[key] then
            error("got two P with the same replication key: " .. key)
        end
        self.programs[key] = p
        self.p_count = self.p_count + 1
    end
end

function Patch:__tostring()
    return self._repr or ""
end

-- 'bind' binds server to Patch.
function Patch:bind(server)
    if self:is_bound() then
        error("Patch already bound")
    end
    self.server = server
    for _, p in ipairs(self.sorted_programs) do
        local errstatus, errormsg = pcall(p.bind, p, self.server)
        if not errstatus then
            self:fatal("couldn't bind '" .. tostring(p) .. "': " .. errormsg)
        end
    end

    self.csound_orchestra = cs.orc.build(self)
end

-- 'unbind' unbinds current server from patch.
function Patch:unbind()
    if not self:is_bound() then
        error("Patch not yet bound")
    end
    self.server = nil
    for _, p in ipairs(self.sorted_programs) do
        local errstatus, errormsg = pcall(p.unbind, p)
        if not errstatus then
            self:fatal("couldn't unbind '" .. tostring(p) .. "': " .. errormsg)
        end
    end
end

-- 'is_bound' returns True if patch is already bound
-- to a server and false otherwise.
function Patch:is_bound()
    return self.server ~= nil
end

return Patch
