-- Parse a yml file into a wm2 patch.
--
-- By providing a parser, wm2 allows users to easily declare patches via
-- writing yml files. This is more convenient & concise than writing lua
-- code. Furthermore it fits better as we actually configure a system
-- (& don't write a program).
--
-- A wm2 yml configuration consist of three blocks:
--
--  (1) configuration block with 'configure' key
--  (2) declaration block with 'declare' key
--  (3) registration block with 'register' key
--
-- In the first block general system-wide configurations are set (e.g.
-- channel count, audio system, name of the patch, ...).
--
-- In the second block all used P are declared with their specific
-- initialization values
--
-- In the third block new P types can be registered. NOTE: This block
-- isn't implemented yet.

local etlua = require("wm2.lib.etlua")
local tinyyaml = require("wm2.lib.tinyyaml")
local log = require("wm2.lib.log")
local utils = require("wm2.utils")
local Patch = require("wm2.patch")
local P = require("wm2.p")

local parser = {} -- namespace to collect yml parser functionality

-- 'parse_yml_string' parses a yml file into a wm2 patch.
function parser.parse_yml_file(path)
    return parser.parse_yml_string(utils.read_file(path), path)
end

-- 'parse_yml_string' parses a yml string into a wm2 patch.
function parser.parse_yml_string(yml, path)
    local ymlr, errormsg = etlua.render(yml, parser.env)
    if errormsg then
        error(errormsg)
    end

    local patchpath = ".wm2.yml"

    log.info("write rendered wm2 patch to " .. patchpath)
    utils.write_to_file(patchpath, ymlr)

    local status, t = pcall(tinyyaml.parse, ymlr)
    if not status then
        error("error when loading yml file: " .. t .. "\nfile is:\n" .. ymlr)
    end

    -- Configure wm2 server
    local configuration = t.configure or {}
    -- Declare all used P
    local declaration = t.declare or {}
    -- Register new P types
    local registration = t.register or {}

    if path then
        configuration.path = path
    end

    parser.register_registration(registration)
    local patch_arg = parser.declaration_to_programs(declaration, t)
    return Patch(configuration, unpack(patch_arg))
end

-- convert declaration into list of P objects.
function parser.declaration_to_programs(declaration, t)
    patch_arg = {}
    for p_name, replications in pairs(declaration) do
        local p_type = P.REGISTRY:get(p_name)
        if p_type then
            for replication_key, options in pairs(replications or {}) do
                p = p_type(replication_key, options)
                table.insert(patch_arg, p)
            end
        else
            log:warn("Couldn't find p_type with name " .. p_name .. ". IGNORED")
        end
    end
    return patch_arg
end

function parser.register_registration(registration)
    -- TODO(implement)
end

-- Set environment for template rendering.
-- Users can declare functions to be used in the yml files by adding
-- them to the 'parser.env' table.
parser.env = {}

-- 'load' loads string content of filename into current template
parser.env.load = function(filename, indent_count)
    local c = utils.read_file(filename)

    if indent_count then
        c = indent(c, indent_count)
    end

    local content, errormsg = etlua.render(c, parser.env)
    if errormsg then
        error(errormsg)
    end
    return content
end

-- Add all variables to global 'conf' table
parser.env.loadconf = function(filename)
    local localconf = tinyyaml.parse(utils.read_file(filename))
    conf = conf or {}
    for k, v in pairs(localconf) do
        conf[k] = v
    end
end

-- Local utility functions
--

-- 'indent' indents given text by 'indent_count' white spaces.
function indent(text, indent_count)
    local indent = ""
    for i = 1, indent_count do
        indent = indent .. " "
    end

    local t = ""
    for s in text:gmatch("[^\r\n]+") do
        t = t .. indent .. s .. "\n"
    end
    return t
end

return parser
