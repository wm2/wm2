-- Server encapsulates the public API and global constants that
-- are used by small programs 'P'.

local luaCsnd6 = require("luaCsnd6")
local libxcsound = require("wm2.libxcsound")
local utils = require("wm2.utils")
local WM2Object = require("wm2.wm2object")
local Process = require("wm2.process")
local Registry = require("wm2.registry")
local TUI = require("wm2.tui")
local cs = require("wm2.cs")
local inspect = require("wm2.lib.inspect")
local class = require("wm2.lib.middleclass")
local Conductor = require("wm2.conductor")
local parser = require("wm2.parser")

-- 'Server' starts and stops wm2 and holds the main loop.
local Server = class("Server", WM2Object)

-- Default path for csound messages log file
Server.CSOUND_LOG_PATH = ".wm2csound.log"

function Server:initialize(options)
    WM2Object.initialize(self)

    options = options or {}

    self.restart_agent = Server.RestartAgent:new(self)
    self.conductor = Conductor()

    function p(k)
        return table.pop(options, k)
    end

    -- Only for tests: start csound automatically when starting server
    self.auto_start_csound = p("auto_start_csound") or true

    -- Only for tests: don't sleep in tests to speed up tests
    if p("never_sleep") then
        self._sleep = function(t)
            self._has_slept = true
        end
    else
        self._sleep = utils.sleep
    end

    -- Loop state
    self.looping = false

    -- Warn unused options
    utils.unused_options(options, function(msg)
        self:warn(msg)
    end)

    -- Declare global clock
    self.clock = cs.clock:new("0", {})
end

function Server:get_perf_thread()
    if self.perf_thread then
        return self.perf_thread
    end
    error("server hasn't been started yet, performance thread isn't ready yet!")
end

-- Start server by loading patch into program.
--
-- The second option 'channel_maps' is a mapping of
--
--   tostring(P.id) => channel_map
--
-- where each channel_map is a mapping of
--
--   channel_index => channel_value
--
-- This is only relevant for cs.instr based P.
-- See also 'wm2.cs.instr:channel_map' for more
-- information. The option helps for the servers
-- restarting/self-healing capacity, but can also
-- be used in case a user wants to ensure specific
-- control channels should already be set to certain
-- values at server start.
function Server:start(patch, channel_maps, assert_valid_csound_code)
    if self:is_started() then
        error("already started!")
    end
    utils.isarg(patch, "patch")
    self:info("Start server with patch '" .. tostring(patch) .. "'")
    self.patch = patch
    -- Write Csound messages to logfile
    libxcsound.csoundSetDefaultMessageCallbackToLogFile(self.CSOUND_LOG_PATH)
    self.csound = luaCsnd6.Csound()
    self:_set_csound_options()
    self.patch:bind(self)
    self.clock:bind(self)
    -- Add 'clock' instr to orchestra
    local orc = patch.csound_orchestra .. "\n\n" .. self.clock:orchestra()
    self:debug("Set csound orchestra to\n\n" .. orc .. "\n")
    local orcpath = ".wm2.orc"
    self:info("write csound orchestra to " .. orcpath)
    utils.write_to_file(orcpath, orc)

    -- assert csound code is valid: this is explicitly done, to
    -- explicitly raise csound errors before wm2 starts so that
    -- the user can easily debug invalid csound code. This is
    -- particularly useful as wm2 hides csound logs to not pollute
    -- the TUI.
    -- This is done by default (default = nil), but can be turned off so
    -- that when restarting a patch no redundant code check is done.
    if assert_valid_csound_code or assert_valid_csound_code == nil then
        self:_assert_valid_csound_code(orcpath)
    end
    self.csound:CompileOrc(orc)
    if self.auto_start_csound then
        self.csound:Start()
    end
    self.perf_thread = luaCsnd6.CsoundPerformanceThread(self.csound)
    self.perf_thread:Play()
    self.clock:start()
    self.conductor:start(self.patch)
    self._started = true

    -- Apply control channel values
    if channel_maps then
        for id, c in pairs(channel_maps) do
            local p = self.patch.programs[tostring(id)]
            if p then
                p:apply_channel_map(c)
            else
                -- Perhaps P is no longer available after patch reload.
                -- This is kind of expected, hence 'info' instead of 'warn'.
                self:info("'" .. tostring(p) .. "' not found: can't set channel map")
            end
        end
    end
end

-- '_assert_valid_csound_code' runs syntax test on orchestra to see if
-- there are any problems in the csound orchestra code.
function Server:_assert_valid_csound_code(orcpath)
    -- We need a score file to run syntax check, even if the score file is empty.
    local scopath = ".wm2.sco"
    utils.write_to_file(scopath, "")
    local exit, stdout, stderr = utils.execute("csound --syntax-check-only " .. orcpath .. " " .. scopath)
    if exit ~= 0 then
        error("syntax error in csound code. Inspect orchestra at '" .. scopath .. "'. Csound traceback:\n\n" .. stderr)
    end
    utils.rm(scopath) -- cleanup
    self:info("run csound syntax check")
    -- NOTE: Even if there isn't any error, the syntax check writes to stderr
    for s in stderr:gmatch("[^\r\n]+") do
        self:info("csound: " .. s)
    end
end

function Server:_set_csound_options()
    csound_options = {
        -- in jack the name is always 'wm2'
        "-+jack_client=wm2",
        -- Read Midi device '1'
        "-M1",
        -- :null => prevent csound from autoconnecting to other jack clients
        "-odac:null",
        "-iadc:null",
        -- Suppress all displays
        "-d",
        -- Realtime audio and midi
        "-+rtaudio=" .. self.patch.audio_type,
        "-+rtmidi=" .. self.patch.midi_type,
        -- Buffer size
        "-b " .. tostring(self.patch.software_buffer),
        "-B " .. tostring(self.patch.hardware_buffer),
        -- k
        "--ksmps=32",
        -- Use real-time scheduling and lock memory (linux only)
        "--sched",
        -- Channel count
        "--nchnls=" .. self.patch.channel_count,
        -- Set Sample rate
        "--sample-rate=" .. self.patch.sampling_rate,
        -- Set control rate XXX Deactivated
        --"--control-rate=" .. (p("control_rate") or 4410),
    }
    for _, csound_option in ipairs(csound_options) do
        self:debug("set csound flag '" .. csound_option .. "'")
        self.csound:SetOption(csound_option)
    end
end

-- Stops all runnings processes, reset patch, close csound server.
function Server:stop()
    if not self:is_started() then
        error("already stopped!")
    end
    self:info("Stop server")
    self:panic()
    self.clock:stop()
    self:debug("Unbind patch")
    self.patch:unbind()
    self.patch = nil
    self:debug("Unbind clock")
    self.clock:unbind()
    self.conductor:stop()
    self:debug("Stop performance thread")
    self.perf_thread:Stop()
    self:debug("Stop csound")
    self.csound:Stop()
    self:debug("Cleanup csound")
    self.csound:Cleanup()
    self:debug("Reset csound")
    self.csound:Reset()
    self.csound = nil
    self._started = false
    self.perf_thread = nil
    -- End writing csound log file
    libxcsound.csoundCloseLogfile()
end

-- 'true' if playing and 'false' otherwise
function Server:is_started()
    return self._started or false
end

-- Restart server. Last resort for a user in case of a sudden bug
-- in a live situation. Restart tries its best to re-create the
-- previous state, but it doesn't guarantee that everything is
-- the same. If there is a user-defined state, that's changed
-- during runtime, maybe with a user-defined text-command, it
-- is very likely that this state won't be restored after restart.
-- Actual restart is handled via the 'Server.RestartAgent'.
function Server:restart(reload_patch)
    self:info("Received restart trigger, waiting until it's executed...")
    self:quit()
    -- We can't immediately restart process in 'restart'
    -- method, because quitting the main loop happens
    -- after the 'restart' method has been called.
    self._restart = true
    -- Set if patch should be reloaded from file.
    self.restart_agent.reload_patch = reload_patch
end

-- Start main blocking loop of program.
--
-- Loop proceeds all registered processes. Process interface
-- is defined in 'wm2.Process' (wm2/process.lua). Process registry
-- is defined in 'wm2.Process.REGISTRY' (this is a singleton).
-- Processes can un/register themselves.
function Server:loop(tui, currentpage, currentp)
    self:_loop_enter(tui, currentpage, currentp)
    while self.looping do
        self:_iteration1()
    end
    self:_loop_exit()
end

function Server:_loop_enter(tui, currentpage, currentp)
    if not self:is_started() then
        error("Can't run loop if server isn't started yet")
    end
    self:info("Enter main loop...")
    if tui then
        self.tui = TUI("tui")
        self.tui:bind(self)
        -- In case of an error during TUI start, we won't get a proper
        -- error message as the TUI hides the message - we therefore need
        -- to print this message into the logfile to debug the problem.
        local errstatus, errormsg = pcall(self.tui.start, self.tui)
        if not errstatus then
            local msg = "error when starting tui: " .. tostring(errormsg)
            self:fatal(msg)
            error(msg)
        end
        local errstatus, errormsg = pcall(self.tui.select_p, self.tui, currentpage, currentp)
        if not errstatus then
            local msg = "error when selecting p of tui: " .. tostring(errormsg)
            self:fatal(msg)
            error(msg)
        end
    end
    self.looping = true
end

function Server:_loop_exit()
    self:info("Exit main loop")
    self.restart_agent:maybe_collect()
    if self.tui then
        self.tui:stop()
        self.tui:unbind()
    end
    self:stop()
    self.restart_agent:maybe_restart()
end

-- Proceed one iteration of main program loop
function Server:_iteration1()
    local t = self.clock:t()
    local sleep = true

    for process, _ in pairs(Process.REGISTRY:content()) do
        local status = self:_process1(process, t)
        -- process tells us what it did:
        --      0: process requests to stop the program
        --      1: process worked on something
        --      2: process didn't do anything
        if status == 0 then
            self:info("process '" .. tostring(process) .. "' requests program exit")
            self.looping = false
            return
        elseif status == 1 then
            sleep = false
        end

        -- Stop process loop if last process called
        -- 'server:quit()'
        if not self.looping then
            return
        end
    end

    -- Sleeping significantly reduces the CPU usage of wm2:
    -- but if there are still processes to be executed this slows
    -- down the program. This is why each process must tell if it
    -- worked on something: if yes, then don't sleep to not slow
    -- down the program.
    if sleep then
        self._sleep(0.01)
    end
end

-- Proceed one process in one iteration
function Server:_process1(process, t)
    local errstatus, errormsg = pcall(process, t)
    if not errstatus then
        self:warn("process '" .. tostring(process) .. "' raised error: " .. errormsg)
        return 2
    end
    return errormsg -- successful: pcall doesn't return errormsg, but value
end

-- Stop main running loop
function Server:quit()
    self:debug("Quit loop")
    self.looping = false
end

-- Unconditionally stop all playing 'P' in 'Patch'.
--
-- Hint: This doesn't stop the 'TUI' or the main loop!
function Server:panic()
    self:debug("panic")
    if self.patch then
        for _, p in ipairs(self.patch.sorted_programs) do
            if p:is_started() then
                status, errormsg = pcall(p.stop, p)
                if not status then
                    self:warn("error when stopping p '" .. tostring(p) .. "': " .. errormsg)
                end
                status, errormsg = pcall(p.state.reset, p.state)
                if not status then
                    self:warn("error when resetting state of p '" .. tostring(p) .. "': " .. errormsg)
                end
            end
        end
    end
end

function Server:request_start(...)
    return self.conductor:request_start(table.unpack({ ... }))
end

function Server:request_stop(...)
    return self.conductor:request_stop(table.unpack({ ... }))
end

function Server:request_switch(...)
    return self.conductor:request_switch(table.unpack({ ... }))
end

-- 'Server.RestartAgent' encapsulates the restart-capacity
-- of the wm2 server.
Server.RestartAgent = class("RestartAgent", WM2Object)

function Server.RestartAgent:initialize(server)
    self._server = server
    self._server_state = {}
    WM2Object.initialize(self)
end

-- 'maybe_collect' collects information about the current
-- server state in case 'Server:restart()' has been
-- called before.
function Server.RestartAgent:maybe_collect()
    if self._server._restart then
        self:_collect()
    end
end

function Server.RestartAgent:_collect()
    self:info("Collect server state")
    self:_collect_patch()
    self:_collect_currentp()
    self:_collect_channel_maps()
end

function Server.RestartAgent:_collect_patch()
    self:debug("Collect patch")
    self._server_state.patch = self._server.patch
end

function Server.RestartAgent:_collect_currentp()
    self:debug("Collect currentp")
    -- Find current selected cue, so that after restart we
    -- are at exactly the same position as before.
    local tui = self._server.tui
    if not tui then
        return
    end
    local page = tui.app.paginator:current_page()
    local panel = page:panel(page)
    local currentp_view = panel:current()
    local c = 0
    for v in panel:views() do
        if v:name() == currentp_view:name() then
            break
        end
        c = c + 1
    end
    self._server_state.currentp = c
    self._server_state.currentpage = page.index
end

function Server.RestartAgent:_collect_channel_maps()
    self:debug("Collect channel maps")
    self._server_state.channel_maps = {}
    for _, p in ipairs(self._server.patch.sorted_programs) do
        if p.channel_map then
            self._server_state.channel_maps[tostring(p.id)] = p:channel_map()
        end
    end
end

-- 'maybe_restart' restarts the server in case
-- 'Server:restart()' has been called before.
function Server.RestartAgent:maybe_restart()
    if self._server._restart then
        self:_restart()
    end
end

function Server.RestartAgent:_restart()
    self:info("Restart server")

    -- Parameters
    local s = self._server_state
    local tui = s.currentp ~= nil

    -- Cleanup
    self._server_state = {}
    self._server._restart = nil

    -- Reload patch
    local patch = s.patch
    if self.reload_patch then
        if not patch.path then
            self:warn("path undefined: can't reload patch")
        else
            local status, msg = pcall(parser.parse_yml_file, patch.path)
            if not status then
                self:warn("patch reload failed: " .. msg)
                self:warn("fallback to previous version of patch")
            else
                patch = msg
            end
        end
    end

    -- Start & loop
    self._server:start(patch, s.channel_maps, false)

    --   Call user defined routines
    for routine, _ in ipairs(Server.RestartAgent.REGISTRY:content()) do
        routine(self._server, s)
    end

    self._server:loop(tui, s.currentpage, s.currentp)
end

-- Server.RestartAgent.REGISTRY allows users to register
-- hooks that are started when the server is restarted. In
-- this way users can improve the restart process by
-- explicitly setting parameters that are necessary for them
-- after a restart.
local RestartRegistry = class("RestartRegistry", Registry)

function RestartRegistry:initialize(options)
    options = options or {}
    options.type_name = "restart_routine"
    Registry.initialize(self, options)
end

function RestartRegistry:register(restart_routine)
    Registry.register(self, restart_routine, 1)
end

-- Singleton
Server.RestartAgent.REGISTRY = RestartRegistry:new()

return Server
