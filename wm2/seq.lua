-- Seq allows sequencing P.
--
-- A sequencer works by iterating through events, where each
-- event can start exactly one P. A sequencer is therefore always
-- only starting & stopping one P. But a sequencer can switch
-- between different P.
--
-- Events are provided as runtime arguments.
-- Each event is structured as a dict-like table
-- with two keys:
--
--    event:
--      duration: float
--      p:
--          - id
--          - runtime_arg
--
-- The first argument 'duration' is simply a floating
-- point number that indicates how many seconds the event
-- takes. The second argument 'p' is a list-like table,
-- where the first argument is the string representation of
-- a P that should be started and the second argument a dict
-- which contains the runtime argument with which the P should
-- be started.
--
-- In case 'id' or 'runtime_arg' are 'nil', or in case all events
-- were consumed, the sequencer stops the P that may have been
-- started from a previous event.

local class = require("wm2.lib.middleclass")
local P = require("wm2.p")
local utils = require("wm2.utils")
local Process = require("wm2.process")
local inspect = require("wm2.lib.inspect")

local Seq = class("Seq", P)

function Seq:initialize(replication_key, options)
    P.initialize(self, replication_key, options)
end

function Seq:bind(server)
    P.bind(self, server)
    self:setup_process()
end

function Seq:unbind()
    P.unbind(self)
    self.process = nil
end

function Seq:setup_process()
    self.process = Seq.Process:new(self.server, self, {}, nil)
end

function Seq:start()
    P.start(self)
    self:_start()
end

function Seq:switch()
    P.switch(self)
    -- In case of a sequencer, we really need to stop and restart it
    -- in case of a switch, because we need to be sure the previous
    -- p that has been activated by the sequencer is stopped again.
    self:_stop()
    self:_start()
end

function Seq:_start()
    self.process:reset()

    -- each event consist of:
    --
    --  p:
    --      - $p_id
    --      - $runtime_arg
    --  duration: $duration
    --
    self.process.events = self.state.runtime_arg.events or {}
    self.process.loop = self.state.runtime_arg.loop or false

    self.process:register()
end

function Seq:_stop()
    self.process:unregister()
end

Seq.Process = class("Seq.Process", Process)

function Seq.Process:initialize(server, seq, events, loop)
    self.server = server
    self.seq = seq
    self.events = events
    self.loop = loop
    self._current_index = 1
    Process.initialize(self, "sequencer process")
end

function Seq.Process:next_event()
    local e = self.events[self._current_index]
    if e then
        self._current_index = self._current_index + 1
    elseif self.loop and self._current_index > 1 then
        self._current_index = 1
        return self:next_event()
    end
    return e
end

function Seq.Process:iteration1()
    local e = self:next_event()
    if not e then
        self.seq:debug("all events consumed: stop")
        return self:unregister()
    end

    self.seq:debug("play event " .. inspect(e))

    local duration = e.duration
    if not duration then
        self.seq:warn("found event without declared duration. autoset to 1")
        duration = 1
    end

    self:defer_sleep(duration)

    local pinfo = e.p
    if not pinfo then
        return self:_stop()
    end
    local id = pinfo[1]
    local runtime_arg = pinfo[2]
    if not id or not runtime_arg then
        return self:_stop()
    end

    function callback(status, errormsg)
        if status then
            -- Only override 'previous_id' argument in case the request
            -- was successful - otherwise the previously started P is
            -- still running (in case of a switch request) and will never
            -- be stopped.
            self.previous_id = id
        else
            self.seq:warn("request failed: " .. errormsg)
        end
    end

    if self.previous_id then
        self.server:request_switch(self.previous_id, id, self.seq.id, runtime_arg, callback)
    else
        self.server:request_start(id, self.seq.id, runtime_arg, callback)
    end
end

function Seq.Process:unregister()
    local r = Process.unregister(self)
    self:_stop()
    return r
end

function Seq.Process:_stop()
    if self.previous_id then
        self.server:request_stop(self.previous_id, self.seq.id)
        self.previous_id = nil
    end
end

function Seq.Process:reset()
    Process.reset(self)
    self._current_index = 1
    self.previous_id = nil
end

P.REGISTRY:register("seq", Seq)

return Seq
