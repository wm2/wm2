-- A 'Process' instance encapsulates a function that can
-- be put into the global event loop and that can repeat
-- its code after waiting ('sleeping') for some time.
-- With 'Process' concurrently running activities can be
-- simulated. The emphasise is on 'simulated', as wm2 is
-- strictly single-threaded, everything runs in fact
-- sequentially. But because processes can wait, it looks
-- as if they would happen simultaneously.
--
-- In order to define a new process type, a new class can
-- be defined that inherits from the 'Process' class.
-- Then the 'iteration1' method needs to be overridden.
-- This method tells what the process should actually
-- do at each iteration. It should return '0' in case
-- the process demands the program to stop, '1' in case
-- it did some work and '2' in case it didn't do anything.
-- In order to skip some iterations of the global loop, the
-- 'defer_sleep' method can be called within 'iteration1'.

local class = require("wm2.lib.middleclass")
local WM2Object = require("wm2.wm2object")
local Registry = require("wm2.registry")

local Process = class("Process", WM2Object)

function Process:initialize(name)
    WM2Object.initialize(self)
    self.name = name
    self.t = nil -- start time
    self.next_t = nil -- next time when process is called
end

function Process:__tostring()
    return self.name or "unnamed process"
end

function Process:__call(t)
    if not self.t then
        self.t = t
    end
    if not self.next_t or t >= self.next_t then
        return self:iteration1()
    end
    return 2 -- no work done
end

-- Register itself to running loop
function Process:register()
    Process.REGISTRY:register(self)
    return self
end

-- Unregister itself from running loop
function Process:unregister()
    Process.REGISTRY:unregister(self)
    return self
end

-- Forces next 'iteration1' to be waited for 'seconds' time
-- to pass.
function Process:defer_sleep(seconds)
    -- We always add from the last expected time and not from
    -- the last real time, to avoid drift.
    local next_t = self.next_t or self.t
    self.next_t = seconds + next_t
end

-- One iteration of the loop. This is the function
-- that should be overridden in user defined processes.
function Process:iteration1()
    self:defer_sleep(1)
end

-- Reset the process internal time.
function Process:reset()
    self.t = nil
    self.next_t = nil
end

-- Process.REGISTRY keeps track of all currently active
-- processes. To start a new process and to put it into
-- event queue, one can register this process via the
-- globally defined 'wm2.Process.REGISTRY'. To drop a
-- process from the global event loop, the process can be
-- unregistered from the 'wm2.Process.REGISTRY'.
local ProcessRegistry = class("ProcessRegistry", Registry)

function ProcessRegistry:initialize(options)
    options = options or {}
    options.type_name = "process"
    Registry.initialize(self, options)
end

function ProcessRegistry:register(process)
    Registry.register(self, process, 1)
end

Process.REGISTRY = ProcessRegistry:new() -- Singleton

return Process
