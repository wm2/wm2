-- libxcsound provides functions to write Csound messages into a logfile.

local ffi = require("ffi")

ffi.cdef([[
void csoundSetDefaultMessageCallbackToLogFile(const char* logfilename);
void csoundCloseLogfile();
]])

return ffi.load("libxcsound")
