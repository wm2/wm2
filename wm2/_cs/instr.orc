{{ -- This template contains the macro 'instrument' which auto-generates Csound code }}
{{ -- for one wm2-Csound instrument. This file is loaded by 'wm2.cs.instr'. }}

{{m instrument }}
{{= f(instr:preamble()) }}
instr {{= instr.id }}
  ;; (auto)instr: Declare parameters
{{= f(declare_parameters({instr = instr})) }}
  ;; (auto)instr: Declare inputs
{{= f(inputs({instr = instr})) }}
  ;; (user)instr: Declare instrument body
{{= instr:instrument_body() }}
  ;; (auto)instr: Declare outputs
{{= f(outputs({instr = instr})) }}
endin
{{m instrument }}

{{m declare_parameters }}
  {{= var_fade_in_duration }}  = {{= instr.fade_in_duration }}
  {{= var_fade_out_duration }} = {{= instr.fade_out_duration }}
  {{ if instr.withdb then }}
  {{= var_db }} chnget "{{= instr.db_channel.name }}"
  {{= var_amp }} = ampdbfs({{= var_db }})
  {{ else }}
  {{= var_db }} = 0  ;; no external db control defined
  {{= var_amp }} = 1  ;; no external db control defined
  {{ end }}

  {{ -- Fading only works if the fade duration is not 0. In this way we }}
  {{ -- can define instruments with only fade-in or only fade-out. }}
  {{ if instr.fade_in_duration > 0 or instr.fade_out_duration > 0 then }}
  {{= var_fader }} linenr 1, {{= var_fade_in_duration }}, {{= var_fade_out_duration }}, 0.001
  {{ else }}
  {{= var_fader }} = 1 ;; no fading defined
  {{ end }}
{{m declare_parameters }}

{{m inputs }}
  {{ contract = { ainput = instr.audio_inputs, kinput = instr.control_inputs } }}
  {{ for base_input_name, inputs in pairs(contract) do }}
  {{   for i, input in ipairs(inputs) do }}
  {{= base_input_name .. tostring(i - 1) }} chnget "{{= input }}"
  {{   end }}
  {{ end }}
{{m inputs }}

{{m outputs }}
{{ if instr.channel_count > 0 then }}
{{= postprocess_outputs({instr = instr}) }}
{{= monitoring_outputs({instr = instr}) }}
{{= audio_outputs({instr = instr}) }}
{{= control_outputs({instr = instr}) }}
{{ end }}
{{m outputs }}

{{m postprocess_outputs }}
  {{ function bool_to_int(v) if v then return 1 else return 0 end end }}
  {{= var_cleanup }} wm2__cleanup {{= var_fade_out_duration }}, {{= bool_to_int(instr.docleanup) }}
  {{= var_amp_final }} = kwm2__amp_main * {{= var_cleanup }} * {{= var_fader }}

  {{ for i, signal_name in ipairs(instr.signals) do }}
  {{= instr.postsignals[i] }} = {{= signal_name }} * {{= var_amp_final }}
  {{ end }}
{{m postprocess_outputs }}

{{ -- Besides the actual audio channels, we also need dedicated channels        }}
{{ -- for monitoring the signals level. It's not enough to use the control      }}
{{ -- channels for this, because                                                }}
{{ --   - we need to scale the control value to be able to display quiet        }}
{{ --     signals, as it seems to be a limitation in the Lua Csound API         }}
{{ --     that it clips values that are smaller than '0.01'                     }}
{{ --   - having the RMS displayed instead of having the amplitude displayed    }}
{{ --     is much more informative (this only works for 'arate' signals.        }}
{{ --     for 'krate' signals we don't send the RMS)                            }}
{{m monitoring_outputs }}
  {{ for signal_name, d in pairs(instr.monitoring_channels) do }}
      {{ local monitor_signal_name = "k" .. signal_name:sub(2) .. "M" }}
  if {{= var_cleanup }} == 0 then
      {{ -- RMS may be > 0, even if audio signal is already 0.                  }}
      {{ -- Therefore we need extra check here, otherwise RMS stays > 0 even    }}
      {{ -- after instr is stopped and after instr only outputs 0.              }}
    {{= monitor_signal_name }} = 0
  else
      {{ if instr.rate == "arate" then }}
    {{= monitor_signal_name }} rms {{= signal_name }}
      {{ else }}
    {{= monitor_signal_name }} = {{= signal_name }}
      {{ end }}
  endif 
    {{ local msig = monitor_signal_name }}
    {{ if instr.absolute_monitor_signal then }}
    {{   msig = "abs(" .. monitor_signal_name .. ")" }}
    {{ end }}

    {{ -- We scale the signal, because when getting a channels value via    }}
    {{ -- Csounds Lua API the smallest received value seems to be 0.01. In  }}
    {{ -- order to support quieter signals, we therefore scale up the value }}
    {{ -- here and scale it down later again in the TUI.                    }}
  chnset {{= msig }} * {{= MONITORING_VALUE_SCALE }}, "{{= d.name }}"
  {{ end }}
{{m monitoring_outputs }}

{{ -- Send to audio channels (if rate==krate, audio_channels is empty) }}
{{m audio_outputs }}
  {{ for signal_name, channel_name in pairs(instr.audio_channels) do }}
  chnset {{= signal_name }}, "{{=channel_name}}"
  {{ end }}
{{m audio_outputs }}

{{ -- Send to control channels (if rate==arate, control_channels is empty) }}
{{m control_outputs }}
  {{ for signal_name, d in pairs(instr.control_channels) do }}
  chnset {{= signal_name }}, "{{= d.name }}"
  {{ end }}
{{m control_outputs }}
