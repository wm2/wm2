0dbfs = 1

chn_a "{{= var_aempty }}", 1
chn_k "{{= var_kempty }}", 1

;; 'wm2__cleanup' sets signal to 0 after instrument faded out, to
;; ensure when instrument isn't playing it doesn't send any noise
;; to other channels.
;;
;; This is mostly useful for audio signals. For control signals we
;; usually want to keep the same value after the instrument is stopped.
opcode wm2__cleanup, k, ii
  ifade_out_duration, icleanup xin
  ;: Check if cleanup is triggered:
  ;; For some signals cleaning up doesn't make sense & it's
  ;; better if the signal stays with the same value (for instance
  ;; a midi control signal shouldn't suddenly change after being
  ;; stopped as this could create sudden jumps in volume or frequency
  ;; when switching cues).
  if icleanup == 1 then
    kreleasestate release
    if kreleasestate == 1 then
      if ifade_out_duration > 0 then
        kreleasepos linseg 0, ifade_out_duration, 1
      else
        kreleasepos = 1
      endif
      if kreleasepos >= 0.99 then
        kcleanup = 0
      else
        kcleanup = 1
      endif
    else
      kcleanup = 1
    endif
    xtratim ifade_out_duration + 0.2
  else
    kcleanup = 1
  endif
  xout kcleanup
endop

{{ -- Write all user declared instruments to orchestra -}}
{{ for _, instr in ipairs(instruments) do -}}
{{ if instr.orchestra ~= nil then -}}
{{= instr:orchestra() }}
{{ end -}}
{{ end -}}
