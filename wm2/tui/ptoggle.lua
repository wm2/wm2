local ltui = require("ltui")

local ptoggle = ptoggle or ltui.button()

function ptoggle:init(app, tui, id, bounds)
    self.app = app
    self.tui = tui
    self.id = id

    local p = self.tui.server.patch.programs[tostring(id)]

    table.insert(p.start_hooks, function()
        self:invalidate()
    end)
    table.insert(p.stop_hooks, function()
        self:invalidate()
    end)

    local n = tostring(id)
    ltui.button.init(self, n, bounds, n, function() end)
end

function ptoggle:__tostring()
    return "ptoggle(" .. tostring(self.id) .. ")"
end

function ptoggle:is_active()
    if not self.tui.server.patch then
        return false
    end
    return self.tui.server.patch.programs[tostring(self.id)]:is_started()
end

function ptoggle:on_event(e)
    -- selected?
    if not self:state("selected") then
        return
    end

    -- enter this button?
    if e.type == ltui.event.ev_keyboard then
        -- Toggle activate this
        if e.key_name == " " then -- Space
            self:toggle()
            return true
        -- Toggle activate this, but also stop previously activated P
        -- The idea of this is to seamlessly start the next cue.
        elseif e.key_name == "o" then
            self:switch()
            return true
        end
    end
end

function ptoggle:switch()
    local previous_ptoggle = self.app.last_activated_ptoggle
    if not previous_ptoggle or not self.app.last_activated_ptoggle:is_active() then
        return self:toggle()
    end
    self.tui.server:request_switch(previous_ptoggle.id, self.id, self.tui.id, {}, function(status, errormsg)
        if status then
            self.app.last_activated_ptoggle = self
        else
            self.tui:warn("Failed to switch " .. tostring(self.id) .. ": " .. errormsg)
        end
    end)
end

function ptoggle:toggle()
    if not self:is_active() then
        self.tui.server:request_start(self.id, self.tui.id, {}, function(status, errormsg)
            if status then
                self.app.last_activated_ptoggle = self
            else
                self.tui:warn("Failed to start " .. tostring(self.id) .. ": " .. errormsg)
            end
        end)
    else
        self.tui.server:request_stop(self.id, self.tui.id, function(status, errormsg)
            if not status then
                self.tui:warn("Failed to stop " .. tostring(self.id) .. ": " .. errormsg)
            end
        end)
    end
end

function ptoggle:on_draw(transparent)
    -- draw background
    ltui.view.on_draw(self, transparent)

    -- strip text string
    local str = self:text()
    if str and #str > 0 then
        str = string.sub(str, 1, self:width())
    end
    if not str or #str == 0 then
        return
    end

    -- get the text attribute value
    local textattr = { self:textattr_val() }

    -- selected?
    if self:state("selected") and self:state("focused") then
        table.insert(textattr, "underline")
    end

    if self:is_active() then
        table.insert(textattr, "reverse")
    end

    -- draw text
    self:canvas():attr(textattr):move(0, 0):putstr(str)
end

return ptoggle
