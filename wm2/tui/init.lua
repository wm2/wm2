-- TUI provides a small terminal user interface for wm2.
--
-- The structure of the TUI is:
--
-- app
--      ->
--          paginator
--                      ->
--                          page
--                                  ->
--                                      gridpanel
--                                                  ->
--                                                      ptoggle
--      ->
--          detail
--                      ->
--                          widgets
--
local class = require("wm2.lib.middleclass")
local ltui = require("ltui")
local P = require("wm2.p")
local app = require("wm2.tui.app")

local TUI = class("TUI", P)

function TUI:initialize(replication_key, options)
    P.initialize(self, replication_key, options)
end

function TUI:start()
    P.start(self)
    self.app = app:new(self)
end

function TUI:_stop()
    if self.app then
        self.app:quit()
    end
    if not ltui.curses.isdone() then
        ltui.curses.done()
    end
end

function TUI:select_p(currentpage, currentp)
    if not currentp then
        return
    end
    self.app.paginator:select_page(currentpage)
    -- Select specific P
    local p = self.app.paginator:current_page():panel()
    local c = 0
    for v in p:views() do
        if c == (currentp or 0) then
            p:select(v)
            break
        end
        c = c + 1
    end
end

return TUI
