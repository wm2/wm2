-- This is a drop-in replacement for ltui.base.list.
-- It aims to offer the same functionality, but works on a 3-dimensional
-- grid instead of working on only a 2-dimensional grid.
--
-- Inserting items into a grid is much more expensive than inserting
-- items into a list. But in the way how grids are used within wm2,
-- this doesn't matter as the grid-building only happens during program
-- startup. Once the program runs, only the "up", "next", "prev", ...
-- operations are needed and all of them should be as fast as the same
-- operations on ltui lists.
--
-- NOTE: Currently not the full list functionality is implemented yet.

-- load modules
local object = require("ltui/object")

-- define module
local grid = grid or object({ _init = { "row_length", "_length" } })({ 0 })

-- clear grid
function grid:clear()
    self._length = 0
    self._first = nil
    self._last = nil
end

-- insert item after the given item
function grid:insert(t, after)
    if not after then
        return self:insert_last(t)
    end
    error("not implemented yet")
end

-- insert the first item in head
function grid:insert_first(t)
    error("not implemented yet")
end

-- insert the last item in tail
function grid:insert_last(t)
    if self._last then
        row_index = self._last._row_index
        column_index = self._last._column_index + 1
        if column_index == self.row_length then
            column_index = 0
            row_index = row_index + 1
        end

        t._row_index = row_index
        t._column_index = column_index

        -- last/next logic
        self._last._next = t
        t._prev = self._last

        -- grid logic
        last = self._last
        if row_index == self._last._row_index then
            t._left = last
            last._right = t
        end
        while 1 do
            if last._column_index == column_index and last._row_index == row_index - 1 then
                last._down = t
                t._up = last
                break
            end
            last = self:prev(last)
            if not last then
                break
            end
        end

        self._last = t
    else
        t._column_index = 0
        t._row_index = 0
        self._first = t
        self._last = t
    end
    self._length = self._length + 1
end

function grid:height()
    return self._length % self.row_length
end

function grid:width()
    return math.min(self.row_length, self._length)
end

-- remove item
function grid:remove(t)
    error("not implemented yet")
end

-- remove the first item
function grid:remove_first()
    error("not implemented yet")
end

-- remove last item
function grid:remove_last()
    error("not implemented yet")
end

-- push item to tail
function grid:push(t)
    self:insert_last(t)
end

-- pop item from tail
function grid:pop()
    self:remove_last()
end

-- shift item: 1 2 3 <- 2 3
function grid:shift()
    self:remove_first()
end

-- unshift item: 1 2 -> t 1 2
function grid:unshift(t)
    self:insert_first(t)
end

-- get first item
function grid:first()
    return self._first
end

-- get last item
function grid:last()
    return self._last
end

-- get next item
function grid:next(last)
    if last then
        return last._next
    else
        return self._first
    end
end

-- get the previous item
function grid:prev(last)
    if last then
        return last._prev
    else
        return self._last
    end
end

-- get the left item
function grid:left(last)
    return last and last._left
end

-- get the right item
function grid:right(last)
    return last and last._right
end

-- get the up item
function grid:up(last)
    return last and last._up
end

-- get the down item
function grid:down(last)
    return last and last._down
end

-- get grid size
function grid:size()
    return self._length
end

-- is empty?
function grid:empty()
    return self:size() == 0
end

-- get items
--
-- e.g.
--
-- for item in grid:items() do
--     print(item)
-- end
--
function grid:items()
    local iter = function(grid, item)
        return grid:next(item)
    end
    return iter, self, nil
end

-- get reverse items
function grid:ritems()
    local iter = function(grid, item)
        return grid:prev(item)
    end
    return iter, self, nil
end

-- new grid
function grid.new(row_length)
    assert(row_length ~= nil)
    return grid(row_length)
end

-- return module: grid
return grid
