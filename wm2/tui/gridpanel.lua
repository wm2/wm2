-- Define main panel that's used in main window.

local ltui = require("ltui")
local grid = require("wm2.tui.grid")

local gridpanel = gridpanel or ltui.panel()

function gridpanel:init(name, bounds, tui, row_length)
    self.tui = tui

    -- Copy-pasted from https://github.com/tboox/ltui/blob/184fb85/src/ltui/panel.lua#L38-L76
    -- in order to replace 'list' with wm2 specific 'grid'.

    -- init view
    ltui.view.init(self, name, bounds)

    -- mark as panel
    self:type_set("panel")

    -- mark as selectable
    self:option_set("selectable", true)

    -- init child views
    self._VIEWS = grid.new({ row_length = row_length, _length = 0 })

    -- init views cache
    self._VIEWS_CACHE = {}

    -- on click action
    self:option_set("mouseable", true)
    self:action_set(ltui.action.ac_on_clicked, function(v, x, y)
        -- get relative coordinates
        x, y = x - v:bounds().sx, y - v:bounds().sy

        -- try focused first
        local current = v:current()
        if
            current
            and current:option("mouseable")
            and (current:option("blockmouse") or current:bounds():contains(x, y))
        then
            return current:action_on(ltui.action.ac_on_clicked, x, y)
        end

        local p = v:last()
        while p do
            if p:option("selectable") and p:bounds():contains(x, y) then
                if p:option("mouseable") then
                    v:select(p)
                    return p:action_on(ltui.action.ac_on_clicked, x, y)
                end
                return true
            end
            p = v:prev(p)
        end
    end)
end

function gridpanel:on_event(e)
    if e.type == ltui.event.ev_keyboard then
        local k = e.key_name
        -- Move left, right, up & down
        if k == "l" then
            return self:select_right()
        elseif k == "h" then
            return self:select_left()
        elseif k == "j" then
            return self:select_down()
        elseif k == "k" then
            return self:select_up()
        -- Jump to to beginning/end
        elseif k == "^" then
            return self:select_first()
        elseif k == "G" then
            return self:select_last()
        elseif k == "n" then
            self:select_next()
            local pt = self:current() -- ptoggle
            pt:switch()
        elseif k == "b" then
            self:select_prev()
            local pt = self:current() -- ptoggle
            pt:switch()
        -- Fast restart => this also exists as a
        -- command ("restart"), but in case we don't
        -- use a keyboard but only remote control, this
        -- may be helpful.
        -- NOTE: Do we really need both, the text command
        -- and the keyboard command?
        elseif k == "r" then
            self.tui.server:restart()
        elseif k == "R" then
            self.tui.server:restart(true)
        elseif k == "P" then
            self.tui.server:panic()
        elseif k == "i" then
            local pt = self:current() -- ptoggle
            local p = self.tui.server.patch.programs[tostring(pt.id)]
            self.tui.app.detail:set_p(p)
            self.tui.app:set_mode("detail")
        end
    end
end

function gridpanel:select_left(start, reset)
    return self:_select_by(start, reset, function(current)
        return self:left(current)
    end)
end

function gridpanel:select_right(start, reset)
    return self:_select_by(start, reset, function(current)
        return self:right(current)
    end)
end

function gridpanel:select_up(start, reset)
    return self:_select_by(start, reset, function(current)
        return self:up(current)
    end)
end

function gridpanel:select_down(start, reset)
    return self:_select_by(start, reset, function(current)
        return self:down(current)
    end)
end

-- select the first view
function gridpanel:select_first(start, reset)
    return self:_select_by(start, reset, function(_)
        return self:first()
    end)
end

-- select the last view
function gridpanel:select_last(start, reset)
    return self:_select_by(start, reset, function(_)
        return self:last()
    end)
end

function gridpanel:_select_by(start, reset, func)
    -- is empty?
    if self:empty() then
        return
    end

    -- reset?
    if reset then
        self._CURRENT = nil
    end

    -- get current view
    local current = start or self:current()
    local v = func(current)
    if v then
        return self:select(v)
    end
end

function gridpanel:left(v)
    return self._VIEWS:left(v)
end

function gridpanel:right(v)
    return self._VIEWS:right(v)
end

function gridpanel:down(v)
    return self._VIEWS:down(v)
end

function gridpanel:up(v)
    return self._VIEWS:up(v)
end

return gridpanel
