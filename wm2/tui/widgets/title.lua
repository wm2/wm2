-- The 'title' widget prints the title of a view.

local ltui = require("ltui")

local title = title or ltui.textarea()

function title:init(y_offset, panel, p)
    self.y_size = 2
    ltui.textarea.init(
        self,
        "title." .. tostring(p),
        ltui.rect({ 1, y_offset, panel:width(), y_offset + self.y_size }),
        "details of " .. tostring(p)
    )
    self:textattr_set("black bold")
end

return title
