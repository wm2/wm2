-- Define TUI widgets to be shown in P:view().

local widgets = {}

widgets.distribute = function(p, panel, widgets)
    local y_offset = 0
    for _, widget in ipairs(widgets) do
        local w = widget:new(y_offset, panel, p)
        panel:insert(w)
        if w.y_size == nil then
            error("y_size of widget " .. tostring(w) .. "is nil")
        end
        y_offset = y_offset + w.y_size
    end
end

widgets.title = require("wm2.tui.widgets.title")
widgets.info = require("wm2.tui.widgets.info")
widgets.ampmeter = require("wm2.tui.widgets.ampmeter")

return widgets
