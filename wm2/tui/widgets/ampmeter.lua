local class = require("wm2.lib.middleclass")
local Process = require("wm2.process")
local ltui = require("ltui")
local utils = require("wm2.utils")

local SetAmpProcess = class("SetAmpProcess", Process)

function SetAmpProcess:initialize(panel, p)
    self.panel = panel
    self.p = p
    self.view_table = {}
    self.x0_bar = 10 -- space needed for text
    self.maxwidth = self.panel:width() - self.x0_bar - 4
    self.min_value = self.p.min_monitoring_value
    self.max_value = self.p.max_monitoring_value
    Process.initialize(self, "print v process - " .. tostring(p))
end

function SetAmpProcess:iteration1()
    status, errormsg = pcall(self.p.monitoring_values, self.p)
    if not status then
        self:warn("couldn't retrieve monitoring values of P '" .. tostring(self.p.id) .. "': " .. errormsg)
        self:defer_sleep(5)
        return
    end

    self.panel:clear()

    local x0_text = 0

    local titletext = "Monitoring values of " .. tostring(self.p)
    local title = ltui.textarea:new("title", ltui.rect({ x0_text, 1, self.panel:width(), self.y_title }), titletext)
    title:textattr_set("black")
    self.panel:insert(title)

    for i, v in ipairs(errormsg) do
        local percentage = utils.scale(v, self.min_value, self.max_value, 0, 1)
        local barcolor = "yellow"
        if percentage > 1 then
            -- Don't print too long bar in case our signal is bigger than
            -- our expected range -- we rather change the color to indicate
            -- the problem.
            percentage = 1
            barcolor = "red"
        elseif percentage < 0 then
            -- In case our value is smaller than the expected range, we
            -- use a different colour to warn the user.
            barcolor = "red"
        end

        local width = math.floor(self.maxwidth * percentage)
        if width <= 0 then
            width = 1
        end
        -- In case of very small control values, still show to the user that
        -- there is a signal by always adding + 1.
        if v > self.min_value then
            width = width + 1
        end
        local y0 = SetAmpProcess.gety0(i)
        local y1 = SetAmpProcess._gety1(y0, i)
        local view = ltui.view:new("v" .. i, ltui.rect({ self.x0_bar, y0, width + self.x0_bar, y1 }))
        view:background_set(barcolor)
        self.panel:insert(view)

        -- Amp text has two purposes:
        --   1 show channel index
        --   2 print RMS value as a number: this is a very useful for
        --     quiet signals, which activity couldn't be notified otherwise
        local vtext = "chn: " .. i .. "\n" .. tostring(v):sub(0, 8)
        local textarea = ltui.textarea:new("vtext" .. i, ltui.rect({ x0_text, y0, self.x0_bar - 1, y1 }), vtext)
        self.panel:insert(textarea)
    end

    self:defer_sleep(0.05)
end

-- Calculation of size

SetAmpProcess.y_meter = 2
SetAmpProcess.y_title = 2

function SetAmpProcess.gety0(i)
    return ((i - 1) * (SetAmpProcess.y_meter + 1)) + 1 + SetAmpProcess.y_title
end

function SetAmpProcess.gety1(i)
    return SetAmpProcess._gety1(SetAmpProcess.gety0(i), i)
end

function SetAmpProcess._gety1(y0, i)
    return y0 + SetAmpProcess.y_meter
end

local ampmeter = ampmeter or ltui.panel()

function ampmeter:init(y_offset, panel, p)
    self.y_size = SetAmpProcess.gety1(p.channel_count)
    local bounds = ltui.rect({ 1, y_offset, panel:width(), y_offset + self.y_size })
    ltui.panel.init(self, "ampmeter." .. tostring(p), bounds, false, 0)
    self.process = SetAmpProcess:new(self, p)
    self.process:register()
end

function ampmeter:quit()
    self.process:unregister()
    self:clear()
end

return ampmeter
