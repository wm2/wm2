-- 'info' widget logs information about the programs state.

local ltui = require("ltui")
local class = require("wm2.lib.middleclass")
local Process = require("wm2.process")

local SetInfoProcess = class("SetInfoProcess", Process)

function SetInfoProcess:initialize(textarea, p)
    self.textarea = textarea
    self.p = p
    Process.initialize(self, "set info process - " .. tostring(p))
end

function SetInfoProcess:iteration1()
    local p = self.p
    local state = p:is_started() and "started" or "stopped"
    local requesters = ""
    for req, _ in pairs(p.state.requesters or {}) do
        requesters = tostring(req) .. "; " .. requesters
    end
    local runtime_arg = ""
    for k, v in pairs(p.state.runtime_arg or {}) do
        runtime_arg = runtime_arg .. tostring(k) .. "=" .. tostring(v) .. "; "
    end
    self.textarea:text_set(string.format(
        [[
state:          %s
runtime_arg:    %s
requesters:     %s
]],
        state,
        runtime_arg,
        requesters
    ))
    self:defer_sleep(0.1) -- don't waste CPU
end

local info = info or ltui.textarea()

function info:init(y_offset, panel, p)
    self.y_size = 4
    local rect = ltui.rect({ 1, y_offset, panel:width(), y_offset + self.y_size })
    ltui.textarea.init(self, "info." .. tostring(p), rect, "")
    self.process = SetInfoProcess:new(self, p)
    self.process:register()
end

function info:quit()
    self.process:unregister()
end

return info
