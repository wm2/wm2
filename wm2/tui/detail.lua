-- detail is a panel that is temporarily hiding the grid-view to show details of a program.

local ltui = require("ltui")

local detail = detail or ltui.panel()

function detail:init(tui, bounds)
    self.tui = tui
    ltui.panel.init(self, "wm2.tui.detail", bounds, false, 0)
    self:background_set("white")
end

function detail:set_p(p)
    p:view(self)
end

function detail:on_event(e)
    if e.type == ltui.event.ev_keyboard then
        local k = e.key_name
        if k == "Enter" or k == "Esc" or k == "q" then
            self:quit()
        end
    end
end

function detail:quit()
    for v in self:views() do
        if v.quit then
            status, msg = pcall(v.quit, v)
            if not status then
                self.tui:warn("error when quitting widget '" .. tostring(v) .. "': " .. msg)
            end
        end
    end
    self:clear()
    self.tui.app:set_mode("grid")
end

return detail
