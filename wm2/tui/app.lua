local class = require("wm2.lib.middleclass")
local Process = require("wm2.process")
local ltui = require("ltui")
local paginator = require("wm2.tui.paginator")
local detail = require("wm2.tui.detail")

local app = app or ltui.application()

function app:init()
    self.tui:debug("initialize TUI app")
    self.app_process = app.Process:new(self):register()

    ltui.application.init(self, "app")
    self:background_set("black")

    -- The app memories which was the last played ptoggle,
    -- to support the feature to play the next toggle and simultaneously
    -- stop the previous one.
    self.last_activated_ptoggle = nil

    local bounds = ltui.rect({ 0, 0, self:width(), self:height() })
    self.paginator = paginator:new(self.tui, bounds, self.row_count, self.column_count)
    self.detail = detail:new(self.tui, bounds)

    -- The app always only shows one main panel. However
    -- it can switch between different main panels. Each
    -- panel type is described as a mode. The two currently
    -- implemented modes are 'grid' and 'detail'. The 'grid'
    -- mode is the default mode: it shows a grid view of
    -- all programs. We can switch to the 'detail' mode when
    -- being inside the 'grid' mode. The 'detail' mode
    -- temporarily provides more details about one specific
    -- program. When it's closed, the app automatically
    -- switches back to the 'grid' mode.
    self.modes = {}
    self.modes.grid = self.paginator
    self.modes.detail = self.detail

    self:set_mode("grid")
end

function app:current_mode()
    return self._current_mode
end

function app:set_mode(mode)
    self.tui:debug("set mode to '" .. mode .. "'")
    local current_mode = self:current_mode()
    if mode == current_mode then
        return
    end
    if current_mode then
        local panel = self.modes[current_mode]
        if panel then
            self:remove(panel)
        end
    end
    self:insert(self.modes[mode])
    self._current_mode = mode
end

-- Users can optionally specify the page layout with the
-- 'row_count' and 'column_count' options. If unset, the
-- app auto-calculates the layout according to the apps
-- size (usually the display size). By default they are
-- unset. Setting them is used in tests.
function app:new(tui, row_count, column_count)
    self.row_count = row_count
    self.column_count = column_count
    self.tui = tui
    return ltui.application.new(self)
end

function app:quit()
    self.app_process:unregister()
    ltui.application.quit(self)
end

app.Process = class("AppProcess", Process)

function app.Process:initialize(app)
    self.app = app
    Process.initialize(self, "tui app process")
end

-- mostly copied from ltui.program.loop:
-- https://github.com/tboox/ltui/blob/285053d4f/src/ltui/program.lua#L212
-- wm2 uses it's own main loop (in Server:loop) and therefore can't use the
-- loop implementation of ltui. Instead of this we add the tui loop as a process
-- to the wm2 process registry.
function app.Process:iteration1()
    -- get the current event
    e = self.app:event()

    -- do event
    if e then
        ltui.event.dump(e)
        self.app:on_event(e)
        sleep = false
    else
        -- do idle event
        self.app:on_event(ltui.event.idle())
        sleep = true
    end

    -- quit?
    if e and ltui.event.is_command(e, "cm_quit") then
        return 0
    end

    -- resize views
    if self.app:state("resize") then
        self.app:on_resize()
    end

    -- draw views
    self.app:on_draw()

    -- refresh views
    if self.app:state("refresh") then
        self.app:on_refresh()
    end

    -- wait some time
    if sleep then
        return 2
    else
        return 1
    end
end

return app
