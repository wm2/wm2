local class = require("wm2.lib.middleclass")
local Process = require("wm2.process")
local ltui = require("ltui")
local command_textedit = require("wm2.tui.command_textedit")

local command_dialog = common_dialog or ltui.inputdialog()

-- class variable and not instance variable, because instance is destroyed once
-- text is proceeded
command_dialog.previous_text = nil

function command_dialog:init(app, bounds)
    self.app = app
    self.printamp_programs = {}
    ltui.inputdialog.init(self, "dialog.input", bounds)
end

function command_dialog:textedit()
    if not self._TEXTEDIT then
        self._TEXTEDIT = command_textedit:new(
            self,
            "command_dialog.command_textedit",
            ltui.rect({ 0, 1, self:panel():width(), self:panel():height() - 1 })
        )
    end
    return self._TEXTEDIT
end

function command_dialog:on_event(e)
    if e.type == ltui.event.ev_keyboard then
        if e.key_name == "Enter" then
            self:quit()
        end
    end
end

function command_dialog:quit()
    local text = self._TEXTEDIT:text()
    if text then
        command_dialog.previous_text = text
    end
    -- Here are all valid commands
    if text == "q" or text == "quit" then
        self.app.tui.server:quit()
    elseif text == "restart" then
        self.app.tui.server:restart()
    elseif string.sub(text, 1, 5) == "setdb" then
        self:_with_p(text, "setdb", function(p, db)
            local status, errormsg = pcall(p.setdb, p, db)
            if not status then
                self.app.tui:warn("error when calling 'setdb' on '" .. tostring(p.id) .. "': " .. errormsg)
            end
        end)
    elseif string.sub(text, 1, 4) == "page" then
        local pindex_string = string.sub(text, 6, -1)
        local status, errormsg = pcall(tonumber, pindex_string)
        if not status then
            return self.app.tui:warn("can't set page to non-number " .. pindex_string)
        end
        self.app:select_page(errormsg)
    else
        self.app.tui:warn("Unknown command '" .. text .. "'")
    end
    return ltui.inputdialog.quit(self)
end

function command_dialog:_with_p(text, cmdname, func)
    local is_first = true
    local id = nil
    local argv = get_argv(text)
    local id = argv[2]
    local p = self.app.tui.server.patch.programs[id]
    if p then
        status, errormsg = pcall(func, p, unpack(argv, 3))
        if not status then
            self.app.tui:warn(cmdname .. " failed with error: " .. errormsg)
        end
    else
        self.app.tui:warn(cmdname .. ": couldn't find P with ID '" .. id .. "'")
    end
end

function get_argv(text)
    argv = {}
    for t in string.gmatch(text, "[^ ]+") do
        table.insert(argv, t)
    end
    return argv
end

return command_dialog
