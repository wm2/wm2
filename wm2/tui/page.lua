-- Define main window.
local ltui = require("ltui")

local command_dialog = require("wm2.tui.command_dialog")
local gridpanel = require("wm2.tui.gridpanel")

local page = page or ltui.window()

function page:init(app, name, bounds, title, row_count, ptoggles, index)
    self.index = index
    self.app = app
    self.ptoggles = ptoggles
    self.row_count = row_count

    ltui.window.init(self, name, bounds, title, 0)

    for _, pt in ipairs(ptoggles) do
        self:panel():insert(pt)
    end
end

-- Adjusted version of ltui.window.panel: We don't use
-- ltui.panel, but wm2.tui.panel.
function page:panel()
    if not self._PANEL then
        self._PANEL = gridpanel:new("window.panel", self:frame():bounds(), self.app.tui, self.row_count)
        self._PANEL:bounds():grow(-1, -1)
        self._PANEL:invalidate(true)
    end
    return self._PANEL
end

function page:on_event(e)
    if e.type == ltui.event.ev_keyboard then
        local k = e.key_name
        if k == ":" then
            local y = 6
            self:insert(
                command_dialog:new(self.app, ltui.rect({ 0, self:height() - y, self:width(), self:height() })),
                {
                    centerx = false,
                    centery = false,
                }
            )
        end
    end
end

return page
