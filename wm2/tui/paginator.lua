-- paginator is a panel that provides different pages of grids.

local ltui = require("ltui")
local ptoggle = require("wm2.tui.ptoggle")
local page = require("wm2.tui.page")

local paginator = paginator or ltui.panel()

function paginator:init(tui, bounds, row_count, column_count)
    self.tui = tui
    self.row_count = row_count
    self.column_count = column_count
    ltui.panel.init(self, "wm2.tui.paginator", bounds)
    self:_setup_pages()
end

function paginator:on_event(e)
    if e.type == ltui.event.ev_keyboard then
        local k = e.key_name
        for number, snumber in ipairs(self._numbers) do
            if snumber == k then
                return self:select_page(number)
            end
        end
    end
end

function paginator:_setup_pages()
    self.tui:debug("setup pages")

    local page_margin = 1
    local page_width = self:width() - (page_margin * 2)
    local page_height = self:height() - (page_margin * 2)

    local page_rect = ltui.rect({ page_margin, page_margin, page_width + page_margin, page_height + page_margin })

    local ptoggle_width = 20
    local ptoggle_height = 4

    local row_count = self.row_count or math.floor(page_width / ptoggle_width)
    local column_count = self.column_count or math.floor(page_height / ptoggle_height)

    local page_ptoggle_count = row_count * column_count

    self.pages = {}
    for page_start = 0, self.tui.server.patch.p_count, (page_ptoggle_count + 1) do
        local x = 0
        local y = 0
        local i = 0
        local ptoggles = {}
        for p_index_offset = 0, page_ptoggle_count do
            local p = self.tui.server.patch.sorted_programs[page_start + p_index_offset]
            if p then
                local id = p.id
                self.tui:debug("Add " .. tostring(id) .. " to TUI")
                local pt = ptoggle:new(self, self.tui, id, ltui.rect:new(x, y, ptoggle_width, ptoggle_height))
                table.insert(ptoggles, pt)
                i = i + 1
                if i % row_count == 0 then
                    x = 0
                    y = y + ptoggle_height
                else
                    x = x + ptoggle_width
                end
            end
        end
        local pnumber = #self.pages + 1
        local p =
            page:new(self, "page.main", page_rect, "wm2: page " .. tostring(pnumber), row_count, ptoggles, pnumber)
        table.insert(self.pages, p)
    end

    self:select_page(1)
end

function paginator:current_page()
    return self._current_page
end

function paginator:select_page(page_index)
    self.tui:debug("select page " .. tostring(page_index))

    local p = self.pages[page_index]
    if not p then
        -- This is an expected case and shouldn't raise any warning or
        -- error.
        return
    end
    if self._current_page then
        if self._current_page_index == page_index then
            return
        end
        self:remove(self._current_page)
    end
    self._current_page = p
    self._current_page_index = page_index
    self:insert(self._current_page)
end

paginator._numbers = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }

return paginator
