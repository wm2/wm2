local ltui = require("ltui")

local luajit, bit = pcall(require, "bit")
if not luajit then
    bit = require("ltui/base/bit")
end

local command_textedit = command_text_edit or ltui.textedit()

function command_textedit:init(command_dialog, name, bounds, text)
    self.command_dialog = command_dialog
    ltui.textedit.init(self, name, bounds, text)
end

-- on event
-- adapted version of original 'textedit.on_event':
--  - we don't allow multiline text, but 'Enter' means execute the
--    command (just as in vi)
function command_textedit:on_event(e)
    -- update text
    if e.type == ltui.event.ev_keyboard then
        if e.key_name == "Enter" then
            self.command_dialog:quit()
            return true
        elseif e.key_name == "Esc" then
            -- We don't proceed our written text in case of escape
            self:text_set("")
            self.command_dialog:quit()
            return true
        elseif e.key_name == "Backspace" then
            local text = self:text()
            if #text > 0 then
                local size = 1
                -- while continuation byte
                while bit.band(text:byte(#text - size + 1), 0xc0) == 0x80 do
                    size = size + 1
                end
                self:text_set(text:sub(1, #text - size))
            end
            return true
        elseif e.key_name == "CtrlV" then
            local pastetext = os.pbpaste()
            if pastetext then
                self:text_set(self:text() .. pastetext)
            end
            return true
        elseif e.key_code > 0x1f and e.key_code < 0xf8 then
            self:text_set(self:text() .. string.char(e.key_code))
            return true
        elseif e.key_name == "Up" then -- get previous text
            local previous_text = self.command_dialog.previous_text
            if previous_text then
                self:text_set(previous_text)
            end
        end
    end

    -- do textarea event
    return ltui.textarea.on_event(self, e)
end

return command_textedit
