-- 'Q' (cue) implements the functionality to start/stop/switch one or more P
-- in one request. A 'Q' instance provides the init-time argument
-- 'p' that is a table which keys are ids and which values are their
-- runtime state.

local class = require("wm2.lib.middleclass")
local P = require("wm2.p")
local utils = require("wm2.utils")

local Q = class("Q", P)

function Q:initialize(replication_key, options)
    self.p = table.pop(options, "p") or {}
    P.initialize(self, replication_key, options)
end

function Q:dependencies()
    if self:is_bound() == false then
        error("Can't return dependencies of unbound cue!")
    end
    local dependencies = {}
    for id, runtime_arg in pairs(self.p) do
        local p = self.server.patch.programs[tostring(id)]
        if not p then
            self:warn("Couldn't find P '" .. tostring(id) .. "' in bound server/patch!")
        else
            table.insert(dependencies, { requester = self.id, id = id, runtime_arg = runtime_arg })
        end
    end
    return dependencies
end

P.REGISTRY:register("q", Q)

return Q
