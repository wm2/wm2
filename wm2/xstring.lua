-- 'xstring' extends builtin 'string' namespace with extra functions

-- Split that preserves empty lines, see https://stackoverflow.com/a/51893646
--
-- We can't call the function 'split', but must call it 'split2', because
-- there is already a 'split' implemented in 'ltui' [1]. The ltui split doesn't
-- preserve empty lines. Overriding this may potentially break something in
-- ltui - something that should be avoided. Therefore we just define 'split2'.
--
-- [1] https://github.com/tboox/ltui/blob/6933a4363/src/ltui/base/string.lua#L58-L68
function string:split2(delimiter)
    local result = {}
    local from = 1
    local delim_from, delim_to = self:find(delimiter, from)
    while delim_from do
        table.insert(result, self:sub(from, delim_from - 1))
        from = delim_to + 1
        delim_from, delim_to = self:find(delimiter, from)
    end
    table.insert(result, self:sub(from))
    return result
end
