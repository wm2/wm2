-- 'xtable' extends builtin 'table' namespace with extra functions

function table.shallow_copy(t)
    local t2 = {}
    for k, v in pairs(t) do
        t2[k] = v
    end
    return t2
end

-- 'table.eq' tests whether to tables are equal or
-- not. If they are equal, 'true' is returned and
-- otherwise 'false'
--
-- See https://stackoverflow.com/questions/20325332/how-to-check-if-two-tablesobjects-have-the-same-value-in-lua
function table.eq(o1, o2, ignore_mt)
    if o1 == o2 then
        return true
    end
    local o1Type = type(o1)
    local o2Type = type(o2)
    if o1Type ~= o2Type then
        return false
    end
    if o1Type ~= "table" then
        return false
    end

    if not ignore_mt then
        local mt1 = getmetatable(o1)
        if mt1 and mt1.__eq then
            --compare using built in method
            return o1 == o2
        end
    end

    local keySet = {}

    for key1, value1 in pairs(o1) do
        local value2 = o2[key1]
        if value2 == nil or table.eq(value1, value2, ignore_mt) == false then
            return false
        end
        keySet[key1] = true
    end

    for key2, _ in pairs(o2) do
        if not keySet[key2] then
            return false
        end
    end
    return true
end

-- 'table.pop' pops value with 'key' from table
function table.pop(table, key)
    if not table then
        return
    end
    local value = table[key]
    table[key] = nil
    return value
end
