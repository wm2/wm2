-- This file contains patches that change the behaviour of builtin lua functions.

-- In Lua 5.2 users can define the meta method '__pairs' on objects, in Lua 5.1
-- this doesn't work yet. However we need a custom '__pairs' method for
-- 'wm2/lib/orderedtable.lua', therefore we apply this patch.
local _p = pairs
function pairs(t, ...)
    local metatable = getmetatable(t)
    return (metatable and metatable.__pairs or _p)(t, ...)
end
