-- P is a small program that can be started & stopped.
--
-- It consists of two other objects:
--
--  (1) P.Id encapsulates the unique identity of a P instance.
--  (2) P.State encapsulates the current state of a P instance.

local class = require("wm2.lib.middleclass")
local utils = require("wm2.utils")
local inspect = require("wm2.lib.inspect")
local Registry = require("wm2.registry")
local WM2Object = require("wm2.wm2object")
local ltui = require("ltui")
local widgets = require("wm2.tui.widgets")

local P = class("P", WM2Object)

-- The lifecyle of a P is defined by steps, where each step depends
-- on the previous step:
--
-- (1) Init time
--
--     P:initialize(replication_key, options)
--
--     This happens before a P is added to a patch. Here
--     a P doesn't know anything yet about a specific server,
--     it just exists as a declaration.
--
-- (2) Bind time
--
--     P:bind(server)
--
--     This happens inside 'Server.start'. Now a P can process
--     all explicit values where it needs the server to depend on.
--     As long as a P is bounded, it can't be bound again. So
--     calling 'server:start' twice with the same patch should break.
--
-- (3) State setting time
--
--     P:set_state(options)
--
--     Sets the runtime state of a P. Unlike initialization
--     arguments they can still be changed on a existing instance
--     before each start call
--
-- (4) Start time
--
--     P:start()
--
--     A P is running. Arguments passed here are runtime arguments.
--     From here, we can only move to (5), to (4) or to (3). If we
--     want to move to (4) when the instance is already started (e.g.
--     when we already called 'start' on this instance), we need to
--     use the function 'switch'. Switch allows to re-start a running
--     instance with new parameters. The following is a valid sequence
--     of a bound instance:
--
--       P:set_state(options0)
--       P:start()
--       P:set_state(options1)
--       P:switch()
--
-- (5) Stop time
--
--     P:stop()
--
--     Program stops.
--
-- (6) State unsetting time
--
--     P.state:unset()
--
--     Program state is unset. Can go to (3) or (7).
--
-- (7) Unbind time
--
--     P:unbind()
--
--     This drops the attachment to the server, all data
--     is cleaned up. Now we can only go to (2) or (3).
--
function P:initialize(replication_key, options)
    -- Assert necessary arguments are set
    utils.isarg(replication_key, "replication_key")
    utils.unused_options(options, function(msg)
        self:warn(msg)
    end)

    -- Hooks help to simplify complex interactions between P.
    self.start_hooks = {}
    self.stop_hooks = {}

    self.id = P.Id:new(self.class.name, replication_key)
    self.state = P.State:new()

    WM2Object.initialize(self)
end

-- Bind P to a specific server. Before P isn't bound it can't be
-- started.
function P:bind(server)
    self:debug("bind")
    if self:is_bound() then
        return error("P is already bound to server")
    end
    utils.isarg(server, "server")
    self.server = server
end

-- Unbind P from server.
function P:unbind()
    self:debug("unbind")
    if self:is_started() then
        return error("Can't unbind started P")
    end
    if not self:is_bound() then
        return error("P isn't bound yet")
    end
    self.server = nil
end

-- Test if P is already bound to server or not yet.
function P:is_bound()
    return self.server ~= nil
end

-- Start program
function P:start()
    self:debug("start")
    if not self:is_bound() then
        error("Can't start not-yet bound P")
    end
    if self:is_started() then
        error(tostring(self) .. " is already playing")
    end
    self._started = true

    local errbank = utils.errbank()
    for _, h in ipairs(self.start_hooks) do
        errbank:call("start hook", h)
    end
    errbank:raise() -- raise any errors that happened during start
end

-- Apply switched runtime_arg while not stopping / when already playing
function P:switch()
    if not self:is_started() then
        error(tostring(self) .. " is stopped")
    end
end

-- Stop program
function P:stop()
    self:debug("stop")
    if not self:is_started() then
        error(tostring(self) .. " is already stopped")
    end
    self._started = false

    local errbank = utils.errbank()

    -- Children may still need set state to stop themselves,
    -- therefore put children _stop into extra method.
    errbank:call("_stop", self._stop, { self })

    for _, h in ipairs(self.stop_hooks) do
        errbank:call("stop hook", h)
    end

    errbank:raise() -- raise any errors that happend during stop
end

-- For subclasses
function P:_stop() end

-- 'true' if playing and 'false' otherwise
function P:is_started()
    return self._started or false
end

function P:__tostring()
    return self.class.name .. "(" .. tostring(self.id) .. ")"
end

-- Return table that lists all programs which this P is starting
-- with which requester. The 'dependencies' function must not
-- return the dependencies of the programs dependencies (e.g. all
-- nested dependencies). This is important to guarantee a functioning
-- program conducting mechanism.
function P:dependencies()
    return {}
end

-- Set runtime state of P
function P:set_state(options)
    self.state.runtime_arg = options
    self:debug("set runtime arg to " .. tostring(inspect(self.state.runtime_arg)))
end

-- 'view' opens details view of program in TUI.
function P:view(panel)
    widgets.distribute(self, panel, self:view_widgets())
end

-- 'view_widgets' returns list of all used widgets in program view.
function P:view_widgets()
    return { widgets.title, widgets.info }
end

-- Id encapsulates the Identity of a P
P.Id = class("Id", WM2Object)

function P.Id:initialize(p_type, replication_key)
    utils.isarg(p_type, "p_type")
    utils.isarg(replication_key, "replication_key")
    self.p_type = p_type:lower()
    self.replication_key = replication_key
    WM2Object.initialize(self)
end

function P.Id:__tostring()
    -- NOTE: We use delimiter '_', because this is allowed in
    -- Csound instrument names.
    return self.p_type .. "_" .. self.replication_key
end

function P.Id:__eq(other)
    return self.p_type == other.p_type and self.replication_key == other.replication_key
end

-- Load an P.Id from an identity string representation.
-- A P.Id string identity is composed of
-- $CLASSNAME_$REPLICATIONKEY.
function P.Id.from_string(id_string)
    local p_type, replication_key = nil, nil
    for w in string.gmatch(id_string, "([^_]+)") do
        if p_type then
            replication_key = w
            break
        else
            p_type = w
        end
    end
    return P.Id:new(p_type, replication_key)
end

-- P.State hosts the current state of a P
-- inside WM2. It is composed of two attributes:
--
--  (1) runtime_arg: This table hosts the runtime arguments
--          of the program. If no runtime arguments are set,
--          this must be nil.
--
--  (2) requesters: This table hosts which 'P' requested
--          the start of the 'P'. If this table is empty, the
--          'P' should be stopped.
P.State = class("P.State", WM2Object)

function P.State:initialize()
    self:reset()
    WM2Object.initialize(self)
end

function P.State:reset()
    self.runtime_arg = nil
    self.requesters = {}
end

-- 'copy' copies programs state into a new state object.
function P.State:copy()
    local state = P.State()
    if self.runtime_arg then
        state.runtime_arg = table.shallow_copy(self.runtime_arg)
    end
    state.requesters = table.shallow_copy(self.requesters)
    return state
end

-- 'any_requester' returns true in case the state is requested
-- by any other program and false if not. This is useful to find
-- out from a programs state whether the program is started or not.
function P.State:any_requester()
    for _, __ in pairs(self.requesters) do
        return true
    end
    return false
end

function P.State:__eq(other)
    if self.runtime_arg == nil or other.runtime_arg == nil then
        runtime_arg_eq = self.runtime_arg == other.runtime_arg
    else
        runtime_arg_eq = table.eq(self.runtime_arg, other.runtime_arg)
    end
    return table.eq(self.requesters, other.requesters) and runtime_arg_eq
end

function P.State:__tostring()
    return "P.State(runtime_arg = " .. inspect(self.runtime_arg) .. "; requesters = " .. inspect(self.requesters) .. ")"
end

-- P.REGISTRY tracks all available P types. This is needed
-- to parse patches. User defined P types can be registered
-- with the global 'wm2.P.REGISTRY' instance.

local PRegistry = class("PRegistry", Registry)

function PRegistry:initialize(options)
    options = options or {}
    options.type_name = "p_type"
    Registry.initialize(self, options)
end

P.REGISTRY = PRegistry:new() -- Singleton

return P
