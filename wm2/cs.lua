-- 'cs' module provides Csound based P

local luaCsnd6 = require("luaCsnd6")
local class = require("wm2.lib.middleclass")
local utils = require("wm2.utils")
local etlua = require("wm2.lib.etlua")
local P = require("wm2.p")
local WM2Object = require("wm2.wm2object")
local Process = require("wm2.process")
local widgets = require("wm2.tui.widgets")

local instrvar = function (rate, name) return rate .. "wm2__" .. name end

local cs = {}

cs._template_path = debug.getinfo(1).source:match("@?(.*/)") .. "_cs/"

-- cs.orc provides functionality to build csound orchestra
cs.orc = {}

-- 'build' builds a csound orchestra from a wm2.Patch
function cs.orc.build(patch)
    local env = table.shallow_copy(cs.orc.env)
    env.instruments = patch.sorted_programs
    local result, err = etlua.render(cs.orc.template, env)
    if err ~= nil then
        error(err)
    end
    return result
end

cs.orc.template = utils.read_file(cs._template_path .. "cs.orc")
cs.orc.env = {
    var_aempty = instrvar("a", "empty"),
    var_kempty = instrvar("k", "empty")
}

-- cs.instr implements a small DSP unit by using Csound.
cs.instr = class("instr", P)

-- option arguments:
--     instr: csound instrument code definition
--     preamble: csound code before instrument definition
--     audio_input_count: declares how many audio inputs the instr expects. If
--          the 'audio_inputs' table is smaller, it appends 'empty' entries
--          to the table. If the table is bigger, it warns the user.
--     audio_inputs: a table of audio inputs => they should be the names of audio
--          channels
--     control_input_count: declares how many control inputs the instr expects.
--     control_inputs: a table of control inputs
--     rate: Can be either 'arate' or 'krate', depending on of the user wants
--          to define cs that create control or audio signals.
--     channel_count: how many audio channels this csound instrument contains
--     fade_in_duration: duration of fade in. If set to 0, no fade in is executed.
--          Default to 0.
--     fade_out_duration: duration of fade out. If set to 0, no fade out is executed.
--          Default to 0.
--     min_monitoring_value: min scale value for monitoring signal.
--     max_monitoring_value: max scale value for monitoring signal.
--     absolute_monitor_signal: If absolute value should be calculated from
--          monitoring signal.
--     docleanup: If set to true, signal is set to 0 after it's stopped.
--     withdb: If set to true, a db channel is added with which the output
--          signal is multiplied. Can be useful to control the instruments
--          volume from the TUI.
--
-- Adds implicit contract to auto-call each audio input 'ainputN' and each
-- control input 'kinputN' in your csound instrument. So if you want to use
-- your inputs, you can refer to them with 'ainputN' or 'kinputN'.
--
-- NOTE We deliberately only allow 'krate' or 'arate' output in order to
-- simplify instr implementation.

function cs.instr:initialize(replication_key, options)
    function p(v)
        return table.pop(options, v)
    end

    replication_key = tostring(replication_key) -- in case we just use a number, it may not be str yet

    -- Prevent csound error due to special character in instrument name.
    -- Only alphanumerical values are supported.
    local getnotalpha = replication_key:gmatch("%W")
    char = getnotalpha()
    while char do
        if char ~= "_" then
            error("found prohibited char '" .. char .. "' in replication_key '" .. replication_key .. "': this is unsupported by csound!")
        end
        char = getnotalpha()
    end

    self._csound_instrument = p("instr")
    self._csound_preamble = p("preamble") or ""
    self.audio_input_count = p("audio_input_count") or 0
    self.audio_inputs = p("audio_inputs") or {}
    self.control_input_count = p("control_input_count") or 0
    self.control_inputs = p("control_inputs") or {}

    self.fade_in_duration = p("fade_in_duration") or 0
    self.fade_out_duration = p("fade_out_duration") or 0
    self.rate = p("rate") or "arate"
    assert(
      self.rate == "arate" or self.rate == "krate",
      "got illegal rate '" .. self.rate .. "': rate must be either 'arate' or 'krate'"
    )
    self.channel_count = p("channel_count") or 0

    -- For control value printing in TUI: in case of 'arate'
    -- this is the RMS of the audio signal due to which a range
    -- from 0 to 1 makes sense. But in case we have a control signal
    -- (for instance a midi control value) that we scale to a different
    -- value set (for instance decibel or frequencies) this may be a
    -- very different value range than 0 to 1. Therefore 'signals' allows
    -- users to set the preferred control value range.
    self.min_monitoring_value = p("min_monitoring_value") or 0
    self.max_monitoring_value = p("max_monitoring_value") or 1

    -- 'absolute_monitoring_signal' tells whether the absolute value of the
    -- monitoring signal is send or not. In case we use RMS with 'arate',
    -- we don't need this as RMS is always a positive number anyway.
    -- For some control signals this may be useful.
    self.absolute_monitor_signal = p("absolute_monitor_signal") or false

    -- Setting 'docleanup' to 'true' activates a mechanism in
    -- Csound to multiply all signals that this instrument hosts with 0
    -- once the instr is stopped (and faded out). This mechanism is
    -- particularly useful for 'arate' instruments, as we usually don't
    -- want any DC to persist once we stopped our instrument. On the other
    -- hand, for 'krate' instrument this may often be undesired as we
    -- usually want to persist the same value (e.g. control value send to
    -- another input that's used there as a amplitude control).
    self.docleanup = p("docleanup")
    -- In  case this is undefined, we therefore guess the wanted usage
    -- based on the assumption mentioned above.
    if self.docleanup == nil then
        local c = { arate = true, krate = false }
        self.docleanup = c[self.rate]
    end

    -- For an audio instrument it's useful to control its volume with an
    -- external decibel channel (so that a user can control an instruments
    -- volume from the TUI). However for control signals, it's usually
    -- unwanted for signals to be interfered with a decibel multiplication
    -- (we don't want to multiply a midi value to control an instruments pitch
    -- to be multiplied with decibel). Therefore by default, 'withdb' is set
    -- to 'true' for 'arate' signals and to 'false' for 'krate' signals.
    self.withdb = p("withdb")
    if self.withdb == nil then
        local c = { arate = true, krate = false }
        self.withdb = c[self.rate]
    end

    self:reset_channels()

    -- Define contract: signal names should always be 'aoutputN' or 'koutputN'
    local prefix_mapping = { arate = "a", krate = "k" }
    local prefix = prefix_mapping[self.rate]
    local base_sig_name = "output"
    local signame = prefix .. base_sig_name

    -- We automatically post-proceed all signals to implement common needs
    local postsigname = instrvar(prefix, base_sig_name .. "p")

    self.signals = {}
    self.postsignals = {}
    for i = 0, self.channel_count do
        local signal_name = signame .. (i - 1)
        self.signals[i] = signal_name
        local postsignal_name = postsigname .. (i - 1)
        self.postsignals[i] = postsignal_name
    end

    P.initialize(self, replication_key, options)

    self.audio_inputs = self:_fixinputs("audio", cs.orc.env.var_aempty, self.audio_inputs, self.audio_input_count)
    self.control_inputs = self:_fixinputs("control", cs.orc.env.var_kempty, self.control_inputs, self.control_input_count)
end

function cs.instr:bind(server)
    P.bind(self, server)
    -- keeps track of all control channels this instrument hosts
    self._channels = {}

    -- Add channel for setting decibel of instrument.
    -- In this way it becomes easily possible to adjust the
    -- volume of a instrument in the TUI.
    if self.withdb then
        self.db_channel = self:new_channel(tostring(self.id) .. self.env.var_db)
        self:setdb() -- Initial decibel setting
    end

    -- Create audio channels
    -- Audio channels don't have lua channel objects, their information
    -- can't be retrieved in lua. Instead we only know the names of the
    -- audio channels.
    if self.rate == "arate" then
        for i, signal_name in ipairs(self.postsignals) do
            self.audio_channels[signal_name] = tostring(self.id) .. "_" .. self.signals[i]
        end
    -- Create control channels
    else
        for i, signal_name in ipairs(self.postsignals) do
            local d = self:new_channel(tostring(self.id) .. "_" .. self.signals[i])
            self.control_channels[signal_name] = d
        end
    end

    -- Create monitoring channels
    -- We always use dedicated monitoring channels for both rates, as
    -- these monitoring channels need specific properties.
    for i, signal_name in ipairs(self.postsignals) do
        local d = self:new_channel(tostring(self.id) .. "_k" .. self.signals[i]:sub(2) .. "_monitoring")
        self.monitoring_channels[signal_name] = d
    end
end

function cs.instr:unbind()
    P.unbind(self)
    self.db_channel = nil
    self:reset_channels()
end

-- Start csound instrument
function cs.instr:start()
    P.start(self)
    self:send_score()
end

-- Switch csound instrument
function cs.instr:switch()
    P.switch(self)
    self:send_score()
end

-- Stop csound instrument
function cs.instr:_stop()
    self:send_score()
end

-- Send current state to csound server.
function cs.instr:send_score()
    local sco = self:score()
    self:debug("send: '" .. sco .. "'")
    self.server:get_perf_thread():InputMessage(sco)
end

-- One line of a csound score for the current state
function cs.instr:score()
    if self:is_started() then
        return 'i "' .. tostring(self.id) .. '" 0 -1'
    else
        return 'i "-' .. tostring(self.id) .. '" 0 0'
    end
end

-- 'orchestra' returns Csound orchestra code for this instrument
function cs.instr:orchestra()
    return self._orchestra({instr = self})
end

-- The body of Csound instr-endin block.
function cs.instr:instrument_body()
    return self._csound_instrument
end

-- Csound code before instr-endin block
function cs.instr:preamble()
    local preamble = ""
    if self._csound_preamble ~= "" then
        preamble = ";; user declared preamble\n" .. self._csound_preamble
    end

    -- krate channels are created with the 'cs.instr:new_channel' method, but
    -- arate channels need to be created in the cs preamble.
    if self.rate == "krate" then
        return preamble
    end

    local isfirst = true
    for _, channel_name in pairs(self.audio_channels) do
        if isfirst then
            preamble = preamble .. "\n\n" .. ";; (auto)cs.instr:preamble"
            isfirst = false
        end
        preamble = preamble .. "\n" .. 'chn_a "' .. channel_name .. '", 1'
    end

    return preamble .. "\n"
end

-- Creates a new channel
function cs.instr:new_channel(name)
    local d = {}
    d.channel = luaCsnd6.CsoundMYFLTArray(1)
    d.name = name
    self.server.csound:GetChannelPtr(
        d.channel:GetPtr(),
        d.name,
        luaCsnd6.CSOUND_CONTROL_CHANNEL + luaCsnd6.CSOUND_INPUT_CHANNEL
    )
    table.insert(self._channels, d)
    return d
end

-- Fetches all control channels and their current values
function cs.instr:channel_map()
    local channel_map = {}
    for i, c in ipairs(self._channels) do
        table.insert(channel_map, tonumber(c.channel:GetValue(0)))
    end
    return channel_map
end

-- Applies all channels in given channel map
function cs.instr:apply_channel_map(channel_map)
    for i, v in ipairs(channel_map) do
        self._channels[i].channel:SetValue(0, v)
    end
end

-- set main decibel value of instrument
function cs.instr:setdb(db)
    if not self.withdb then
        error("instr '" .. tostring(self.id) .. "' doesn't have a db channel")
    end
    db = db or -6
    self:debug("set db of '" .. tostring(self.id) .. "' to " .. tostring(db))
    self.db_channel.channel:SetValue(0, db)
end

function cs.instr:reset_channels()
    if self:is_bound() then -- may be dangerous while bound as we'd loose pointers
        error("can't reset channels when already bound")
    end
    self.audio_channels = {}
    self.monitoring_channels = {}
    self.control_channels = {}
end

-- 'monitoring_values' returns a table with the current values
-- of the instruments monitoring channels
function cs.instr:monitoring_values()
    local monitoring_values = {}
    -- NOTE: We iterate over 'self.postsignals' instead of
    -- iterating directly over 'self.monitoring_channels' to
    -- always preserve the same order.
    for _, signal_name in ipairs(self.postsignals) do
        local d = self.monitoring_channels[signal_name]
        table.insert(monitoring_values, d.channel:GetValue(0) / cs.instr.env.MONITORING_VALUE_SCALE)
    end
    return monitoring_values
end

-- 'monitoring_value' returns the average value of all monitoring
-- value channels.
function cs.instr:monitoring_value()
    return utils.average(self:monitoring_values())
end

-- Heuristic to check if instrument is currently playing:
-- it simply checks if the last sample is 0. In rare cases
-- it can happen that the instrument is considered silent,
-- while the signal is just passing 0 currently.
function cs.instr:is_silent()
    for _, d in pairs(self.monitoring_channels) do
        if d.channel:GetValue(0) ~= 0 then
            return false
        end
    end
    return true
end

function cs.instr:_fixinputs(input_name, empty_name, inputs, input_count)
    local c = #inputs
    local diff = input_count - c
    if diff > 0 then
        self:warn(tostring(diff) .. " " .. input_name .. " input(s) were missing!")
        for i = c + 1, c + diff do
            inputs[i] = empty_name
        end
    elseif diff < 0 then
        self:warn(tostring(math.abs(diff)) .. " " .. input_name .. " input(s) too much!")
    end
    if diff ~= 0 then
        self:warn("got: " .. tostring(c) .. " input(s), want: " .. tostring(input_count))
    end
    return inputs
end

function cs.instr:view_widgets()
    local w = P.view_widgets(self)
    table.insert(w, widgets.ampmeter)
    return w
end

-- Constants (that are also used for csound orchestra generation)
cs.instr.env = {
    -- Auto-created variables that are used within an instr scope.
    -- (We use Lua variables instead of strings to avoid typo mistakes &
    -- to ensure they follow the same naming convention.)
    var_fade_in_duration = instrvar("i", "fade_in_duration"),
    var_fade_out_duration = instrvar("i", "fade_out_duration"),
    var_fader = instrvar("k", "fader"),
    var_cleanup = instrvar("k", "cleanup"),
    var_db = instrvar("k", "db_main"),
    var_amp = instrvar("k", "amp_main"),
    var_amp_final = instrvar("k", "amp_final"),
    MONITORING_VALUE_SCALE = 100000,
    f = utils.remove_empty_lines -- format auto-generated csound code
}

-- Load 'instrument' macro that is defined in etlua template only once
-- to save RAM/CPU/time
cs.instr._orchestra = etlua.loadmacros(
    utils.read_file(cs._template_path .. "instr.orc"), table.shallow_copy(cs.instr.env)
    ).instrument


-- Csound instrument to handle global time clock
cs.clock = class("clock", cs.instr)

function cs.clock:initialize(replication_key, options)
    self.channel_name = "clock"
    options.instr = [[
  kclock times
  chnset kclock, "clock"
]]
    cs.instr.initialize(self, replication_key, options)
end

function cs.clock:bind(server)
    cs.instr.bind(self, server)
    self.channel = self:new_channel(self.channel_name)
end

function cs.clock:unbind()
    cs.instr.unbind(self)
    self.channel = nil
end

-- 't' returns current time
function cs.clock:t()
    return self.channel.channel:GetValue(0)
end

-- Csound instrument to test output channels
cs.chntest = class("chntest", cs.instr)

function cs.chntest:initialize(replication_key, options)
    self.channel_offset = options.channel_offset or 0
    self.duration = 1
    local instr = etlua.render(
        [[
  idur = {{= duration }}
  ichncount = p4
  ichnoffset = p5
  kchn loopseg 1 / (idur * ichncount), 0, 0, 0, 1, ichncount
  anoise noise 1, 0.5
  anoiseWithSeg = anoise * {{= var_amp }}
  outch ceil(kchn) + ichnoffset, anoiseWithSeg
]],
        { duration = self.duration, var_amp = self.env.var_amp }
    )
    options.instr = instr
    cs.instr.initialize(self, replication_key, options)
end

function cs.chntest:bind(server)
    cs.instr.bind(self, server)
    self.channel_count = self.server.patch.channel_count - self.channel_offset
    self.channeltestp = cs.chntest.PrintProcess(self.duration, self.channel_count, self.channel_offset)
end

function cs.chntest:unbind(server)
    cs.instr.unbind(self)
    self.channel_count = nil
    self.channeltestp = nil
end

function cs.chntest:score()
    local sco = cs.instr.score(self)
    return utils.join(" ", { sco, self.channel_count, self.channel_offset })
end

function cs.chntest:start()
    cs.instr.start(self)
    self.channeltestp:register()
end

function cs.chntest:stop()
    cs.instr.stop(self)
    self.channeltestp:unregister()
end

cs.chntest.PrintProcess = class("PrintProcess", Process)

function cs.chntest.PrintProcess:initialize(duration, channel_count, channel_offset)
    self.duration = duration
    self.channel_count = channel_count
    self.channel_offset = channel_offset
    self.channel_index = 0
    Process.initialize(self, "channel test process")
end

function cs.chntest.PrintProcess:iteration1()
    self:info("current channel: " .. tostring(self.channel_index + self.channel_offset))
    self.channel_index = (self.channel_index + 1) % self.channel_count
    self:defer_sleep(self.duration)
end

function cs.chntest.PrintProcess:unregister()
    self.channel_index = 0
    Process.unregister(self)
    return 1
end

-- Csound instrument for mono audio input.
-- Input channel can be specified via 'channel_index'.
-- A channel index of '1' (default value) is the first
-- Csound input.
cs.in1 = class("in1", cs.instr)

function cs.in1:initialize(replication_key, options)
    self.channel_index = table.pop(options, "channel_index") or 1
    options.instr = etlua.render(
        [[
  aoutput0 inch {{= channel_index }}
]],
        { channel_index = self.channel_index }
    )
    options.channel_count = 1
    cs.instr.initialize(self, replication_key, options)
end

-- Csound instrument for N audio outputs, with builtin protection against
-- too loud signals.
cs.outn = class("outn", cs.instr)

function cs.outn:initialize(replication_key, options)
    -- TODO(maybe it's better if we add a fadeout in case signal becomes too loud,
    -- to avoid a click - this would perhaps be smoother in a performance if something
    -- as bad as this really happens)
    local max_rms = table.pop(options, "max_rms") or 0.1
    local amplitude = table.pop(options, "amplitude") or 1
    local instr = {}
    for i, _ in ipairs(options.audio_inputs or {}) do
        local rms = "kRMSOfInput" .. tostring(i - 1)
        local ainput = "ainput" .. tostring(i - 1)
        local ainputproc = ainput .. "proc"
        table.insert(instr, "  " .. ainputproc .. " = " .. ainput .. " * " .. tostring(amplitude))
        table.insert(instr, "  " .. rms .. " rms " .. ainputproc)
        table.insert(instr, "  if " .. rms .. " < " .. tostring(max_rms) .. " then")
        table.insert(instr, "    outch " .. tostring(i) .. ", " .. ainputproc)
        table.insert(instr, "  endif\n")
        options.audio_input_count = i
    end

    options.instr = utils.join("\n", instr)
    cs.instr.initialize(self, replication_key, options)
end

-- Simple mono sine tone. Useful for testing patches.
cs.sine = class("sine", cs.instr)

function cs.sine:initialize(replication_key, options)
    options.instr = [[
  ifrequency = p4
  iamplitude = p5
  aoutput0 oscil iamplitude, ifrequency
]]
    options.audio_input_count = 0
    options.channel_count = 1
    cs.instr.initialize(self, replication_key, options)
end

function cs.sine:score()
    local sco = cs.instr.score(self)
    local frequency = self.state.runtime_arg.frequency or 440
    local amplitude = self.state.runtime_arg.amplitude or 1
    return utils.join(" ", { sco, frequency, amplitude })
end

-- Csound instrument to mix multiple signals together.
-- A mixer should only receive one init-time argument: 'mapping'.
-- This argument maps audio input channels to mixer output channels
-- and has the form:
--
--     $AUDIO_INPUT_NAME: [chn_index0, chn_index1, ..., chn_indexN]
--
-- For example, to map a mono sine tone to the second and the fourth
-- output channel we can do:
--
--     mapping:
--       sine_0_aoutput0: [1, 3]
--
cs.mixer = class("mixer", cs.instr)

function cs.mixer:initialize(replication_key, options)
    self.mapping = table.pop(options, "mapping") or {}

    options.control_input_count = 1

    options.audio_inputs = {}
    channels_orc = {}
    local index = 0
    local maxchannel = 0
    for audio_input, channels in pairs(self.mapping) do
        local ainput = "ainput" .. tostring(index)
        table.insert(options.audio_inputs, audio_input)
        for _, channel in ipairs(channels) do
            channel = tonumber(channel)
            if not channel then
                self:warn("ignored invalid channel value '" .. tostring(channel) .. "' for audio input '" .. audio_input .. "'")
                goto continue
            end

            if channel > maxchannel then
                maxchannel = channel
            end

            local chncode = channels_orc[channel]
            if chncode then
                chncode = chncode .. " + " .. ainput
            else
                chncode = "  aoutput" .. tostring(channel) .. " = (" .. ainput
            end
            channels_orc[channel] = chncode
            ::continue::
        end
        index = index + 1
    end

    local instr = [[
  kmixer_amp = ampdbfs(kinput0)
]]
    for channel = 0, maxchannel do
        local line = channels_orc[channel] or ("  aoutput" .. tostring(channel) .. " = (0")
        instr = instr .. line .. ") * kmixer_amp\n"
    end

    options.audio_input_count = index
    options.instr = instr
    options.channel_count = maxchannel + 1
    cs.instr.initialize(self, replication_key, options)
end

-- Csound instrument for midi control inputs
cs.midictrl = class("midictrl", cs.instr)

function cs.midictrl:initialize(replication_key, options)
    self.midi_channel = table.pop(options, "midi_channel") or 1
    self.midi_control_number = table.pop(options, "midi_control_number") or 0
    self.min_value = table.pop(options, "min_value") or 0
    self.max_value= table.pop(options, "max_value") or 1

    -- TODO Add optional port ? But activated by default
    options.instr = "  koutput0 ctrl7 " .. utils.join(", ", {self.midi_channel, self.midi_control_number, self.min_value, self.max_value})
    options.audio_input_count = 0
    options.channel_count = 1
    options.rate = "krate"

    options.min_monitoring_value = self.min_value
    options.max_monitoring_value = self.max_value

    cs.instr.initialize(self, replication_key, options)
end

-- Mono butterworth highpass filter - https://csound.com/docs/manual/butterhp.html
cs.butterhp = class("butterhp", cs.instr)

function cs.butterhp:initialize(replication_key, options)
    self.filter_frequency = table.pop(options, "filter_frequency") or 70
    options.instr = "  aoutput0 butterhp ainput0, " .. tostring(self.filter_frequency)
    options.audio_input_count = 1
    options.channel_count = 1
    cs.instr.initialize(self, replication_key, options)
end

-- Csound builtin standard mono reverb
cs.reverb = class("reverb", cs.instr)

function cs.reverb:initialize(replication_key, options)
    options.instr = [[
  ireverbtime = p4
  aoutput0 reverb ainput0, ireverbtime
]]
    options.audio_input_count = 1
    options.channel_count = 1
    cs.instr.initialize(self, replication_key, options)
end

function cs.reverb:score()
    local sco = cs.instr.score(self)
    local reverb_time = self.state.runtime_arg.reverb_time or 1.5
    return utils.join(" ", {sco, reverb_time})
end

-- midiin monitors midi inputs to easily find midi devices
cs.midiin = class("midiin", cs.instr)

function cs.midiin:initialize(replication_key, options)
    options.instr = [[
  kstatus, kchan, kdata1, kdata2 midiin
  iprinttime = 0.01
  if kstatus != 0 then
    printk iprinttime, kstatus, 0, 1
    printk iprinttime, kchan,   0, 1
    printk iprinttime, kdata1,  0, 1
    printk iprinttime, kdata2,  0, 1
  endif
]]
    options.audio_input_count = 0
    options.channel_count = 0
    options.rate = "krate"
    cs.instr.initialize(self, replication_key, options)
end

P.REGISTRY:register("chntest", cs.chntest)
P.REGISTRY:register("in1", cs.in1)
P.REGISTRY:register("outn", cs.outn)
P.REGISTRY:register("sine", cs.sine)
P.REGISTRY:register("mixer", cs.mixer)
P.REGISTRY:register("midictrl", cs.midictrl)
P.REGISTRY:register("butterhp", cs.butterhp)
P.REGISTRY:register("reverb", cs.reverb)
P.REGISTRY:register("midiin", cs.midiin)

return cs
