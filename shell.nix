{ sources ? import ./nix/sources.nix, pkgs ? import sources.nixpkgs {}}:

with pkgs;
with pkgs.luajitPackages;

let

    lua = pkgs.luajit;
    luaPackages = pkgs.luajitPackages;

    # Csound with extra Lua bindings.
    # We use luajit, but our luajit version is compatible with lua5_1. Csound
    # can't be compiled with luajit, but needs a normal lua for compilation.
    # Afterwards the bindings can be imported by both, the normal lua interpreter
    # and luajit.
    csound-with-lua = import ./nix/csound.nix {sources=sources; pkgs=pkgs; lua=pkgs.lua5_1;};

    ltui = import ./nix/ltui.nix {sources=sources; pkgs=pkgs; lua=lua;};

    # Wrapped Lua that loads csound bindings to Lua path.
    lua-with-csound = pkgs.writeShellScriptBin "lua" ''
        export LUA_CPATH=$LUA_CPATH";${csound-with-lua}/lib/?.so"
        exec ${lua}/bin/lua "$@"
    '';

    libxcsound = import ./xcsound/default.nix { sources=sources; pkgs=pkgs; csound=csound-with-lua; };

in
    mkShell {
        buildInputs = [
            lua-with-csound
            ltui
            # Providing xcsound functions
            libxcsound

            # For formatting lua code
            stylua
        ];
        shellHook = ''
            # When using 'luaPackages' we can't use local paths anymore, so
            # explicitly specify them...

            wm2path=$(pwd)

            export LUA_PATH=$LUA_PATH";$wm2path/?.lua"
            export LUA_PATH=$LUA_PATH";$wm2path/?/init.lua"

            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH";${libxcsound}/lib/"
        '';
    }
