-- Play a sine tone to manually test cs.instr

local wm2 = require("wm2")

function start()
    local sine = wm2.cs.sine("0", { fade_in_duration = 2, fade_out_duration = 2 })
    local outn = wm2.cs.outn("0", { audio_inputs = { "sine_0_aoutput0" }, max_rms = 2 })
    local patch = wm2.Patch(sine, outn)
    local server = wm2.Server({ loglevel = "debug", channel_count = 1 })

    server:start(patch)
    server:loop(true)
end

start()
