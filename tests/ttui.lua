-- Starts TUI to manually test its functionality.

local wm2 = require("wm2")

function startTUI()
    local ptable = {}
    for i = 0, 200 do
        local p = wm2.P("t" .. i, {})
        table.insert(ptable, p)
    end

    -- To test info view
    local seq = wm2.Seq("0", {})
    local q = wm2.Q("0", {
        p = {
            seq_0 = {
                events = {
                    { duration = 1, p = { "p_t0", { arg = "test" } } },
                    {
                        duration = 0.5,
                        p = { "p_t1", {} },
                    },
                },
                loop = true,
            },
        },
    })
    table.insert(ptable, seq)
    table.insert(ptable, q)

    local patch = wm2.Patch(unpack(ptable))

    local server = wm2.Server({ loglevel = "debug" })

    server:start(patch)
    server:loop(true)
end

startTUI()
