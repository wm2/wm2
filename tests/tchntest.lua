-- Starts wm2 with chntest to manually test its functionality.

local wm2 = require("wm2")

function start()
    local chntest = wm2.cs.chntest("0", {})
    local patch = wm2.Patch(chntest)
    local server = wm2.Server({ loglevel = "debug", channel_count = 4 })

    server:start(patch)
    server:loop(true)
end

start()
