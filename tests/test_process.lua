-- Test 'Process' and 'ProcessRegistry'

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")
local class = require("wm2.lib.middleclass")

-- TProcess is a test class for an actual process
local TProcess = class("TProcess", wm2.Process)

function TProcess:initialize()
    self.counter = 0
    wm2.Process.initialize(self, "tprocess")
end

function TProcess:iteration1()
    -- See 'test_call' to better understand the reason
    -- of the logic here.
    if self.counter < 1 then
        self:defer_sleep(1)
        self.counter = self.counter + 1
        return 1 -- work done
    end

    if self.next_t == 50 then
        return 0 -- stop program
    end

    if self.next_t + 29 == 50 then
        self:defer_sleep(29)
        return 2 -- no work done
    end

    self:defer_sleep(1)
    return 2 -- no work done
end

-- Here the actual test code starts
TestProcess = {}

function TestProcess:setUp()
    self.process = TProcess:new()
end

function TestProcess:test_name()
    lu.assertEquals(self.process.name, "tprocess")
end

function TestProcess:test_tostring()
    lu.assertEquals(tostring(self.process), "tprocess")
end

function TestProcess:test_register_and_unregister()
    -- stylua: ignore start
    function getp() return wm2.Process.REGISTRY:get(self.process) end
    function is_unregistered() lu.assertIsNil(getp()) end
    function is_registered() lu.assertEquals(getp(), 1) end
    -- stylua: ignore end

    is_unregistered()
    self.process:register()
    is_registered()
    self.process:unregister()
    is_unregistered()
end

function TestProcess:test_defer_sleep()
    lu.assertIsNil(self.process.next_t)

    self.process.t = 0
    self.process:defer_sleep(1)
    lu.assertEquals(self.process.next_t, 1)

    self.process:defer_sleep(1)
    lu.assertEquals(self.process.next_t, 2)
end

function TestProcess:test_call()
    local t0 = 20
    local v0 = self.process(t0)

    lu.assertEquals(self.process.t, t0)
    lu.assertEquals(self.process.next_t, t0 + 1)
    lu.assertEquals(v0, 1) -- initially: work done

    local t1 = 21
    local v1 = self.process(t1)

    lu.assertEquals(self.process.t, t0) -- t doesn't change
    lu.assertEquals(self.process.next_t, 50)
    lu.assertEquals(v1, 2) -- no work done anymore

    -- Here nothing should happen, as our next time is
    -- 50 and we are only at 22: the process shouldn't
    -- call 'iteration1'.
    local t2 = 22
    local v2 = self.process(t2)

    lu.assertEquals(self.process.next_t, 50)
    lu.assertEquals(v2, 2) -- no work done anymore

    local t3 = 50
    local v3 = self.process(t3)

    lu.assertEquals(self.process.next_t, t3)
    lu.assertEquals(v3, 0) -- exit program
end

function TestProcess:test_reset()
    function is_reset()
        lu.assertIsNil(self.process.t)
        lu.assertIsNil(self.process.next_t)
    end
    function is_not_reset()
        lu.assertNotIsNil(self.process.t)
        lu.assertNotIsNil(self.process.next_t)
    end

    is_reset()
    self.process(1)
    is_not_reset()
    self.process:reset()
    is_reset()
end

os.exit(lu.LuaUnit.run())
