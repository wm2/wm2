-- Test wm2.Registry

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

TestRegistry = {}

function TestRegistry:setUp()
    self.type_name = "testregistry"
    self.registry = wm2.Registry:new({ type_name = self.type_name })

    -- Test key/value
    self.key = "testkey"
    self.value = "testvalue"
end

function TestRegistry:test_register()
    self:_test_key_does_not_exist()
    self:_register()
    self:_test_key_exists()
end

function TestRegistry:test_unregister()
    self:_test_key_does_not_exist()
    self:_register()
    self:_test_key_exists()
    self:_unregister()
    self:_test_key_does_not_exist()
end

-- We can override an existing key in a registry with a new value,
-- but the registry should send a warning to the log file.
function TestRegistry:test_register_override_existing()
    self:_register()
    self:_test_key_exists()
    local value2 = self.value .. "2"
    self:_register(self.key, value2)
    self:_test_key_exists(self.key, value2)
end

function TestRegistry:test_reset()
    self:_register()
    self:_test_key_exists()
    self.registry:reset()
    self:_test_key_does_not_exist()
end

-- Helper methods
function TestRegistry:_register(key, value)
    self.registry:register(key or self.key, value or self.value)
end

function TestRegistry:_unregister()
    self.registry:unregister(self.key)
end

function TestRegistry:_test_key_exists(key, value)
    lu.assertEquals(self.registry:get(key or self.key), value or self.value)
end

function TestRegistry:_test_key_does_not_exist(key)
    lu.assertEquals(self.registry:get(key or self.key), nil)
end

os.exit(lu.LuaUnit.run())
