-- This module provides utilities to run csound tests.
-- These utilities are provided by a base class. To inherit
-- from this base class, simply call 'tcs()'.

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

local TestCsound = {}

-- 'setupServer' sets up wm2 server and starts this server. It
-- needs to be explicitly called by each test case. In this way,
-- each test case can specify its patch.
--
-- NOTE We don't use 'setUp', because each test case may need
-- to specify its own test. Automatically running 'setUp' before
-- each test case prevents test cases from specifying arguments
-- when setting up the server.
function TestCsound:setupServer(...)
    local programs = { ... }
    self.patch = wm2.Patch:new({ loglevel = "debug", audio = "null", midi = "null" }, table.unpack(programs))

    self.server = wm2.Server:new({ auto_start_csound = false })
    self.bufferduration = self.patch.software_buffer / self.patch.sampling_rate

    self.server:start(self.patch)
end

-- 'tearDown' shuts down csound server (if there is any)
function TestCsound:tearDown()
    if self.server and self.server:is_started() then
        self.server:_loop_exit()
    end
    -- Unset class state
    self.server = nil
    self.patch = nil
    self.bufferduration = nil
end

-- Proceed audio server by seconds.
function TestCsound:perform(seconds)
    buffercount = 1
    if seconds then
        buffercount = math.floor((seconds / self.bufferduration) + 0.5)
    end
    for _ = 1, (buffercount or 1) do
        self.server.csound:PerformBuffer()
    end
end

-- 'run' starts instrument. It's merely a convenience wrapper for
-- set_state + start.
function TestCsound:run(instr)
    instr:set_state({})
    instr:start()
end

-- Asserts that instrument is not silent when it is playing.
function TestCsound:assertAudibleIfStarted(instr)
    self:run(instr)
    local dur = instr.fade_in_duration
    if dur == 0 then
        dur = 0.05
    end
    self:perform(dur)
    self:assertIsNotSilent(instr)
    instr:stop()
    local dur = instr.fade_out_duration
    if dur == 0 then
        dur = 0.05
    end
    self:perform(dur)
end

-- Asserts that instrument is silent when it is not playing
function TestCsound:assertSilentIfStopped(instr)
    self:assertIsSilent(instr)
    self:run(instr)
    local dur = instr.fade_in_duration
    if dur == 0 then
        dur = 0.2
    end
    self:perform(dur)
    instr:stop()
    -- Add tolerance of 0.05s to decrease flaky test likelihood
    self:perform(instr.fade_out_duration + 0.05)
    self:assertIsSilent(instr)
end

-- Assert instrument is silent.
function TestCsound:assertIsSilent(instr)
    lu.assertIsTrue(instr:is_silent(), tostring(instr) .. " is not silent")
end

-- Assert instrument is not silent.
function TestCsound:assertIsNotSilent(instr)
    lu.assertIsFalse(instr:is_silent(), tostring(instr) .. " is silent")
end

-- Asserts that specified channels are silent
function TestCsound:assertChannelsAreSilent(instr, ...)
    local channels = { ... }
    local status = self:_are_channels_silent(instr, channels)
    for chn, v in pairs(status) do
        lu.assertAlmostEquals(v, 0, 0.0001, "channel " .. chn .. " is not silent!")
    end
end

-- Asserts that specified channels are not silent
function TestCsound:assertChannelsAreNotSilent(instr, ...)
    local channels = { ... }
    local status = self:_are_channels_silent(instr, channels)
    for chn, v in pairs(status) do
        lu.assertIsTrue(math.abs(v) > 0.0001, "channel " .. chn .. " is silent! v = " .. v)
    end
end

function TestCsound:_are_channels_silent(instr, channels)
    local status = {}
    local monitoring_values = instr:monitoring_values()
    for _, chn in ipairs(channels) do
        local v = monitoring_values[chn + 1]
        assert(v ~= nil, "instrument has less than " .. chn .. " channels!")
        status[chn] = v
    end
    return status
end

-- This function returns new cls that inherits from TestCsound
return function()
    local cls = {}
    setmetatable(cls, { __index = TestCsound })
    return cls
end
