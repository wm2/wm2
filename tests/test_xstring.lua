-- Test wm2 string extensions

local lu = require("wm2.lib.luaunit")

require("wm2") -- load for side effects

-- Test string split2 method
function TestSplit2()
    local s = "abc def ghj"
    local s_split2 = s:split2(" ")
    lu.assertEquals(#s_split2, 3)
    lu.assertEquals(s_split2[1], "abc")
    lu.assertEquals(s_split2[2], "def")
    lu.assertEquals(s_split2[3], "ghj")
end

-- Test string split2 works by persisting empty lines
function TestSplit2EmptyLine()
    local s = [[abc

def]]
    local s_split2 = s:split2("\n")
    wm2 = require("wm2")
    lu.assertEquals(#s_split2, 3)
    lu.assertEquals(s_split2[1], "abc")
    lu.assertEquals(s_split2[2], "")
    lu.assertEquals(s_split2[3], "def")
end

os.exit(lu.LuaUnit.run())
