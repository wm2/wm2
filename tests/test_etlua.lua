-- This tests custom 'etlua' features that are unique to wm2.

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")
local etlua = require("wm2.lib.etlua")

-- Test basic rendering and calling of a macro.
function TestBasicMacro()
    local template = [[
{{m test }}
Hello {{= arg1 }} this is {{= arg2 }}.
A macro can have multiple arguments.
They are always

keyword arguments.
{{m test }}
{{= test({ arg1 = "world", arg2 = "a etlua macro" }) }}]]
    local result_ok = [[
Hello world this is a etlua macro.
A macro can have multiple arguments.
They are always

keyword arguments.]]
    local result = etlua.render(template)
    lu.assertEquals(result, result_ok)
end

-- Ensure error is raised if a macro is defined twice.
function TestDuplicateMacro()
    local template = [[
{{m test }}
hello
{{m test }}
{{m test }}
hello2
{{m test }}]]
    lu.assertError(etlua.render, template)
end

-- Ensure nested macros are forbidden and proper error is called.
function TestNoNestedMacros()
    local template = [[
{{m test }}
hello
{{m test2 }}
hello2
{{m test2 }}
hello3
{{m test }}]]
    lu.assertError(etlua.render, template)
end

-- Ensure etlua drops all comments
function TestDropComments()
    local template = [[
{{ -- This is a comment: }}
{{ -- it should be removed }}
    {{ -- without leaving }}
  {{ -- any }}
            {{ -- traces}}]]
    local result_ok = ""
    local result, err = etlua.render(template)
    lu.assertIsNil(err)
    lu.assertEquals(result, result_ok)
end

-- Ensure the custom etlua function 'loadmacros' works as expected.
function TestLoadmacros()
    local template = [[
{{m macrotest }}
hello {{= name }}!
{{= goodbye({name = name}) }}
{{m macrotest }}

{{m goodbye }}
see you soon {{= name }}
{{m goodbye }}]]
    local result_ok = [[
hello bob!
see you soon bob]]
    local env = etlua.loadmacros(template)
    local result = env.macrotest({ name = "bob" })
    lu.assertEquals(result, result_ok)
end

os.exit(lu.LuaUnit.run())
