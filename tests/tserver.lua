-- 'tserver' provides base class and utilities to run Server tests.

local wm2 = require("wm2")
local lu = require("wm2.lib.luaunit")

TestServer = {}

function TestServer:setUp(servercls)
    wm2.Process.REGISTRY:reset()

    wm2.P.REGISTRY:register("p", wm2.P)
    wm2.P.REGISTRY:register("buggyp", BuggyP)

    self.patch = wm2.parser.parse_yml_file("tests/tserver.yml")
    local p = self.patch.programs

    -- Assign P objects for handy tests
    self.p0 = p["p_t0"]
    self.p1 = p["p_t1"]
    self.p2 = p["p_t2"]
    self.buggy_p = p["buggyp_t0"]
    self.q0 = p["q_t0"]
    self.q1 = p["q_t1"]
    self.q2 = p["q_t2"]
    self.q3 = p["q_t3"]
    self.q4 = p["q_t4"]
    self.chntest = p["chntest_0"]

    -- 'p_requester' doesn't belong to the patch, but can
    -- request start/stop/switch of P that belongs to Patch.
    self.p_requester = wm2.P:new("requester", {})

    self.server = (servercls or wm2.Server):new({
        never_sleep = true,
    })
end

function TestServer:tearDown()
    if self.server:is_started() then
        self.server:_loop_exit()
    end

    wm2.P.REGISTRY:unregister("p")
    wm2.P.REGISTRY:unregister("buggyp")
end

-- 'iterate' repeats n iterations of the global loop.
function TestServer:iterate(iteration_count)
    if not self.server._looping then
        self.server:_loop_enter()
    end
    self.server:iterate(iteration_count)
end

-- 'TestServer' provides specified 'request_start', 'request_stop' and
-- 'request_switch' methods to be used in tests. These methods simplify
-- request methods (don't need 'p.id' but 'p' itself, provide default
-- arguments, ...).
function TestServer:start_p(p, requester, runtime_arg)
    self.server:request_start(p.id, (requester or self.p_requester).id, runtime_arg or {})
    self:iterate()
end

function TestServer:stop_p(p, requester, runtime_arg)
    self.server:request_stop(p.id, (requester or self.p_requester).id)
    self:iterate()
end

function TestServer:switch_p(p0, p1, requester, runtime_arg)
    self.server:request_switch(p0.id, p1.id, (requester or self.p_requester).id, runtime_arg or {})
    self:iterate()
end

-- This function returns new cls that inherits from TestServer
return function()
    local cls = {}
    setmetatable(cls, { __index = TestServer })
    return cls
end
