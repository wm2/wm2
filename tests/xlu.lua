-- Extend luaunit with wm2 specifc assertions

local lu = require("wm2.lib.luaunit")
local inspect = require("wm2.lib.inspect")
local wm2 = require("wm2")

local xlu = {}

function xlu.assert_p_started(p)
    lu.assertIsTrue(p:is_started(), "'" .. tostring(p.id) .. "' is not started")
    xlu.assert_set_state(p)
end

function xlu.assert_p_stopped(p)
    lu.assertIsFalse(p:is_started(), "'" .. tostring(p.id) .. "' is not stopped")
    xlu.assert_unset_state(p)
end

function xlu.assert_runtime_arg_is_equal(p, runtime_arg)
    lu.assertIsTrue(
        xlu.runtime_arg_eq(p, runtime_arg),
        "runtime arg is not equal; want: " .. inspect(runtime_arg) .. ", got: " .. inspect(p.state.runtime_arg)
    )
end

function xlu.assert_requesters_is_equal(p, requesters)
    lu.assertIsTrue(xlu.requesters_eq(p, requesters), "runtime arg is equal: " .. inspect(runtime_arg))
end

function xlu.assert_runtime_arg_is_not_equal(p, runtime_arg)
    lu.assertIsFalse(xlu.runtime_arg_eq(p, runtime_arg))
end

function xlu.assert_requesters_is_not_equal(p, requesters)
    lu.assertIsFalse(xlu.requesters_eq(p, requesters))
end

function xlu.assert_unset_state(p)
    xlu.assert_runtime_arg_is_equal(p, nil)
    xlu.assert_requesters_is_equal(p, {})
end

function xlu.assert_set_state(p)
    xlu.assert_runtime_arg_is_not_equal(p, nil)
    xlu.assert_requesters_is_not_equal(p, {})
end

function xlu.runtime_arg_eq(p, runtime_arg)
    return table.eq(runtime_arg, p.state.runtime_arg)
end

function xlu.requesters_eq(p, requesters)
    return table.eq(requesters, p.state.requesters)
end

-- monkeypatch helps to temporarily monkey patch an object for test purposes.
xlu.monkeypatch = {}

function xlu.monkeypatch:set_attribute(object, attribute_name, value)
    local before = object[attribute_name]
    table.insert(self:patches(), { object, attribute_name, before })
    object[attribute_name] = value
end

function xlu.monkeypatch:revert()
    for _, patch in ipairs(self:patches()) do
        local object = patch[1]
        local attribute_name = patch[2]
        local attribute = patch[3]
        patch[attribute_name] = attribute
    end
    self._patches = nil
end

function xlu.monkeypatch:patches()
    if not self._patches then
        self._patches = {}
    end
    return self._patches
end

return xlu
