-- Test parser

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

TestParser = {}

-- Ensure an empty file still returns a valid server & patch.
function TestParser:test_empty_patch()
    local patch = wm2.parser.parse_yml_string("")

    lu.assertNotNil(patch)

    local server = wm2.Server()

    -- Check if usual API works
    server:start(patch)
    lu.assertIsTrue(server:is_started())
    server:stop()
end

-- Ensure configuration block of yml file works as expected.
function TestParser:test_configure()
    local yml = [[
configure:
    loglevel: debug
    channel_count: 5
    audio: alsa
]]

    local patch = wm2.parser.parse_yml_string(yml)

    lu.assertEquals(patch.loglevel, "debug")
    lu.assertEquals(patch.audio_type, "alsa")
    lu.assertEquals(patch.channel_count, 5)
end

-- Ensure declaration block of yml file works as expected.
function TestParser:test_declare()
    local yml = [[
declare:
    q:
        0:
            p:
                q_test:
        test:
]]

    local patch = wm2.parser.parse_yml_string(yml)

    -- All P loaded?
    lu.assertEquals(#patch.sorted_programs, 2)
    lu.assertNotNil(patch.programs["q_0"])
    lu.assertNotNil(patch.programs["q_test"])

    -- P with correct init arg?
    lu.assertIsTrue(table.eq(patch.programs["q_0"].p, { q_test = {} }))
end

-- Ensure registration block of yml file works as expected.
function TestParser:test_register()
    lu.skip("registration block in yml file isn't implemented yet")
end

-- Ensure template language etlua works as expected.
function TestParser:test_template_language()
    local yml = [[
configure:
    # some stupid calculations to see if etlua expression works
    channel_count: {{= (2 + 3) * 2 }}

declare:
    q:
    {{ for k = 1, 4 do }}
        {{= k }}: {}
    {{ end }}
]]

    local patch = wm2.parser.parse_yml_string(yml)

    lu.assertEquals(patch.channel_count, 10)

    lu.assertEquals(#patch.sorted_programs, 4)

    lu.assertNotNil(patch.programs["q_1"])
    lu.assertNotNil(patch.programs["q_2"])
    lu.assertNotNil(patch.programs["q_3"])
    lu.assertNotNil(patch.programs["q_4"])
end

-- Ensure loading yml from file works.
function TestParser:test_parse_file()
    local yml = [[
configure:
    loglevel: debug
    channel_count: 2

declare:
    q:
        1:
]]
    local path = "/tmp/wm2_test_parser.yml"
    wm2.utils.write_to_file(path, yml)

    local patch = wm2.parser.parse_yml_file(path)

    lu.assertEquals(patch.path, path)
    lu.assertEquals(patch.loglevel, "debug")
    lu.assertEquals(patch.channel_count, 2)
    lu.assertNotNil(patch.programs["q_1"])
    lu.assertEquals(#patch.sorted_programs, 1)

    wm2.utils.rm(path)
end

os.exit(lu.LuaUnit.run())
