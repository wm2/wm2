-- Test Q

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

TestQ = {}

function TestQ:setUp()
    self.p0 = wm2.P:new("p0", {})
    self.p1 = wm2.P:new("p1", {})

    self.q = wm2.Q:new("q", { p = { [self.p0.id] = {}, [self.p1.id] = { test = 0 } } })
    self.nested_q = wm2.Q:new("nestedq", { p = { [self.q.id] = {} } })
    self.q_with_undefined_p = wm2.Q:new("qwithundefinedp", { p = { [self.p0.id] = {}, test_undefined = {} } })

    self.patch = wm2.Patch:new({}, self.p0, self.p1, self.q)

    self.server = {} -- mock server with patch object
    self.server.patch = self.patch
end

-- Test 'dependencies()' method returns correct table
-- in simple case and in nested case.
function TestQ:test_dependencies()
    -- Test simple q
    local dependencies = {
        { requester = self.q.id, id = self.p0.id, runtime_arg = {} },
        { requester = self.q.id, id = self.p1.id, runtime_arg = { test = 0 } },
    }
    self.q:bind(self.server)
    dependencies_ok(self.q:dependencies(), dependencies)

    -- Test nested q (q which starts another q)
    local dependencies = {
        { requester = self.nested_q.id, id = self.q.id, runtime_arg = {} },
    }
    self.nested_q:bind(self.server)
    dependencies_ok(self.nested_q:dependencies(), dependencies)
end

-- Ensure 'dependencies' raises an error in case it's called
-- before we bind the q to the server.
function TestQ:test_dependencies_before_bind()
    lu.assertError(self.q.dependencies, self.q)
end

-- Ensure the 'dependencies' function doesn't raise an error in
-- case a user-declared P in Qs p init-time argument doesn't exist
-- in the patch.
function TestQ:test_dependencies_do_not_crash_if_p_is_not_found()
    self.q_with_undefined_p:bind(self.server)

    local dependencies = {
        { requester = self.q_with_undefined_p.id, id = self.p0.id, runtime_arg = {} },
    }

    dependencies_ok(self.q_with_undefined_p:dependencies(), dependencies)
end

-- Ensure the Q type is registered in the global P.REGISTRY.
function TestQ:test_q_is_registered()
    lu.assertEquals(wm2.Q, wm2.P.REGISTRY:get("q"))
end

-- Check if two dependencies are equal. We can't simply
-- use 'table.eq', as 'table.eq' only returns true if the
-- same elements are in the same order. But the order of
-- the elements that the 'dependencies' method returns is
-- not deterministic. Therefore we need to use a specific
-- function that checks if only the same elements are
-- present in both tables. This function is not generic
-- and inefficient and therefore not added to wm2.utils.
function dependencies_eq(dependencies0, dependencies1)
    for d0, d1 in pairs({ [dependencies0] = dependencies1, [dependencies1] = dependencies0 }) do
        for _, v0 in ipairs(d0) do
            present = false
            for _, v1 in ipairs(d1) do
                if table.eq(v1, v0) then
                    present = true
                    break
                end
            end
            if present == false then
                return false
            end
        end
    end
    return true
end

-- Asserts that dependencies are equal.
function dependencies_ok(dependencies0, dependencies1)
    lu.assertIsTrue(dependencies_eq(dependencies0, dependencies1))
end

os.exit(lu.LuaUnit.run())
