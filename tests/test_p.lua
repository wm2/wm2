-- Test P, P.Id & P.State

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

local MockServer = {}

TestP = {}

function TestP:setUp()
    self.replication_key = "test"
    self.p = wm2.P:new(self.replication_key, {})
end

-- We start with tests to check whether the different
-- lifecycle steps work as expected and are prohibited
-- if not activated in the correct order.

function TestP:test_is_bound()
    lu.assertIsFalse(self.p:is_bound())
    self.p:bind(MockServer)
    lu.assertIsTrue(self.p:is_bound())
    self.p:unbind()
    lu.assertIsFalse(self.p:is_bound())
end

function TestP:test_bind_twice()
    self.p:bind(MockServer)
    lu.assertError(self.p.bind, self.p, MockServer)
end

function TestP:test_unbind_twice()
    self.p:bind(MockServer)
    self.p:unbind()
    lu.assertError(self.p.unbind, self.p)
end

function TestP:test_unbound_unbind()
    lu.assertError(self.p.unbind, self.p)
end

function TestP:test_unbound_start()
    lu.assertIsFalse(self.p:is_bound())
    lu.assertError(self.p.start, self.p)
end

function TestP:test_is_started()
    lu.assertIsFalse(self.p:is_started())
    self.p:bind(MockServer)
    self.p:start()
    lu.assertIsTrue(self.p:is_started())
    self.p:stop()
    lu.assertIsFalse(self.p:is_started())
end

function TestP:test_start_twice()
    self.p:bind(MockServer)
    self.p:start()
    lu.assertError(self.p.start, self.p)
end

function TestP:test_stop_twice()
    self.p:bind(MockServer)
    self.p:start()
    self.p:stop()
    lu.assertError(self.p.stop, self.p)
end

function TestP:test_inactive_stop()
    self.p:bind(MockServer)
    lu.assertError(self.p.stop, self.p)
end

function TestP:test_unbound_active_p()
    self.p:bind(MockServer)
    self.p:start()
    lu.assertError(self.p.unbind, self.p)
end

-- Now we test the attributes of P ('Id' and 'State')

function TestP:test_id()
    lu.assertEquals(self.p.id, wm2.P.Id:new("p", self.replication_key))
end

function TestP:test_state()
    lu.assertEquals(self.p.state, wm2.P.State:new())
end

function TestP:test_set_state()
    self.p:set_state({ hello = 1 })
    local state = wm2.P.State:new()
    state.runtime_arg = { hello = 1 }
    lu.assertEquals(self.p.state, state)
    lu.assertNotEquals(self.p.state, wm2.P.State:new())
end

-- Test P.Id outside of the context of being an attribute
-- of P.
TestPId = {}

function TestPId:test_from_string()
    lu.assertEquals(wm2.P.Id:new("hello", "world"), wm2.P.Id.from_string("hello_world"))
end

-- Test P.State outside of the context of being an attribute
-- of P.
TestPState = {}

function TestPState:test_reset()
    local state = wm2.P.State:new()
    lu.assertEquals(state.runtime_arg, nil)
    state.runtime_arg = { test = 1 }
    lu.assertEquals(state.runtime_arg, { test = 1 })
    state:reset()
    lu.assertEquals(state.runtime_arg, nil)
end

function TestPState:test_copy()
    local state0 = wm2.P.State:new()
    state0.runtime_arg = { test = 1 }
    state0.requesters[wm2.P.Id:new("p", "test")] = 1

    -- set runtime_arg in copy
    local state1 = state0:copy()
    lu.assertEquals(state0, state1)

    state1.runtime_arg.test = 2
    lu.assertNotEquals(state0, state1)

    -- set requesters in copy
    local state2 = state0:copy()
    lu.assertEquals(state0, state2)

    table.insert(state2.requesters, "p_test2")
    lu.assertNotEquals(state0, state2)
end

os.exit(lu.LuaUnit.run())
