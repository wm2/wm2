-- Automatically test TUI

local lu = require("wm2.lib.luaunit")
local xlu = require("tests.xlu")
local wm2 = require("wm2")
local app = require("wm2.tui.app")
local ltui = require("ltui")
local offline = require("tests.offline")

TestTUI = {}

function TestTUI:setUp()
    wm2.Process.REGISTRY:reset()

    -- Ensure we always have the same page layout with the same amount
    -- of columns and rows per page. By default this depends on the
    -- screen size, which is therefore not controllable/reproducible
    -- among different machines. In order to make the test not flaky,
    -- we therefore explicitly specify the pages layout.
    local row_count = 9
    local column_count = 5
    local appnew = app.new
    function new(self, object)
        return appnew(self, object, row_count, column_count)
    end
    xlu.monkeypatch:set_attribute(app, "new", new)

    self.p0 = wm2.cs.sine:new("0", {})
    self.p1 = wm2.P:new("t1", {})
    self.q0 = wm2.Q:new("t0", { p = { sine_0 = {}, p_t1 = {} } })

    self.pdummy_table = {}
    for i = 0, 2 * column_count * row_count do
        table.insert(self.pdummy_table, wm2.P:new("dummy" .. tostring(i), {}))
    end

    self.patch = wm2.Patch:new({ loglevel = "debug" }, self.p0, self.p1, self.q0, unpack(self.pdummy_table))

    self.server = offline.Server:new({ never_sleep = true })
    self.server:start(self.patch)
    self.server:_loop_enter(true)
    self.tui = self.server.tui

    -- needed for 'presskey' function
    self.reverse_key_map = {}
    for k, v in pairs(self.tui.app:_key_map()) do
        self.reverse_key_map[v] = k
    end

    -- start TUI
    -- multiple iterations: reduce likelihood of
    -- flaky test failures
    self:iterate(20)
end

function TestTUI:tearDown()
    self.server:_loop_exit()
    xlu.monkeypatch:revert()
end

-- Test Spacebar works to toggle start/stop of P.
-- Also ensures that when starting a P with dependencies
-- in the TUI all dependents start/stop.
function TestTUI:test_start_and_stop_p()
    self.tui:select_p(1, 2)

    self:presskey(" ")
    self:iterate(2)
    xlu.assert_p_started(self.q0)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)

    self:presskey(" ")
    self:iterate(2)
    xlu.assert_p_stopped(self.q0)
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
end

-- Test 'n' key works to switch to next P.
function TestTUI:test_switch_next()
    self.tui:select_p(1, 0) -- move to first p

    self:presskey(" ")
    self:iterate(5)

    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)

    self:presskey("n")
    self:iterate(5)

    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_started(self.p1)
end

-- Test 'b' key works to switch to previous P.
function TestTUI:test_switch_before()
    self.tui:select_p(1, 1) -- move to second p

    self:presskey(" ")
    self:iterate(5)

    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_started(self.p1)

    self:presskey("b")
    self:iterate(5)

    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)
end

-- Test 'l' key works to move to the right P.
function TestTUI:test_right()
    self.tui:select_p(1, 0)
    self:presskey("l")
    self:iterate(5)
    lu.assertEquals(self:get_selected_id(), self.p1.id)
end

-- Test 'h' key works to move to the left P.
function TestTUI:test_left()
    self.tui:select_p(1, 1)
    self:presskey("h")
    self:iterate(5)
    lu.assertEquals(self:get_selected_id(), self.p0.id)
end

-- Test 'k' key works to move to the upper P.
function TestTUI:test_up()
    self.tui:select_p(1, 9)
    self:presskey("k")
    self:iterate(5)
    lu.assertEquals(self:get_selected_id(), self.p0.id)
end

-- Test 'j' key works to move to the lower P.
function TestTUI:test_down()
    self.tui:select_p(1, 0)
    self:presskey("j")
    self:iterate(5)
    lu.assertEquals(self:get_selected_id(), self.pdummy_table[7].id)
end

-- Test 'r' key restarts the server
function TestTUI:test_r_restart()
    lu.assertIsNil(self.server._restart)
    lu.assertIsNil(self.server.restart_agent.reload_patch)
    self:presskey("r")
    self:iterate(5)
    lu.assertIsTrue(self.server._restart)
    lu.assertIsNil(self.server.restart_agent.reload_patch)
    self.server._restart = false -- don't actually restart
end

-- Test 'R' key restarts the server and reloads the patch
function TestTUI:test_R_restart_and_reload_patch()
    lu.assertIsNil(self.server._restart)
    lu.assertIsNil(self.server.restart_agent.reload_patch)
    self:presskey("R")
    self:iterate(5)
    lu.assertIsTrue(self.server._restart)
    lu.assertIsTrue(self.server.restart_agent.reload_patch)
    self.server._restart = false -- don't actually restart
end

-- Test number keys ('1', '2', ...) switch between pages
function TestTUI:test_select_page_keyboard()
    self:presskey("2")
    self:iterate(2)
    lu.assertEquals(self:get_current_page_index(), 2)

    self:presskey("1")
    self:iterate(2)
    lu.assertEquals(self:get_current_page_index(), 1)
end

-- Ensure 'restart' command restarts the server
function TestTUI:test_command_restart()
    lu.assertIsNil(self.server._restart)
    self:command("restart")
    self:iterate(10)
    lu.assertIsTrue(self.server._restart)
    self.server._restart = false -- don't actually restart
end

-- Ensure 'q' command quits loop
function TestTUI:test_quit()
    lu.assertIsTrue(self.server.looping)
    self:command("q")
    self:iterate(5)
    lu.assertIsFalse(self.server.looping)
end

-- Ensure 'P' command panics server
function TestTUI:test_panic()
    local panicked = false
    xlu.monkeypatch:set_attribute(self.server, "panic", function()
        panicked = true
    end)
    self:presskey("P")
    self:iterate(30)
    lu.assertIsTrue(panicked)
end

-- Ensure 'i' command opens details view of currently selected program.
-- Ensure 'q' command switches back to 'grid' mode.
function TestTUI:test_open_and_close_detail()
    local app = self.tui.app
    lu.assertEquals(app:current_mode(), "grid")
    self.tui:select_p(1, 0)
    self:presskey("i")
    self:iterate(10)
    lu.assertEquals(app:current_mode(), "detail")
    self:presskey("q")
    self:iterate(10)
    lu.assertEquals(app:current_mode(), "grid")
end

-- Test detail widgets work ok
function TestTUI:test_widget_title()
    self:_test_widget(1)
end

function TestTUI:test_widget_info()
    self:_test_widget(2)
end

function TestTUI:test_widget_ampmeter()
    self:_test_widget(3)
end

function TestTUI:_test_widget(widget_index)
    self.tui:select_p(1, 0)
    self:presskey("i")
    self:iterate(20)
    local detail = self.tui.app.detail
    local views = {}
    for v in detail:views() do
        table.insert(views, v)
    end
    lu.assertIsTrue(#views > 0)
    local widget = views[widget_index]
    lu.assertNotIsNil(widget)
    local process = widget.process
    if process ~= nil then
        -- process must be registered
        lu.assertEquals(wm2.Process.REGISTRY:get(process), 1)
    end
    self:presskey("q")
    self:iterate(10)
    -- 'widget' must not be in detail anymore after
    -- quitting detail view => if it still is, 'remove'
    -- raises an assertion error.
    lu.assertError(detail.remove, detail, widget)
    -- widget process must be unregistered to not waste resources
    if process ~= nil then
        -- process must be registered
        lu.assertEquals(wm2.Process.REGISTRY:get(process), nil)
    end
end

-- Test actual restart works & that the same program + page are selected like before restart
function TestTUI:test_restart()
    -- We need to patch loop function to not raise error when
    -- restarting server (as it enters loop when restarted).
    xlu.monkeypatch:set_attribute(self.server, "loop", function(tui, currentpage, currentp)
        self.server:_loop_enter(tui, currentpage, currentp)
        self.loop_executed = true
    end)

    self:presskey("2")
    self.tui:select_p(2, 0)
    self:presskey("l")
    self:command("restart")
    self:iterate(30)
    lu.assertIsNil(self.loop_executed)

    self.server:_loop_exit() -- trigger restart => enter loop

    lu.assertIsTrue(self.loop_executed)
    lu.assertEquals(self:get_selected_id(), self.pdummy_table[44].id)
    lu.assertEquals(self:get_current_page_index(), 2)
    xlu.monkeypatch:revert()
end

-- 'command' executes command in TUI
function TestTUI:command(cmd)
    self:presskey(":")
    for i = 1, #cmd do
        local c = cmd:sub(i, i)
        self:presskey(c)
    end
    self:presskey("Enter")
end

-- 'presskey' sends key to program
function TestTUI:presskey(key_name)
    key_code = self.reverse_key_map[key_name]
    if key_code == nil then
        key_code = string.byte(key_name)
    end
    -- XXX: we skip 'key_meta', therefore alt keys don't work yet.
    local e = ltui.event.keyboard({ key_code, key_name, nil })
    self:put_event(e)
end

function TestTUI:iterate(iteration_count, sleeptime)
    self.server:iterate(iteration_count, sleeptime)
end

function TestTUI:put_event(e)
    self.tui.app:put_event(e)
end

function TestTUI:get_current_page_index()
    return self.tui.app.paginator:current_page().index
end

function TestTUI:get_selected_id()
    local ptoggle = self.tui.app.paginator:current_page():panel():current()
    return ptoggle.id
end

os.exit(lu.LuaUnit.run())
