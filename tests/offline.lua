-- Provides offline server for tests that don't want to
-- run the main loop, but still need to use the process
-- machinery (via 'iterate').

local wm2 = require("wm2")
local class = require("wm2.lib.middleclass")

local offline = {}

offline.Server = class("offline.Server", wm2.Server)

function offline.Server:initialize(options)
    self.iteration_duration = table.pop(options, "iteration_duration") or 0.25
    wm2.Server.initialize(self, options)
    self.looping = true
    self.clock = offline.Clock:new(self.iteration_duration)
end

function offline.Server:loop(tui, currentp)
    error("can't loop with offline server")
end

function offline.Server:iterate(iteration_count, sleeptime)
    for _ = 1, iteration_count or 1 do
        self:_iteration1()
        if sleeptime then
            wm2.utils.sleep(sleeptime)
        end
    end
end

-- offline.Clock mimics the API of 'wm2.cs.Clock', but
-- is rising a fixed number of seconds in each iteration.
offline.Clock = class("offline.Clock", wm2.WM2Object)

function offline.Clock:initialize(iteration_duration)
    wm2.WM2Object.initialize(self)
    self.iteration_duration = iteration_duration
end

function offline.Clock:bind(server) end

function offline.Clock:unbind() end

function offline.Clock:start()
    self._t = 0
end

function offline.Clock:stop() end

function offline.Clock:t()
    local t = self._t
    self._t = self._t + self.iteration_duration
    return t
end

function offline.Clock:orchestra()
    return ""
end

return offline
