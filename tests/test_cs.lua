-- This test suite includes all tests based on Csound:
-- the server, the instruments, etc.

local wm2 = require("wm2")
local lu = require("wm2.lib.luaunit")

local tcs = require("tests.tcs")

TestInstr = tcs()

function TestInstr:test_fixinputs()
    local instr = wm2.cs.instr:new("t", { instr = "aoutput0 = 0" })

    function t(inputs_ok, inputs, input_count)
        local inputs_test = instr:_fixinputs("test", "empty", inputs, input_count)
        local eq = table.eq(inputs_ok, inputs_test)
        lu.assertTrue(eq)
    end

    self:setupServer(instr)

    t({ "empty", "empty" }, {}, 2)
    t({ "t", "empty" }, { "t" }, 2)
    -- fixinputs doesn't drop any input if there are too many
    t({ "t0", "t1" }, { "t0", "t1" }, 1)
end

-- Ensure instr is audible if started
function TestInstr:test_audible()
    local instr = wm2.cs.instr:new("sine0", { instr = "  aoutput0 oscil 1, 200", channel_count = 1 })
    self:setupServer(instr)
    self:assertAudibleIfStarted(instr)
end

-- Ensure instr with fade in works
function TestInstr:test_fade_in()
    local fade_in_duration = 1
    local instr =
        wm2.cs.instr:new("t", { instr = "  aoutput0 = a(1)", channel_count = 1, fade_in_duration = fade_in_duration })
    self:setupServer(instr)
    self:assertAudibleIfStarted(instr)
    self:assertSilentIfStopped(instr)
    -- Assert during fade in time, instr indeed fades in.
    -- NOTE This is a somewhat imprecise test as it only checks
    -- that the amplitude is raising in two steps. However it's
    -- quite difficult to have preciser tests, as its not rising
    -- linearly.
    lu.assertEquals(instr:monitoring_value(), 0)
    local stepsize = 0.3
    local stepduration = fade_in_duration * stepsize
    local stepcount = math.floor(1 / stepsize)
    self:run(instr)
    local v = 0
    for i = 1, (stepcount - 1) do
        self:perform(stepduration)
        local v1 = instr:monitoring_value()
        lu.assertTrue(v < v1, "v: " .. v .. "; v1:" .. v1)
        v = v1
    end
end

-- Ensure instr with fade out works
function TestInstr:test_fade_out()
    local fade_out_duration = 1
    local instr =
        wm2.cs.instr:new("t", { instr = "  aoutput0 = a(1)", channel_count = 1, fade_out_duration = fade_out_duration })
    self:setupServer(instr)
    self:assertAudibleIfStarted(instr)
    self:assertSilentIfStopped(instr)
    -- Assert during fade out time, instr indeed fades out.
    -- NOTE This is a somewhat imprecise test (see also note
    -- at 'test_fade_in').
    instr:setdb(50) -- reduce likelihood of flaky test
    self:run(instr)
    self:perform(0.1)
    instr:stop()
    local stepsize = 0.1
    local stepduration = fade_out_duration * stepsize
    local v = 1 / 0 -- infinity
    for i = 1, 3 do
        self:perform(stepduration)
        local v1 = instr:monitoring_value()
        lu.assertTrue(v > v1, "v: " .. v .. "; v1:" .. v1)
        v = v1
    end
end

-- Ensure instr with both fades work ok
function TestInstr:test_fade_in_and_fade_out()
    local instr = wm2.cs.instr:new(
        "t",
        { instr = "  aoutput0 = a(1)", channel_count = 1, fade_in_duration = 0.5, fade_out_duration = 0.5 }
    )
    self:setupServer(instr)
    self:assertAudibleIfStarted(instr)
    self:assertSilentIfStopped(instr)
end

-- Ensure in a arate instr no DC persists
function TestInstr:test_arate_no_dc()
    local instr = wm2.cs.instr:new("noise", { instr = "  aoutput0 noise 1, 0.5", channel_count = 1 })
    self:setupServer(instr)
    self:assertAudibleIfStarted(instr) -- just to be sure it _could_ generate DC
    self:assertSilentIfStopped(instr)
    self:perform(0.1)
    self:assertIsSilent(instr)
end

-- Ensure in a krate instr the signal persists after being stopped
function TestInstr:test_krate_signal_persists()
    local instr = wm2.cs.instr:new("test", { instr = "  koutput0 = k(17)", channel_count = 1, rate = "krate" })
    self:setupServer(instr)
    self:run(instr)
    self:perform(0.2)
    lu.assertEquals(instr:monitoring_value(), 17)
    instr:stop()
    self:perform(0.3)
    lu.assertEquals(instr:monitoring_value(), 17)
end

-- Ensure setting 'channel_count' argument of an instrument creates
-- an instrument with the correct number of channels for both rates.
function TestInstr:test_channel_count()
    local testarate = wm2.cs.instr:new("testarate", {
        instr = [[
  asig noise 1, 0.5
  aoutput0 = asig
  aoutput1 = asig
  aoutput2 = asig
  aoutput3 = asig
  aoutput4 = asig
]],
        channel_count = 5,
        rate = "arate",
    })
    local testkrate = wm2.cs.instr:new(
        "testkrate",
        { instr = [[
  koutput0 = k(1)
  koutput1 = k(2)
  koutput2 = k(3)
]], channel_count = 3, rate = "krate" }
    )
    self:setupServer(testarate, testkrate)
    local instrdata = { [testarate] = 5, [testkrate] = 3 }
    for instr, chncount in pairs(instrdata) do
        self:run(instr)
        self:perform(0.3)
        local monitoring_values = instr:monitoring_values()
        lu.assertEquals(#monitoring_values, chncount)
        for _, val in ipairs(monitoring_values) do
            lu.assertIsTrue(val > 0)
        end
    end
end

-- Ensure setting audio inputs works as expected.
function TestInstr:test_audio_inputs()
    self:_tinputs("arate")
end

-- Ensure setting control inputs works as expected.
function TestInstr:test_control_inputs()
    self:_tinputs("krate")
end

-- Generic input testing
function TestInstr:_tinputs(rate)
    if rate == "arate" then
        prefix = "a"
        input_count = "audio_input_count"
        inputs = "audio_inputs"
    else
        prefix = "k"
        input_count = "control_input_count"
        inputs = "control_inputs"
    end
    local instroutput = wm2.cs.instr:new(
        "instroutput",
        { instr = prefix .. "output0 = " .. prefix .. "(0.5)", channel_count = 1, rate = rate }
    )
    local instrinput = wm2.cs.instr:new("instrinput", {
        instr = prefix .. "output0 = 2 * " .. prefix .. "input0",
        channel_count = 1,
        [input_count] = 1,
        [inputs] = { "instr_instroutput_" .. prefix .. "output0" },
        rate = rate,
    })
    self:setupServer(instroutput, instrinput)
    self:run(instrinput)
    self:perform(0.2)
    -- As long as 'instroutput' isn't running, this
    -- should be silent.
    self:assertIsSilent(instrinput)
    self:run(instroutput)
    self:perform(0.2)
    self:assertIsNotSilent(instroutput)
    self:assertIsNotSilent(instrinput)
end

-- Ensure arate instruments implement a db setting channel
-- (so that users can easily control the volume of each instr).
function TestInstr:test_arate_with_db_calculation()
    local margin = 0.0001 -- test margin
    local t = wm2.cs.instr:new("test", { instr = "aoutput0 = a(0.5)", channel_count = 1 })
    self:setupServer(t)
    self:run(t)
    self:perform(0.1)
    -- By default db should already not be 0, so that the
    -- signal is more quiet than without this db channel.
    lu.assertNotEquals(t:monitoring_value(), 0.5)
    -- Default should be -6dB, which is 0.5011872336272722
    lu.assertAlmostEquals(t:monitoring_value(), 0.2505936168136361, margin)
    -- However once we set it to 0, the monitoring value
    -- should indeed be 0.5 now.
    t:setdb(0)
    self:perform(0.1)
    lu.assertAlmostEquals(t:monitoring_value(), 0.5, margin)
    -- Now, using a lower db value again, we should have a
    -- quieter signal again:
    t:setdb(-20)
    self:perform(0.1)
    lu.assertAlmostEquals(t:monitoring_value(), 0.05, margin)
end

-- Ensure krate instruments don't implement a db setting
-- channel by default. Their value should be exactly the
-- same as the signals value.
function TestInstr:test_krate_no_db_calculation()
    local t = wm2.cs.instr:new("test", { instr = "koutput0 = k(20)", channel_count = 1, rate = "krate" })
    self:setupServer(t)
    self:run(t)
    self:perform(0.1)
    lu.assertEquals(t:monitoring_value(), 20)
    -- setdb shouldn't have any effect at all
    lu.assertError(t.setdb, t, -21) -- (it should raise an error)
    self:perform(0.1)
    lu.assertEquals(t:monitoring_value(), 20)
end

-- Test builtin 'wm2.cs.mixer' program
TestMixer = tcs()

function TestMixer:test_mixer()
    function makeinstr(replication_key)
        return wm2.cs.instr:new(replication_key, { instr = "aoutput0 = a(0.5)", channel_count = 1 })
    end

    local t0 = makeinstr("0")
    local t1 = makeinstr("1")

    local mixer = wm2.cs.mixer("0", { mapping = { instr_0_aoutput0 = { 1 }, instr_1_aoutput0 = { 0, 2 } } })
    self:setupServer(t0, t1, mixer)
    self:assertIsSilent(mixer)
    self:run(mixer)
    self:perform(0.1)
    -- Mixer already started playing, but should still be
    -- silent as all inputs are still not playing.
    self:assertIsSilent(mixer)
    -- Now we start t0 to check if we get an output at
    -- the second channel (but all other channels must
    -- keep silent).
    self:run(t0)
    self:perform(0.1)
    self:assertIsNotSilent(t0)
    self:assertIsNotSilent(mixer)
    self:assertChannelsAreNotSilent(mixer, 1)
    self:assertChannelsAreSilent(mixer, 0, 2)
    -- When starting t1 now, all channels should have
    -- a signal.
    self:run(t1)
    self:perform(0.1)
    self:assertIsNotSilent(t1)
    self:assertIsNotSilent(mixer)
    self:assertChannelsAreNotSilent(mixer, 0, 1, 2)
    -- Stopping t0 now should make channel 1 silent.
    t0:stop()
    self:perform(0.2)
    self:assertIsSilent(t0)
    self:assertChannelsAreNotSilent(mixer, 0, 2)
    self:assertChannelsAreSilent(mixer, 1)
    -- Stopping t1 now should make everything silent.
    t1:stop()
    self:perform(0.2)
    self:assertIsSilent(t1)
    self:assertChannelsAreSilent(mixer, 0, 1, 2)
    -- NOTE The following assertion fails, because
    -- there are very small leftover values on all
    -- channels (denormalized numbers). The following
    -- assertion is only true if the values are
    -- exactly 0, however the 'assertChannelsAre{Not}Silent'
    -- methods consider denormalized numbers to be
    -- silent and don't fail.
    -- self:assertIsSilent(mixer)
    mixer:stop()
    self:perform(0.2)
    self:assertIsSilent(mixer)
end

-- Test simple builtin instruments that don't need dedicated tests
TestSimpleInstr = tcs()

function TestSimpleInstr:test_simple_instr()
    -- Super simple test that only ensures {un}binding & starting/stopping P
    -- doesn't raise any error.
    for _, instr in ipairs(TestSimpleInstr.instruments) do
        local i = instr("0", {})
        self:setupServer(i)
        i:set_state({})
        i:start()
        self:perform(0.1)
        i:stop()
        self:perform(math.max(0.1, i.fade_out_duration))
        self:tearDown()
    end
end

TestSimpleInstr.instruments = {
    wm2.cs.in1,
    wm2.cs.outn,
    wm2.cs.sine,
    wm2.cs.midictrl,
    wm2.cs.butterhp,
    wm2.cs.reverb,
    wm2.cs.midiin,
}

os.exit(lu.LuaUnit.run())
