-- Test Server
--
-- This is a quite complex test suite, as the wm2 server has various
-- responsibilities that need to be tested. They are split into:
--
--  general test:   general server methods
--  csound test:    test audio server is behaving correctly
--  restart test:   test restart agent is correctly restarting server
--  loop test:      test global blocking program loop
--  request test:   start, stop & switch requests are correctly proceeded

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")
local class = require("wm2.lib.middleclass")
local xlu = require("tests.xlu")
local offline = require("tests.offline")
local tserver = require("tests.tserver")

--
--
-- Test general server methods
TestServerGeneral = tserver()

-- Test that starting the server creates all relevant effects.
function TestServerGeneral:test_start()
    self.server.CSOUND_LOG_PATH = ".wm2csound" .. math.random() .. ".log"

    -- We do it multiple time to ensure it always creates the same
    -- effects even if we repeat it some times.
    for _ = 0, 1 do
        lu.assertIsFalse(self.server:is_started())
        lu.assertIsFalse(self.patch:is_bound())
        lu.assertIsNil(self.server.csound)
        lu.assertIsNil(self.server.perf_thread)
        lu.assertIsFalse(self.server.looping)

        self.server:start(self.patch)

        lu.assertIsTrue(self.server:is_started())
        lu.assertIsTrue(self.patch:is_bound())
        lu.assertNotIsNil(self.server.csound)
        lu.assertNotIsNil(self.server.perf_thread)
        -- Should only turn on after self.server:loop() call
        lu.assertIsFalse(self.server.looping)

        lu.assertIsTrue(wm2.utils.file_exists(self.server.CSOUND_LOG_PATH))

        -- Unfortunately we can't ask Csound if it's started
        -- (there is no 'IsStarted' method or anything similar
        -- in the Csound class). We need to trust that it has
        -- been started. In follow-up test we test this
        -- indirectly: if it wouldn't have started those tests
        -- would fail.

        self.server:stop()

        wm2.utils.rm(self.server.CSOUND_LOG_PATH)
    end
end

-- Ensure 'panic' stops all started P,
-- but doesn't stop the server itself.
function TestServerGeneral:test_panic()
    self.server:start(self.patch)
    local programs = { self.p0, self.p1 }
    for i, p in ipairs(programs) do
        p.state.runtime_arg = {}
        p.state.requesters = { self.p_requester.id }
        p:start()
    end
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
    self.server:panic()
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
    lu.assertIsTrue(self.server:is_started())
end

--
--
-- Test Server Csound

TestServerCsound = tserver()

function TestServerCsound:setUp()
    TestServer.setUp(self)
    self.server:start(self.patch)
end

function TestServerCsound:tearDown()
    TestServer.tearDown(self)
end

--
--
-- Test Server restart capacity

TestServerRestart = tserver()

function TestServerRestart:setUp()
    TestServer.setUp(self, offline.Server)

    -- Ensure loop is never really executed, as we would otherwise
    -- get into loop when the restart agent restarts the server.
    -- The function nevertheless sets a tag in the test, so that
    -- we can ensure the restart was successful.
    xlu.monkeypatch:set_attribute(self.server, "loop", function()
        self.loop_executed = true
    end)

    self.server:start(self.patch)
    self.server:_loop_enter()
end

function TestServerRestart:tearDown()
    if self.server:is_started() then
        self.server:_loop_exit()
    end
    self.loop_executed = nil -- unset to not confuse next test
    TestServer.tearDown(self)
    xlu.monkeypatch:revert()
    self:revert_wm2patch()
end

-- Ensure 'maybe_restart' restarts server, but only in case
-- '_restart' flag is set.
function TestServerRestart:test_maybe_restart()
    -- initial state: we only started server, but didn't
    -- start loop yet
    lu.assertIsNil(self.loop_executed)
    lu.assertIsTrue(self.server:is_started())

    self:restart(true) -- restart

    lu.assertIsTrue(self.loop_executed)
    lu.assertIsTrue(self.server:is_started())

    self.loop_executed = nil -- reset to check if next call start loop

    self:restart(nil) -- don't restart (unset flag)

    lu.assertIsNil(self.loop_executed)
    lu.assertIsFalse(self.server:is_started())
end

-- Ensure we can restart as often as we like & csound doesn't
-- break (for instance due to a memory leak).
function TestServerRestart:test_infinite_restart()
    for _ = 1, 10 do
        self:restart(true)
    end
end

-- Ensure that when there is an active 'P' while restarting
-- the server, this 'P' is stopped after server restart.
function TestServerRestart:test_active_p_stopped_after_restart()
    local runtime_arg = { test = 1 }
    self:start_p(self.p0, self.p_requester, runtime_arg)
    xlu.assert_p_started(self.p0)
    self:restart(true) -- restart
    self:iterate(2)
    xlu.assert_p_stopped(self.p0)
end

-- Ensure that we can start and stop 'P' after a server restart.
function TestServerRestart:test_p_startable_after_restart()
    self:start_p(self.p0)
    xlu.assert_p_started(self.p0)
    self:start_p(self.p1)
    xlu.assert_p_started(self.p1)
    self:restart(true) -- restart
    self:iterate(2)
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
    self:start_p(self.p0)
    xlu.assert_p_started(self.p0)
    self:start_p(self.p1)
    xlu.assert_p_started(self.p1)
    self:start_p(self.p2)
    xlu.assert_p_started(self.p2)
    self:stop_p(self.p0)
    xlu.assert_p_stopped(self.p0)
end

-- Ensure no pre-restart requests exist anymore after server
-- has been restarted. In case they would still exist, suddenly
-- old requests would be unexpectedly executed again.
function TestServerRestart:test_no_leftover_requests()
    self.server:request_start(self.p0.id, self.p_requester.id)
    self:restart(true)
    for i, request in ipairs(self.server.conductor.requests_process._requests) do
        lu.fail("old request survived server restart!")
    end
end

-- Ensure reloading a wm2 patch indeed reloads the patch
-- and the new patch is then available (and the old patch no
-- longer available).
function TestServerRestart:test_reload_changed_patch()
    self:patch_wm2patch([[

configure:
  audio: "alsa"
  midi: "NULL"

declare:
  p:
    test:
]])

    self:restart(true, true)
    local patch = self.server.patch
    lu.assertEquals(#patch.sorted_programs, 1)
    lu.assertNotIsNil(patch.programs["p_test"])
end

-- Ensure server doesn't break when trying to reload
-- an invalid or corrupt wm2 patch file. The server
-- should continue to function with previously loaded
-- patch.
function TestServerRestart:test_reload_bad_patch()
    self:patch_wm2patch("faslfjqwf")
    self:restart(true, true)
    -- No error, so faulty patch hasn't been loaded.
    -- Let's see if old patch is still present:
    self:start_p(self.p0)
    xlu.assert_p_started(self.p0)
    self:stop_p(self.p0)
    xlu.assert_p_stopped(self.p0)
end

-- Ensure server doesn't break when attempting to reload
-- wm2 patch without a path (e.g. probably a patch that
-- hasn't been loaded from a file but from a string / from
-- RAM).
function TestServerRestart:test_reload_pathless_patch()
    self.server.patch.path = nil
    self:restart(true, true)
    -- No error, so wm2 just skipped error when trying
    -- to reload patch without path.
    -- Let's ensure old patch is still loaded:
    self:start_p(self.p0)
    xlu.assert_p_started(self.p0)
    self:stop_p(self.p0)
end

-- Override wm2 patch file (tests/tserver.yml) for server
-- tests to test patch reload functions.
function TestServerRestart:patch_wm2patch(content)
    local path = self.patch.path
    self._original_patch_path = "tests/.tserver.yml"
    wm2.utils.mv(path, self._original_patch_path)
    wm2.utils.write_to_file(path, content)
end

-- Revert all changes made to wm2 patch file.
function TestServerRestart:revert_wm2patch()
    if self._original_patch_path then
        wm2.utils.mv(self._original_patch_path, self.patch.path)
        self._original_patch_path = nil
    end
end

-- 'restart' stops server & runs restart agent.
function TestServerRestart:restart(restart, reload_patch)
    -- manually do 'server:restart()', as we aren't in a loop:
    self.server._restart = restart
    self.server.restart_agent.reload_patch = reload_patch
    self.server.restart_agent:maybe_collect()
    self.server:stop()
    self.server.restart_agent:maybe_restart()
end

--
--
-- Test Server loop

TestServerLoop = tserver()

function TestServerLoop:setUp()
    TestServer.setUp(self)
    self.server:start(self.patch)
end

function TestServerLoop:tearDown()
    TestServer.tearDown(self)
end

-- Test that the loop exits when a process returns 0
-- (if it doesn't exit, timeout error is raised).
function TestServerLoop:test_process_message_0()
    self:runtest(function(tprocess)
        return 0
    end)
end

-- Test that the loop doesn't sleep when a process returns 1.
function TestServerLoop:test_process_message_1()
    self:runtest(function(tprocess)
        if tprocess.is_first == nil then
            tprocess.is_first = true
            return 1
        else -- only quit in second iteration, otherwise
            -- _sleep could never be called in any way
            self.server:quit()
        end
    end)
    lu.assertIsNil(self.server._has_slept)
end

-- Test that the loop sleeps when a process returns 2.
function TestServerLoop:test_process_message_2()
    self:runtest(function(tprocess)
        if tprocess.is_first == nil then
            tprocess.is_first = true
            return 2
        else -- only quit in second iteration, otherwise
            -- _sleep could never be called in any way
            self.server:quit()
        end
    end)
    lu.assertIsTrue(self.server._has_slept)
end

-- Ensure the time passed from the server to each process
-- is increasing after each iteration.
function TestServerLoop:test_time_increases()
    testtable = {}

    local iteration1 = function(tprocess)
        local c = tprocess.tcounter
        if c == nil then
            tprocess.tcounter = 0
        -- do 5 tests & then quit
        elseif c > 5 then
            self.server:quit()
        end
        tprocess.tcounter = tprocess.tcounter + 1
        return 1 -- don't skip any iteration
    end

    local call = function(tprocess, t)
        local previous_t = tprocess.previous_t
        if previous_t ~= nil then
            -- Don't immediately run luaunit tests, otherwise the call
            -- raises an error & we only see a timeout error as a error
            -- message, because the wm2 server catches all exceptions that
            -- happen within processes.
            table.insert(testtable, {
                func = function(t, previous_t, counter)
                    local msg = "not increasing time in iteration "
                        .. counter
                        .. ". t: "
                        .. t
                        .. "; previous t: "
                        .. previous_t
                    lu.assertNotEquals(t, previous_t, msg)
                    lu.assertIsTrue(t > previous_t, msg)
                end,
                args = { t, previous_t, tprocess.tcounter },
            })
        end
        tprocess.previous_t = t
    end

    self:runtest(iteration1, call)

    -- Run actual tests
    for _, tdata in ipairs(testtable) do
        tdata.func(unpack(tdata.args))
    end
end

-- Ensure 'quit' method quits the servers loop
-- before timeout happens.
function TestServerLoop:test_quit()
    self:runtest(function(tprocess)
        self.server:quit()
    end)
end

-- Ensures 'Timeout' method works as expected
-- (this doesn't really test the 'Server' class,
-- but rather tests whether our test code works
-- as expected. Only if this test is ok, the
-- 'test_quit' and the 'test_process_message_0'
-- tests can be trusted to be correct).
function TestServerLoop:test_timeout()
    self:_runtest(function(tprocess) end)
    lu.assertIsTrue(self.timeout:is_timeout())
end

function TestServerLoop:runtest(iteration1_test, call_test)
    self:_runtest(iteration1_test, call_test)
    lu.assertIsFalse(self.timeout:is_timeout(), "Test timed out!")
end

function TestServerLoop:_runtest(iteration1_test, call_test)
    self.timeout = TestServerLoop.Timeout:new()
    self.tprocess = TestServerLoop.TProcess:new(iteration1_test, call_test)
    self.timeout:register()
    self.tprocess:register()
    self.server:loop()
    self.tprocess:unregister()
    self.timeout:unregister()
end

-- 'TestServerLoop.TProcess' provides possibility to
-- run test code after servers main blocking loop started.
TestServerLoop.TProcess = class("TProcess", wm2.Process)

function TestServerLoop.TProcess:__call(t)
    if self.call_test ~= nil then
        self.call_test(self, t)
    end
    return wm2.Process.__call(self, t)
end

function TestServerLoop.TProcess:initialize(iteration1_test, call_test)
    self.iteration1_test = iteration1_test
    self.call_test = call_test
    wm2.Process.initialize(self, "test process")
end

function TestServerLoop.TProcess:iteration1()
    if self.iteration1_test ~= nil then
        return self.iteration1_test(self)
    end
end

-- 'TestServerLoop.Timeout' is a process that times out
-- after 100 iterations - then it stops the main loop.
-- In this way bad 'test' code or bad 'TProcess' is
-- detected and doesn't run forever.
--
-- Timeout only works in case a process return value 0
-- is still considered as a STOP_LOOP message.
TestServerLoop.Timeout = class("Timeout", wm2.Process)

function TestServerLoop.Timeout:initialize()
    self._counter = 0
    self._max_counter = 100
    wm2.Process.initialize(self, "timeout process")
end

function TestServerLoop.Timeout:iteration1()
    self._counter = self._counter + 1
    if self:is_timeout() then
        return 0 -- stop loop
    end
    return 2 -- shouldn't change has_slept state
end

function TestServerLoop.Timeout:is_timeout()
    return self._counter >= self._max_counter
end

--
--
-- Test REQUEST

TestServerRequests = tserver()

function TestServerRequests:setUp()
    TestServer.setUp(self, offline.Server)
    self.server:start(self.patch)
    self.server:_loop_enter()
end

function TestServerRequests:tearDown()
    TestServer.tearDown(self)
end

--
--
-- Test 'CONSISTENCY' property of ACID.
--
--

-- Ensure 'request_start' starts the requested P and
-- 'request_stop' stops the requested P.
function TestServerRequests:test_request_start_and_stop()
    self:assert_all_p_stopped()
    self:start_p0()
    self:assert_only_p0_started()
    self:stop_p0()
    self:assert_all_p_stopped()
    -- Check if starting twice is also ok
    self:start_p0()
    self:start_p0()
    self:assert_only_p0_started()
    self:stop_p0()
    self:assert_all_p_stopped()
end

-- Ensure when calling 'request_start' with multiple
-- requesters, the server only stops the P if all
-- requesters requested its stop.
function TestServerRequests:test_request_start_and_stop_with_multiple_requesters()
    self:assert_all_p_stopped()
    self:start_p0(self.p1)
    self:assert_only_p0_started()
    self:start_p0(self.p_requester)
    self:assert_only_p0_started()
    self:stop_p0(self.p1)
    self:assert_only_p0_started()
    self:stop_p0(self.p_requester)
    self:assert_all_p_stopped()
end

-- Ensure when calling 'request_start' with multiple
-- requesters on a P with dependencies, the server only
-- stops the P and all its dependencies if all requesters
-- requested its stop.
function TestServerRequests:test_request_start_and_stop_with_multiple_requesters_and_dependencies()
    function assertstarted()
        xlu.assert_p_started(self.q0)
        self:assert_p0_and_p1_started()
    end

    self:assert_all_p_stopped()

    self:start_p(self.q0, self.p_requester)
    assertstarted()

    self:start_p(self.q0, self.p2)
    assertstarted()

    self:stop_p(self.q0, self.p_requester)
    -- They should still be started as p2 also requested the q.
    assertstarted()

    self:stop_p(self.q0, self.p2)

    -- Now no one requests q0 anymore and should be really
    -- stopped.
    self:assert_all_p_stopped()
end

-- Ensure nothing breaks if we call 'request_stop' on an
-- already stopped P
function TestServerRequests:test_request_stop_on_stopped_p()
    self:assert_all_p_stopped()
    self:stop_p0()
    self:assert_all_p_stopped()
end

-- Ensure a second start request of the same P is
-- rejected in case the runtime arguments are different
-- than in the first start request.
function TestServerRequests:test_request_start_with_different_runtime_arguments()
    self:assert_all_p_stopped()
    self:start_p0(self.p1, { test = 0 })
    xlu.assert_runtime_arg_is_equal(self.p0, { test = 0 })
    self:assert_only_p0_started()
    self:start_p0(self.p_requester, { test = 1 })
    xlu.assert_runtime_arg_is_equal(self.p0, { test = 0 })
    self:assert_only_p0_started()
    -- 'p1' is allowed as a requester..
    lu.assertEquals(self.p0.state.requesters[self.p1.id], 1)
    -- ..but 'p_requester' isn't, because it
    -- requested start of 'p0' with different
    -- runtime_arg than the runtime_arg with which
    -- it is running already.
    lu.assertIsNil(self.p0.state.requesters[self.p_requester.id], nil)
    self:stop_p0(self.p1)
    self:assert_all_p_stopped()
end

-- Test that a simple switch request, that stops a P and
-- starts a different P works.
function TestServerRequests:test_request_switch_between_p()
    self:assert_all_p_stopped()
    self:start_p0()
    self:assert_only_p0_started()
    self:switch_p(self.p0, self.p1)
    self:assert_only_p1_started()
    self:switch_p(self.p1, self.p0)
    self:assert_only_p0_started()
    self:stop_p0()
    self:assert_all_p_stopped()
end

-- Test that switch requests that only switch the runtime_arg
-- of the same P work
function TestServerRequests:test_request_switch_same_p()
    self:start_p0(self.p1, { test = 1 })
    self:assert_only_p0_started()
    xlu.assert_runtime_arg_is_equal(self.p0, { test = 1 })

    self:switch_p(self.p0, self.p0, self.p1, { test = 2 })
    self:assert_only_p0_started()
    xlu.assert_runtime_arg_is_equal(self.p0, { test = 2 })
end

-- Ensure when starting/stopping a P with dependencies,
-- all dependencies are also started/stopped.
function TestServerRequests:test_request_p_with_dependencies()
    self:assert_all_p_stopped()
    xlu.assert_p_stopped(self.q0)
    self:start_p(self.q0)
    self:assert_p0_and_p1_started()
    xlu.assert_p_started(self.q0)
    self:stop_p(self.q0)
    self:assert_all_p_stopped()
    xlu.assert_p_stopped(self.q0)
end

-- Ensure when starting a P with dependencies and then starting a
-- different P with the same dependencies, but different runtime
-- arguments, the second P start request fails.
function TestServerRequests:test_request_p_with_dependencies_and_different_runtime_arguments()
    self:start_p(self.q0)
    xlu.assert_p_started(self.q0)
    self:assert_p0_and_p1_started()

    self:start_p(self.q2)
    xlu.assert_p_stopped(self.q2)
    xlu.assert_p_started(self.q0)

    self:stop_p(self.q0)
    self:assert_all_p_stopped()

    self:start_p(self.q2)
    self:assert_only_p0_started()
end

-- Ensure start/stop/switch requests work with a P that has deeper
-- levels of dependencies.
function TestServerRequests:test_request_p_with_nested_dependencies()
    self:assert_all_p_stopped()
    self:start_p(self.q1)
    self:assert_p0_and_p1_started()
    xlu.assert_p_started(self.q1)
    xlu.assert_p_started(self.q0)
    self:stop_p(self.q1)
    xlu.assert_p_stopped(self.q1)
    xlu.assert_p_stopped(self.q0)
    self:assert_all_p_stopped()
end

-- Ensure switching between P with same nested dependencies work:
--
-- Switch from
--
--    q1:
--        q0:
--            p0:
--            p1:
--
-- to
--
--    q4:
--        q0:
--            p0:
--            p1:
--
--        p2:
function TestServerRequests:test_switch_nested_p()
    self:assert_all_p_stopped()
    self:start_p(self.q1)
    xlu.assert_p_started(self.q1)
    xlu.assert_p_started(self.q0)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
    self:switch_p(self.q1, self.q4)
    xlu.assert_p_stopped(self.q1)
    xlu.assert_p_started(self.q0)
    xlu.assert_p_started(self.q4)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
    xlu.assert_p_started(self.p2)
end

-- Ensure switching between P with dependencies work with nested relationships
-- on different levels:
--
--Switch from
--
--    q1:
--        q0:
--            p0:
--            p1:
--
-- to
--
--    q0:
--        p0:
--        p1:
function TestServerRequests:test_switch_nested_p_with_different_nesting_level()
    self:assert_all_p_stopped()
    self:start_p(self.q1)
    self:assert_p0_and_p1_started()
    xlu.assert_p_started(self.q1)
    xlu.assert_p_started(self.q0)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
    self:switch_p(self.q1, self.q0)
    xlu.assert_p_stopped(self.q1)
    xlu.assert_p_started(self.q0)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
end

--
--
-- Test 'ISOLATION' property of ACID.
--
--

-- Ensure that the order of requests is strictly followed by the
-- 'RequestsProcess', even when the requests happen "at the same
-- time" (e.g. during the same iteration of a loop).
function TestServerRequests:test_request_order()
    function start()
        self.server:request_start(self.p0.id, self.p_requester.id, {})
    end
    function stop()
        self.server:request_stop(self.p0.id, self.p_requester.id)
    end

    self:assert_all_p_stopped()

    -- The RequestsProcess always only proceeds 1 process
    -- per iteration. In this way the TUI or any other parallel
    -- process doesn't freeze in case of many start/stop/switch
    -- requests.
    start()
    stop()
    self:iterate()
    self:assert_only_p0_started()
    self:iterate()
    self:assert_all_p_stopped()

    start()
    stop()
    start()
    start()
    self:iterate()
    self:assert_only_p0_started()
    self:iterate()
    self:assert_all_p_stopped()
    self:iterate()
    self:assert_only_p0_started()
    self:iterate()
    self:assert_only_p0_started()
end

--
--
-- Test 'ATOMICITY' property of ACID.
--
--

-- Ensure that a rejected request is fully rejected (e.g. some
-- dependencies could be legal, but they are still rejected, because
-- some other dependencies fail).
function TestServerRequests:test_rejected_start_request_atomicity()
    self:start_p(self.q2)
    self:assert_only_p0_started()
    xlu.assert_set_state(self.q2)

    -- q0 starts p0 & p1. p1 would be ok to start, as
    -- q2 only starts p0. But because starting p0 is illegal,
    -- because it has different runtime arguments, the full
    -- request needs to be rejected.
    self:start_p(self.q0)
    self:assert_only_p0_started()
    xlu.assert_p_stopped(self.q0) -- prohibited to start
    xlu.assert_p_started(self.q2) -- should still be running
end

-- Same as previous test, but with switch request instead of start request.
function TestServerRequests:test_rejected_switch_request_atomicity()
    self:start_p(self.q2)
    self:assert_only_p0_started()
    self:start_p(self.p2)
    self:assert_p0_and_p2_started()
    -- The next switch request should be fully rejected - the
    -- q could actually legally start p1, but not p0 - and
    -- therefore both p0 & p1 should be rejected.
    self:switch_p(self.p2, self.q0)
    xlu.assert_p_stopped(self.q0)
    self:assert_p0_and_p2_started()
end

-- Ensure when doing a start request with a buggy P, the full P is
-- rejected.
function TestServerRequests:test_start_request_with_buggy_p_atomicity()
    lu.skip("TODO FIXME: Known bug")
    -- The start request should be fully rejected, because q3
    -- includes a buggy P. No change to the system should be applied
    -- by this start request.
    self:start_p(self.q3)
    lu.assertIsFalse(self.q3:is_started())
    self:assert_all_p_stopped()
end

-- Ensure when doing a switch request with a buggy P, the full P is
-- rejected.
function TestServerRequests:test_switch_request_with_buggy_p_atomicity()
    lu.skip("TODO FIXME: Known bug")
    self:start_p(self.p1)
    self:assert_only_p1_started()
    -- The next switch request should be fully rejected, because
    -- the q contains a buggy P. A full rejection means, p1
    -- should still run in the same way as before, the switch request
    -- shouldn't apply any changes to the system.
    self:switch_p(self.p1, self.q3)
    self:assert_only_p1_started()
end

-- Ensure that when restarting a P (switch request to same P) nothing happens
-- in case the parameters are the same.
function TestServerRequests:test_switch_request_same_runtime_arg_no_restart()
    local switched = false
    function switch(self)
        wm2.P.switch(self)
        switched = true
    end
    xlu.monkeypatch:set_attribute(self.p0, "switch", switch)
    local runtime_arg = { arg = 1 }
    self:start_p(self.p0, self.p_requester, runtime_arg)
    xlu.assert_p_started(self.p0)
    self:switch_p(self.p0, self.p0, self.p_requester, runtime_arg)
    -- NOTE Use a different requester to ensure that even in case
    -- there is a delta in the state of the p (a new requester is
    -- added that wasn't there yet before & therefore creating a
    -- delta), the conductor still doesn't switch the program.
    self:switch_p(self.p0, self.p0, self.p1, runtime_arg)
    lu.assertIsFalse(switched)
    xlu.monkeypatch:revert()
end

--
--
-- Utility methods for request tests

function TestServerRequests:start_p0(requester, runtime_arg)
    self:start_p(self.p0, requester, runtime_arg)
end

function TestServerRequests:stop_p0(requester)
    self:stop_p(self.p0, requester)
end

function TestServerRequests:start_p1(requester, runtime_arg)
    self:start_p(self.p1, requester, runtime_arg)
end

function TestServerRequests:stop_p1(requester)
    self:stop_p(self.p1, requester)
end

function TestServerRequests:assert_p0_and_p1_started()
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)
    xlu.assert_p_stopped(self.p2)
end

function TestServerRequests:assert_p0_and_p2_started()
    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)
    xlu.assert_p_started(self.p2)
end

function TestServerRequests:assert_all_p_stopped()
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
    xlu.assert_p_stopped(self.p2)
end

function TestServerRequests:assert_only_p0_started()
    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)
    xlu.assert_p_stopped(self.p2)
end

function TestServerRequests:assert_only_p1_started()
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_started(self.p1)
    xlu.assert_p_stopped(self.p2)
end

-- BuggyP mocks a P with a bug in its start/stop/switch code.
-- It helps to test if the server provides isolation even with
-- buggy P.
BuggyP = class("BuggyP", wm2.P)

function BuggyP:start()
    wm2.P.start(self)
    self:_bug()
end

function BuggyP:stop()
    wm2.P.stop(self)
    self:_bug()
end

function BuggyP:switch()
    wm2.P.switch(self)
    self:_bug()
end

function BuggyP:_bug()
    error("bug")
end

os.exit(lu.LuaUnit.run())
