-- Test Patch

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

local MockServer = {}

TestPatch = {}

function TestPatch:setUp()
    self.p0 = wm2.P("t0", {})
    self.p1 = wm2.P("t1", {})
    self.patch = wm2.Patch:new({}, self.p0, self.p1)
end

function TestPatch:test_is_bound()
    function isNotBound()
        lu.assertIsFalse(self.patch:is_bound())
        lu.assertIsFalse(self.p0:is_bound())
        lu.assertIsFalse(self.p1:is_bound())
    end

    function isBound()
        lu.assertIsTrue(self.patch:is_bound())
        lu.assertIsTrue(self.p0:is_bound())
        lu.assertIsTrue(self.p1:is_bound())
    end

    isNotBound()
    self.patch:bind(MockServer)
    isBound()
    self.patch:unbind()
    isNotBound()
end

function TestPatch:test_bind_twice()
    self.patch:bind(MockServer)
    lu.assertError(self.patch.bind, self.patch, MockServer)
end

function TestPatch:test_unbind_twice()
    self.patch:bind(MockServer)
    self.patch:unbind()
    lu.assertError(self.patch.unbind, self.patch)
end

function TestPatch:test_unbound_unbind()
    lu.assertError(self.patch.unbind, self.patch)
end

function TestPatch:test_p_count()
    lu.assertEquals(self.patch.p_count, 2)
end

function TestPatch:test_programs()
    lu.assertEquals(self.patch.programs["p_t0"], self.p0)
    lu.assertEquals(self.patch.programs["p_t1"], self.p1)
    lu.assertNotEquals(self.p0, self.p1)
end

function TestPatch:test_sorted_programs()
    lu.assertEquals(self.patch.sorted_programs[1], self.p0)
    lu.assertEquals(self.patch.sorted_programs[2], self.p1)
end

os.exit(lu.LuaUnit.run())
