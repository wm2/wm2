-- Test WM2Object.

local lu = require("wm2.lib.luaunit")
local wm2 = require("wm2")

TestWM2Object = {}

local LOGFILE_PATH = ".wm2.log"

function removelog()
    wm2.utils.rm(LOGFILE_PATH)
end

function TestWM2Object:setUp()
    removelog()
    self.wm2object = wm2.WM2Object:new()
end

function TestWM2Object:tearDown()
    removelog()
end

function TestWM2Object:test_log()
    -- This tests wm2 object default logging in two ways:
    --
    --  (1) The log file is created when calling logging methods.
    --  (2) The printed logging message is written to the log file.
    self:_test_log_file_does_not_exist()
    local msg = "test message"
    self.wm2object:info(msg)
    self:_test_log_file_exists()
    local content = wm2.utils.read_file(LOGFILE_PATH)
    lu.assertIsTrue(string.match(content, msg) ~= nil)
end

function TestWM2Object:_test_log_file_exists()
    lu.assertIsTrue(wm2.utils.file_exists(LOGFILE_PATH))
end

function TestWM2Object:_test_log_file_does_not_exist()
    lu.assertIsFalse(wm2.utils.file_exists(LOGFILE_PATH))
end

os.exit(lu.LuaUnit.run())
