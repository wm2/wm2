-- Test Seq

local lu = require("wm2.lib.luaunit")
local xlu = require("tests.xlu")
local offline = require("tests.offline")
local wm2 = require("wm2")

TestSeq = {}

function TestSeq:setUp()
    self.p0 = wm2.P("0", {})
    self.p1 = wm2.P("1", {})
    self.req = wm2.P("trequester", {})
    self.seq = wm2.Seq("0", {})
    self.patch = wm2.Patch({ loglevel = "debug" }, self.p0, self.p1, self.seq)
    self.server = offline.Server()
    self.server:start(self.patch)
end

function TestSeq:tearDown()
    if self.server:is_started() then
        self.server:stop()
    end
end

-- Ensure providing an event with the right
-- id starts the respective P.
function TestSeq:test_event_start()
    self:start()
    self:iterate(2)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)
end

-- Ensure a started P is stopped after all
-- events were consumed.
function TestSeq:test_p_stopped_after_all_events_consumed()
    self:start()

    -- Each iteration is 0.25, but it takes some iterations
    -- until process starts.
    self:iterate(6)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
end

-- Ensure a P is stopped if an event doesn't specify a P.
function TestSeq:test_p_stopped_if_empty_event()
    self:start({ events = { e(1, "p_0"), e(2) } })

    self:iterate(6)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_stopped(self.p1)
end

-- Test a P is switched if the events demand this.
function TestSeq:test_p_switched()
    self:start({ events = { e(1, "p_0"), e(1, "p_1") } })

    self:iterate(2)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_stopped(self.p1)

    self:iterate(4)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_stopped(self.p0)
    xlu.assert_p_started(self.p1)
end

-- Test a P is considered to be last active P if next P can't be
-- started because the request is rejected by the server.
function TestSeq:test_p_switched_error_consistency()
    -- We start 'p_1' with other runtime arguments by a different
    -- requested so that 'seq' is not allowed to start 'p_1'.

    self.server:request_start(self.p1.id, self.req.id, { test = 10 })
    self:iterate(2)
    xlu.assert_p_started(self.p1)

    -- Now let's start the sequencer
    self:start({ events = { e(1, "p_0"), e(1, "p_1") } })

    self:iterate(2)

    xlu.assert_p_started(self.seq)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)

    self:iterate(4)

    -- We should still have the same state as before,
    -- because the server should reject seq to start
    -- 'p_1' with other runtime arguments.
    xlu.assert_p_started(self.seq)
    xlu.assert_p_started(self.p0)
    xlu.assert_p_started(self.p1)

    -- Because seq can't start 'p_1', the 'previous_id' argument
    -- should still point to the previously started P - otherwise
    -- when we stop the process, it won't stop 'p_0' anymore.
    lu.assertEquals(tostring(self.seq.process.previous_id), "p_0")

    self:stop()

    self:iterate(2)

    xlu.assert_p_stopped(self.seq)
    xlu.assert_p_stopped(self.p0)
end

-- Ensure an event is repeated forever if the loop
-- flag is set.
function TestSeq:test_loop()
    self:start({ events = { e(1, "p_0") }, loop = true })
    self:iterate(2)

    for _ = 1, 10 do
        xlu.assert_p_started(self.seq)
        xlu.assert_p_started(self.p0)
        xlu.assert_p_stopped(self.p1)
        self:iterate(4)
    end
end

function TestSeq:start(runtime_arg)
    self.server:request_start(self.seq.id, self.req.id, runtime_arg or { events = { e(1, "p_0") } })
end

function TestSeq:stop()
    self.server:request_stop(self.seq.id, self.req.id)
end

function TestSeq:iterate(iteration_count)
    self.server:iterate(iteration_count)
end

function e(duration, id, runtime_arg)
    return { duration = duration or 1, p = { id, runtime_arg or {} } }
end

os.exit(lu.LuaUnit.run())
